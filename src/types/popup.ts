import type { DbPosition, MapCoordinates } from './db-position';

export interface DrawnMarkerPopupData {
  position: MapCoordinates | null;
  marker: any;
}
export interface DrawnTrackPopupData {
  distance_in_meters: number;
  positions: MapCoordinates[];
}
export interface DrawnAreaPopupData {
  area_readable: string;
  positions: MapCoordinates[];
}

export interface ItemPopupData {
  item_id: string;
  item_title: string;
  latest_position: DbPosition | null;
}

export type PopupData =
  | ItemPopupData
  | DrawnMarkerPopupData
  | DrawnTrackPopupData
  | DrawnAreaPopupData
  | {};
