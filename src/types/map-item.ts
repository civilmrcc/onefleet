import type { DbItemMapSettings } from './db-item';
import type { DbPosition } from './db-position';

export interface MapItem {
  // TODO: replace MapItem with a simple grouping of DbItem and DbPositions from db-item.ts in the future
  id: string;
  doc: {
    template: string;
    identifier: string;
    properties: {
      icon: string;
      name: string;
    };
    map_settings: DbItemMapSettings;
  };
  positions: {
    doc: DbPosition;
  }[];
}

/**
 * A collection of leaflet objects needed for representing a single MapItem or DbItem on a Leaflet map
 */
export interface ItemLeafletObjects {
  marker: L.Marker<any>;
  line: L.Polyline<any, any>;
  lineCaptions: L.Marker<any>[];
  show_track?: boolean; // whether to show track on the map
  showing_on_layer?: string; // which map layer an item was added to
}
