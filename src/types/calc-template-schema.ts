import type { Templates } from '@/constants/templates';

export interface CalcTemplate {
  /* Name of the actor/group of actors using this sheet
   * This name var and the version var are mandatory as their combination 'name@version' is to be stored in a second
   * sheet of the file for the import to know what rules to follow.
   * This information will also be stored in all the items that will be imported.
   * This info and the next are assumed to always be stored in the first cell of the second sheet.
   */
  name: string;

  //Revision of this sheet
  version: string;

  //Default status for the case
  defaultStatus?: string;

  //Name of the sheet containing the template info
  templateInfoSheetname: 'Template';

  //Name of the sheet containing the data
  dataSheetName: 'Data';

  //Type of item to create
  typeOfItemToParse: keyof typeof Templates.templates;

  //At what row is the tool supposed to start parsing for cases?
  casesRowStart: number;

  //What's the furthest column to parse data from?
  letterColumnEnd: string;

  // Is this a deprecated calc format that should be rejected by the tool and inform the user.
  deprecated: boolean;

  CalcFields: Array<CalcTemplateField>;

  CalcPositions: Array<CalcTemplatePosition>;
}

export interface CalcTemplateField {
  // Expected name of the column(s) containing the info - Used to debug new calc templates changes in human terms
  cellName: string[];

  // Actual column(s) letter(s) containing the info - Use this in methods to read the workbook
  columnLetter: string[];

  // Name of the field in the context of onefleet
  caseTemplateFieldEquivalentName: string;

  //Fallback field if column value is empty or unparsable, the order of the array expresses the priority
  fallbackCells?: Array<FallBackFieldCells>;

  // Whether this information is required for this item to be created (default: false)
  requiredForCreation?: boolean;

  // Used for special cases fields where a regex is required
  // Try to avoid having the creators of an excel file write in the cells in a way that necessitates regex operations
  regex?: RegExp;

  // Use this to override the default value provided by the item definition for "select" fields for example
  defaultValue?: string;
}

export interface FallBackFieldCells {
  // Expected name of the column(s) containing the information - Used to debug new calc templates changes in human terms
  fallbackCellName: string[];

  // Actual column(s) letter(s) containing the information
  column: string[];
}

export interface CalcTemplatePosition {
  // Expected positions format
  format: 'DD @ ISO' | 'DMS @ ISO' | 'REGEX';

  // Actual column(s) letter(s) containing the position - Used to debug new calc templates changes in human terms
  positionCellName: string;

  // Actual column(s) letter(s) containing the position - Use this in methods in methods to read the workbook
  positionCellColumn: string;

  regex?: RegExp;
}
