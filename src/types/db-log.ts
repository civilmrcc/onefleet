import type { DbItemProperty } from './db-item';

export interface LogEntry {
  _id: string;
  _rev?: string;
  user: string;
  change: LogEntryChange;
  comment: string;
}

export interface LogEntryChange {
  index: string;
  old: DbItemProperty;
  new: DbItemProperty;
}
