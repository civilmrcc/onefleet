import type { DbItemProperty } from './db-item';

export interface ParsingResult {
  /** Whether or not valid data was able to be parsed. */
  success: boolean;
  /** Should equal to undefined data was present in the parsed cell but could not be parsed.
   * Should equal to null if no data was in present in the cells parsed.
   * In every other case, equals the parsed data value.
   */
  result: DbItemProperty;
}
