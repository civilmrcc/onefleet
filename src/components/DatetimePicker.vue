<script lang="ts">
import type { PropType } from 'vue';
import { defineComponent } from 'vue';

import {
  overrideTimezoneWithUtc,
  addOffsetToDatetime,
} from '@/utils/datetimeUtils';

/**
 * [Element-UI's DatetimePicker](https://element.eleme.io/#/en-US/component/datetime-picker)
 * with integrated timezone conversion to UTC
 */
export default defineComponent({
  name: 'DatetimePicker',

  props: {
    /**
     * Placeholder for input field
     */
    placeholder: { type: String as PropType<string>, required: true },
    /**
     * Whether input is mandatory or not
     */
    mandatory: {
      type: Boolean as PropType<boolean>,
      required: false,
      default: false,
    },
    /**
     * The icon class to use as prefix in input field
     */
    prefixIcon: {
      type: String as PropType<string>,
      required: false,
      default: '',
    },
    /**
     * Datetime in browser locale's timezone as ISO String
     */
    datetimeLocal: {
      type: String as PropType<string>,
      required: false,
      default: null,
    },
    /**
     * Whether input field should be disabled or not
     */
    disabled: {
      type: Boolean as PropType<boolean>,
      required: false,
      default: false,
    },
    /**
     * Whether input field should be clearable or not
     */
    clearable: {
      type: Boolean as PropType<boolean>,
      required: false,
      default: true,
    },
    /**
     * The format of the displayed value in the input box
     */
    format: {
      type: String as PropType<string>,
      required: false,
      default: 'YYYY-MM-DD HH:mm',
    },
    /**
     * Tooltip shown on hover
     */
    title: { type: String as PropType<string>, required: false, default: '' },
  },

  emits: ['update_datetime'],

  data() {
    return {
      /**
       * el-date-picker unfortunately does not allow specifying a timezone, so the datetime selected will be dependent on the browser locale.
       * Do not use this for anything else than as v-model for el-date-picker
       */
      uiDatetime: this.datetimeLocal
        ? (addOffsetToDatetime(
            new Date(this.datetimeLocal),
            new Date(this.datetimeLocal).getTimezoneOffset()
          ) as Date)
        : null,
      /**
       * Controls CSS styling
       */
      uiState: '' as string,
    };
  },

  computed: {
    /**
     * Get the date and time of sighting in UTC.
     *
     * @returns The date and time of sighting in UTC.
     */
    datetimeUtc(): Date | null {
      return this.uiDatetime ? overrideTimezoneWithUtc(this.uiDatetime) : null;
    },
  },

  watch: {
    /**
     * This watcher overrides it with the current time in UTC, if the entered datetime happens to be in the future.
     *
     * This is necessary because the "Now" button of `el-date-picker` selects the current datetime in the browser's timezone.
     */
    uiDatetime(): void {
      const currentUtcTime = addOffsetToDatetime(
        new Date(),
        new Date().getTimezoneOffset()
      );
      if (this.uiDatetime && this.uiDatetime > currentUtcTime) {
        this.uiDatetime = currentUtcTime;
      }
    },
    /**
     * Once input value changes, notify parent component.
     * parent component can listen to this event to update
     * e.g. corresponding form state value accordingly
     */
    datetimeUtc(): void {
      this.$emit(
        'update_datetime',
        this.datetimeUtc ? this.datetimeUtc.toISOString() : null
      );
    },
  },

  methods: {
    updateDatetime(datetime: Date | null): void {
      if (datetime) {
        this.uiDatetime = datetime;
      }
      this.uiState = datetime ? 'full-width' : 'full-width error';
    },
    /*
     *  Set UI state of Datetime Picker to Error
     */
    setUiStateToError(): void {
      this.uiState = 'full-width error';
    },
    resetDatetime(): void {
      this.uiDatetime = null;
      this.uiState = 'full-width';
    },
    /**
     * Disable dates that lie in the future
     * @param date - parameter in datepicker
     */
    disabledDate(date: Date): boolean {
      const currentUtcTime = addOffsetToDatetime(
        new Date(),
        new Date().getTimezoneOffset() + 120 // allow to select dates 2h in the future
      );
      return date && date > currentUtcTime ? true : false;
    },
  },
});
</script>

<template>
  <el-date-picker
    v-model="uiDatetime"
    type="datetime"
    :placeholder="placeholder"
    :format="format ? format : 'YYYY-MM-DD HH:mm'"
    size="small"
    :class="mandatory ? uiState : 'full-width'"
    :disabled="disabled"
    :prefix-icon="prefixIcon"
    :title="title"
    :disabled-date="disabledDate"
    :clearable="clearable"
    @change="updateDatetime"
  >
  </el-date-picker>
</template>

<style scoped>
.full-width {
  width: 100% !important;
}
</style>
