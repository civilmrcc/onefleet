<script setup lang="ts">
import type { TagSettings } from '@/types/tags';
import type { ComputedRef, PropType, Ref } from 'vue';
import type { DbItemProperty } from '@/types/db-item';
import { ref, nextTick, computed, watch } from 'vue';
import { ElSelect, ElTooltip } from 'element-plus';
import { type ElInput, ElTag, ElButton, ElOption } from 'element-plus';

/** This component is using the Composition API
 *   Please see https://vuejs.org/guide/extras/composition-api-faq.html#what-is-composition-api
 *   for an explanation on the differences between it and the Options API that
 *   most other components in Onefleet currently use as of writing this comment (January 2023).
 */
const props = defineProps({
  /**
   * All the tags entered as of the initialization of this component
   */
  field: {
    type: String as PropType<DbItemProperty>,
    required: false,
    default: null,
  },
  /**
   * The options of the tag to determine what parts of the template to display and
   * how to handle the logic of the form.
   */
  propTagSettings: { type: Object as PropType<TagSettings>, required: true },
});

const emits = defineEmits(['submit']);

/**  String value used to separate the key and the value of a tag
 * Should eventually not be needed as we leverage couchDB's JSON structure with Record types
 */
const keyValueSeparator: string = ':';
/**  String value used to separate the different tags
 * Should eventually not be needed as we leverage couchDB's JSON structure with Record and/or Array types
 */
const tagSeparator: string = ';';

// The DOM elements of the key and the value of the tag being entered by the user.
const newTagKey: Ref<string> = ref('');
const newTagValue: Ref<string> = ref('');

// The tags ordered in an array to use as local data to modify before sending it back to the parent.
const dynamicTags = computed(() => {
  return props.field === null
    ? []
    : String(props.field! ?? '').split(tagSeparator);
});

// Method to remove a tag from the array
const handleDelete = (tag: string) => {
  dynamicTags.value.splice(dynamicTags.value.indexOf(tag), 1);
  sendEventToParent(dynamicTags);
};

// The template ref of the select input
const InputSelectRef = ref<InstanceType<typeof ElSelect>>();

// Ref and method to show and hide the inputs for a new tag
const inputVisible: Ref<boolean> = ref(false);
const showInput = async () => {
  inputVisible.value = true;
  await nextTick();
  InputSelectRef.value?.focus();
};

const toolTipText: Ref<string> = ref('');
const findTooltipKeyValue = (tag: string): void => {
  toolTipText.value = '';
  Object.entries(props.propTagSettings.fixedKeyOptions).find(([key, value]) => {
    if (tag.split(keyValueSeparator)[0] === key) {
      toolTipText.value = value;
      return;
    }
  });
};

const confirmButton: Ref<boolean> = ref(false);
/**
 * Once the confirm button is pressed, calls the
 * appropriate functions to handle the newly entered tag depending on the TagSettings
 */
watch(confirmButton, async () => {
  let validKey: Boolean = true;
  const newKey: string = newTagKey.value.split(' ').pop() as string;
  if (confirmButton.value) {
    if (!props.propTagSettings.allowOpenOptions) {
      validKey = Object.keys(props.propTagSettings.fixedKeyOptions).some(
        (key) => newKey === key
      );
    }

    if (props.propTagSettings.forceUniqueKeys) {
      removeDuplicateKeys(newKey);
    }

    if (validKey) {
      if (props.propTagSettings.keyOnly) {
        handleInputConfirmKeyOnlyTag();
      } else if (props.propTagSettings.fixedKeyOptions) {
        handleInputConfirmKeyValueTag(newKey);
      }
    }
  }
});

/**
 * If a new type of tag consisting only of a key is to be created, this is the method to implement
 * to handle that new tag, along with an appropriate template.
 */
function handleInputConfirmKeyOnlyTag(): void {}

/**
 * Constructs a new key:value tag and adds it to
 * the local tags array and calls the method to send the modified tags to the parent.
 */
function handleInputConfirmKeyValueTag(newKey: string): void {
  if (newKey && areThereNoForbiddenCharactersInValue()) {
    if (dynamicTags.value[0] === '') {
      dynamicTags.value[0] =
        newKey + keyValueSeparator + newTagValue.value.split(' ').join('');
    } else {
      dynamicTags.value!.push(
        newKey + keyValueSeparator + newTagValue.value.split(' ').join('')
      );
    }
    resetComponentState();
    sendEventToParent(dynamicTags);
  } else {
    return;
  }
}

/**
 * Checks for forbidden characters (: or ;)
 * that would break the string of tags sent to the DB.
 * Displays an error message if any of these characters are found.
 */
function areThereNoForbiddenCharactersInValue(): boolean {
  const regex = /[:;]/gm;
  const forbiddenCharacterIndex = newTagValue.value.search(regex);
  if (forbiddenCharacterIndex === -1) {
    return true;
  } else {
    ElMessage.error({
      showClose: true,
      duration: 10000,
      message: `You used at least one forbidden character, remove any of the following characters -> ${tagSeparator} or ${keyValueSeparator}`,
    });
    return false;
  }
}

/**
 * Resets the form after a new tag has been entered
 */
function resetComponentState(): void {
  inputVisible.value = false;
  newTagKey.value = '';
  newTagValue.value = '';
  confirmButton.value = false;
}

/**
 * Takes an updated tags array and transforms it back to a string before sending it to the parent
 * @param dynamicTags - The updated local tags to send to the parent.
 */
function sendEventToParent(dynamicTags: ComputedRef<string[]>): void {
  let finalTagString: string = '';
  dynamicTags.value?.forEach((tag) => {
    finalTagString += tag + tagSeparator;
  });
  finalTagString = finalTagString.slice(0, -1);

  emits('submit', finalTagString);
}

/**
 *  Takes a key as an argument to check against the existings tags to look for duplicates.
 *  If a duplicate exists, removes the old tag using that specific key before calling sendEventToParent()
 *  with the updated tags.
 * @param newTagKey - The key to check against the keys of the existings tags
 */
function removeDuplicateKeys(newTagKey: string): void {
  dynamicTags.value.forEach((key: string, index: number) => {
    if (props.propTagSettings.keyOnly) {
      if (key === newTagKey) {
        dynamicTags.value.splice(index, 1);
      }
    } else {
      const keyOnly = dynamicTags.value[index].split(`${keyValueSeparator}`);
      if (keyOnly[0] === newTagKey) {
        dynamicTags.value.splice(index, 1);
      }
    }
  });
  sendEventToParent(dynamicTags);
}
</script>

<template>
  <div v-if="!propTagSettings.keyOnly">
    <div class="tag-group">
      <div v-if="dynamicTags[0] != ''">
        <el-tooltip
          v-for="tag in dynamicTags"
          :key="tag"
          :hide-after="0"
          placement="top"
        >
          <template #content>{{ toolTipText }}</template>
          <el-tag
            closable
            effect="dark"
            class="tag"
            size="default"
            @mouseenter="findTooltipKeyValue(tag)"
            @close="handleDelete(tag)"
            >{{ tag }}
          </el-tag></el-tooltip
        >
      </div>

      <el-button
        class="new-tag-button"
        size="small"
        icon="Plus"
        @click="showInput"
      >
        New Tag
      </el-button>
    </div>
    <div>
      <el-select
        v-if="inputVisible && propTagSettings.fixedKeyOptions"
        ref="InputSelectRef"
        v-model="newTagKey"
        :placeholder="propTagSettings.labelKey"
        filterable
        clearable
        class="select"
        ><el-option
          v-for="[key, value] in Object.entries(
            propTagSettings.fixedKeyOptions
          )"
          :key="key"
          :value="value + ' - ' + key"
        ></el-option>
      </el-select>

      <el-input
        v-if="inputVisible && propTagSettings.fixedKeyOptions"
        v-model="newTagValue"
        class="value-input"
        :placeholder="propTagSettings.labelValue"
      />
      <el-button
        v-if="inputVisible"
        class="confirm-button"
        @click="confirmButton = !confirmButton"
        >Confirm</el-button
      >
    </div>
  </div>
</template>

<style>
.tag {
  margin-right: 1.5%;
  margin-bottom: 0.5%;
}

.tag-group {
  margin-bottom: 1%;
}

.new-tag-button {
  width: 100%;
}

.value-input {
  width: 100%;
  margin-bottom: 1%;
}

.select {
  width: 100%;
  margin-bottom: 1%;
}

.confirm-button {
  width: 100%;
}
</style>
