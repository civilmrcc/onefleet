<script lang="ts">
import { defineComponent } from 'vue';
import type { Templates } from '@/constants/templates';
import type { DbItemProperty } from '@/types/db-item';
import templates from '@/constants/templates';

import PositionsTable from './PositionsTable.vue';
import InputTable from './InputTable.vue';
import NumberInput from './NumberInput.vue';
import DatetimePicker from '../DatetimePicker.vue';
import TagInput from './TagInput.vue';
import { ulid } from 'ulid';

import type { DbItem } from '@/types/db-item';
import type { LogEntryChange } from '@/types/db-log';
import type { DbPosition, NewPosition } from '@/types/db-position';
import { STORABLE_POSITION_FIELDS } from '@/types/db-position';
import type { MessageOptions } from 'element-plus';
import { ElMessageBox } from 'element-plus';
import type {
  ItemTemplate,
  ItemTemplateField,
} from '@/types/item-template-schema';
import * as mapUtils from '@/utils/map/positionUtils';

import { mapState, mapStores } from 'pinia';
import { useDataStore } from '../../stores/DataStore';
import { useShowStore } from '../../stores/ShowStore';

export default defineComponent({
  name: 'EditItem',

  components: {
    DatetimePicker,
    PositionsTable,
    InputTable,
    NumberInput,
    TagInput,
  },

  data() {
    const dataStore = useDataStore();
    const showStore = useShowStore();

    return {
      dataStore,
      showStore,
      mode: '', // SHOW | CREATE
      template: {} as ItemTemplate,
      current_template: undefined as
        | keyof typeof Templates.templates
        | undefined,
      form_data: null as DbItem | null,
      historical_form_data: undefined as DbItem | undefined,
      positions: [] as (DbPosition | NewPosition)[],
      current_positions: [] as (DbPosition | NewPosition)[],
      historicalPositionsMap: {} as Record<string, DbPosition>,
      showCommentBox: false,
      comment: '',
      collapse_list: ['Important'] as string[],
      formchanged: false,
    };
  },

  computed: {
    itemTemplateText(): string {
      return (
        this.historical_form_data?.template +
        ' ' +
        this.form_data?.properties.name
      );
    },
    headerText(): string {
      if (this.mode === 'CREATE') return 'Create ' + this.current_template;
      return this.itemTemplateText; // SHOW
    },
    savePositionText(): string {
      const change_lists = this.positionsChangeLists;
      const trashText = this.form_data?.soft_deleted ? ' to trash' : '';
      let change_text = '';
      if (change_lists.create.length > 0)
        change_text += `+${change_lists.create.length} `;
      if (change_lists.delete.length > 0)
        change_text += `-${change_lists.delete.length} `;
      if (change_lists.update.length > 0)
        change_text += `±${change_lists.update.length} `;

      return (
        trashText + (change_text.length > 0 ? ` (${change_text}Positions)` : '')
      );
    },
    positionsChangeLists(): {
      create: NewPosition[];
      delete: DbPosition[];
      update: DbPosition[];
    } {
      return {
        create: this.current_positions.filter(
          (pos: NewPosition | DbPosition) =>
            !('_id' in pos) &&
            !pos.soft_deleted &&
            !(pos.lat == 0 && pos.lon == 0)
        ) as NewPosition[],
        delete: this.current_positions.filter(
          (pos: NewPosition | DbPosition) =>
            '_id' in pos &&
            (pos.soft_deleted ?? false) &&
            this.historicalPositionsMap[pos['_id']] &&
            !(this.historicalPositionsMap[pos['_id']].soft_deleted ?? false)
        ) as DbPosition[],
        update: this.current_positions.filter(
          (pos: NewPosition | DbPosition) =>
            '_id' in pos &&
            !(pos.soft_deleted ?? false) &&
            this.historicalPositionsMap[pos['_id']] &&
            this.positionChanged(
              pos as DbPosition,
              this.historicalPositionsMap[pos['_id']]
            )
        ) as DbPosition[],
      };
    },
    // This function groups the template's fields by field group.
    groupedTemplateFields(): Record<string, ItemTemplateField[]> {
      if (!this.template || !this.template.fields) return {};

      const grouped_fields: Record<string, ItemTemplateField[]> = {};
      const other_fields: ItemTemplateField[] = [];

      for (let p_index = 0; p_index < this.template.fields.length; p_index++) {
        const template_field: ItemTemplateField = this.template.fields[p_index];
        const field_group: string | undefined = template_field.field_group;
        if (field_group) {
          if (!grouped_fields[field_group]) {
            grouped_fields[field_group] = [];
          }
          grouped_fields[field_group].push(template_field);
        } else {
          // when the template field has no field_group
          other_fields.push(template_field);
        }
      }
      // after going through all template fields, append the properties
      // without a field_group to the end if there are any:
      if (other_fields.length) {
        grouped_fields['Other Properties'] = other_fields;
      }
      return grouped_fields;
    },
    ...mapState(useShowStore, ['showItem']),
    ...mapStores(useDataStore, useShowStore),
  },

  watch: {
    collapse_list(): void {
      const positionsElement = this.$refs[
        'positions-table-element'
      ] as typeof PositionsTable;
      if (positionsElement) {
        positionsElement.updateDisplayPositions();
      }
    },
  },

  mounted() {},

  async created() {
    if (this.showItem.id) {
      this.mode = 'SHOW';
      // get item from local database
      const item: DbItem = await this.$db.getItem(this.showItem.id);

      this.positions = this.showItem.positions as NewPosition[];

      // load template for doc
      this.template = await this.loadTemplate(item.template);
      this.current_template = item.template;

      // load doc into form_data and historical formdata for changelog
      this.historical_form_data = item;
      this.form_data = JSON.parse(JSON.stringify(item));
      if (
        this.form_data &&
        this.form_data.properties &&
        !this.form_data.map_settings
      ) {
        this.form_data.map_settings =
          mapUtils.convertTrackingPropertiesToMapSettings(
            this.form_data,
            this.template?.default_marker_icon ?? item.template
          );
      }
    } else {
      this.mode = 'CREATE';
      // create new item modal
      this.positions = this.showStore.createItem.positions as NewPosition[];

      this.historical_form_data = undefined;
      this.collapse_list = ['Important'];

      this.current_template = this.showStore.createItem
        .templateKey as keyof typeof Templates.templates;
      this.template = await this.loadTemplate(this.current_template);
      this.form_data = {
        template: this.current_template,
        properties: this.prefillProperties(this.template),
        map_settings: {
          marker_color:
            '#' +
            ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, '0'),
          marker_icon:
            this.template.default_marker_icon ??
            this.showStore.createItem.templateKey,
          get_historical_data_since: -1, // number of days that should be loaded for this item
          tracking_type: this.template.default_tracking_type,
          tracking_id: '', // e.g. MMSI number for AIS or Iridium Sender Mail or ADSB id
          tracking_token: '', // e.g. fleetmon vessel id or other temporary token
        },
      };
    }
  },

  methods: {
    async loadTemplate(template_name: keyof typeof Templates.templates) {
      return template_name
        ? templates.get(template_name)
        : ({} as ItemTemplate);
    },
    prefillProperties(template: ItemTemplate): Record<string, DbItemProperty> {
      const properties: Record<string, DbItemProperty> = {};
      for (const i in template.fields) {
        const field_name = template.fields[i].name;
        const default_value = template.fields[i].default_value;
        if (default_value !== undefined) {
          properties[field_name] = default_value;
        }
      }
      return properties;
    },
    async closeModal(confirm: boolean = false): Promise<void> {
      // confirm - whether confirm dialog is to be shown
      try {
        if (confirm && this.formchanged) {
          await this.$confirm(
            'Are you sure you want to cancel? All changes to the form will be lost.',
            'Are you sure?',
            {
              confirmButtonText: 'Close and lose changes',
              cancelButtonText: 'Keep editing',
              type: 'warning',
            }
          );
        }
        this.showStore.setShowItem({ id: undefined, positions: null });
        this.formchanged = false;
        this.showStore.setCreateItem({
          show: false,
          templateKey: undefined,
          positions: [],
        });
      } catch {
        // there was a confirmation dialog that was cancelled: do nothing.
        // (but without this catch there will be an "uncaught" error)
      }
    },
    containsAtLeastOnePosition(positions: NewPosition[]): boolean {
      if (
        this.template?.enforce_initial_position == true &&
        positions.filter((pos) => !pos.soft_deleted).length === 0
      ) {
        this.$message.error({
          duration: 5000,
          showClose: true,
          message: `The template "${this.current_template}" requires at least one position, according to its configuration.`,
        } as MessageOptions);
        return false;
      }
      return true;
    },
    _validateNewPosition(position: NewPosition): boolean {
      return Boolean(
        position &&
          position.timestamp &&
          position.timestamp == new Date(position.timestamp).toISOString() &&
          position.lat &&
          position.lon &&
          position.lat != 0 &&
          position.lon != 0
      );
    },
    /** Ensure all given positions are valid: */
    areAllGivenPositionsValid(new_positions: NewPosition[]): boolean {
      for (
        let position_index = 0;
        position_index < new_positions.length;
        position_index++
      ) {
        const position = new_positions[position_index];
        if (!this._validateNewPosition(position)) {
          const position_string = JSON.stringify(position, null, 2);
          console.log('position_string: ' + position_string);
          this.$message.error({
            duration: 5000,
            showClose: true,
            message: `The given position #${position_index} is not valid.
                Please correct it. Here is the bad position: ${position_string} `,
          } as MessageOptions);
          return false;
        }
      }
      return true;
    },
    showExportModal(id: string): void {
      this.showStore.setExportItemId(id);
    },
    async saveItem(): Promise<void> {
      if (!this.form_data) {
        return;
      }

      // we must never create an item without a proper name:
      if (
        (((this.form_data.properties?.name as string) || '').length ?? 0) <= 2
      ) {
        this.$message.error({
          duration: 5000,
          showClose: true,
          message: 'Please enter a valid name',
        } as MessageOptions);
        this.collapse_list = ['Important'];
        return;
      }
      if (
        !this.containsAtLeastOnePosition(this.current_positions) ||
        !this.areAllGivenPositionsValid(this.current_positions)
      ) {
        return;
      }

      //compare the changed form data with historic properties to identify changes
      const changes: LogEntryChange[] = [];
      for (const i in this.form_data.properties || []) {
        if (
          this.historical_form_data?.properties &&
          this.historical_form_data?.properties[i] !==
            this.form_data.properties[i]
        ) {
          const oldValue = this.historical_form_data.properties[i];
          const newValue = this.form_data.properties[i];
          if (oldValue && oldValue !== newValue) {
            changes.push({
              index: i,
              old: oldValue,
              new: newValue,
            });
          }
        }
      }
      for (const i in changes) {
        this.$db.addItemLog(this.showItem.id!, changes[i], this.comment);
      }
      this.comment = '';
      if (
        this.historical_form_data?.map_settings?.get_historical_data_since !=
        this.form_data.map_settings?.get_historical_data_since
      ) {
        this.dataStore.reloadPositionsFromLocalDatabase(this.showItem.id);
      }

      const pouch_identifier = this.template?.pouch_identifier;
      const item_id =
        this.form_data._id || `${pouch_identifier}_${ulid()}`.toUpperCase();

      const response: PouchDB.Core.Response | PouchDB.Core.Error =
        this.mode === 'SHOW'
          ? await this.$db.updateItem(this.form_data as DbItem)
          : await this.$db.createItem(this.form_data, item_id);

      if (!(response as PouchDB.Core.Response).ok) {
        const err = response as PouchDB.Core.Error;
        if (err.name === 'conflict') {
          this.$message.error({
            duration: 5000,
            showClose: true,
            message: 'The id is already taken, please choose another one',
          } as MessageOptions);
        } else {
          this.$message.error({
            duration: 5000,
            showClose: true,
            message: 'An unknown error occured while creating the item.',
          } as MessageOptions);
        }
        console.error('error while storing item:', err);
      } else {
        this.dataStore.reloadItem(item_id);
        this.$message.success({
          duration: 5000,
          message:
            'The ' +
            this.itemTemplateText +
            ' has been ' +
            (this.mode === 'SHOW' ? 'updated' : 'created'),
        } as MessageOptions);

        // Store any changed positions after the item has been stored:
        this._storePositions(item_id);
      }
    },

    /**
     * Stores changed Positions after the DbItem has been changed in the database. Called from saveItem().
     * (CouchDB knows no atomic database transactions besides single writes)
     */
    _storePositions(item_id: string) {
      const create_list: NewPosition[] = this.positionsChangeLists.create;
      const delete_list: DbPosition[] = this.positionsChangeLists.delete;
      const update_list: DbPosition[] = this.positionsChangeLists.update;

      const creation_promises = this.$db.createPositions(item_id, create_list);
      const update_promises = this.$db.updatePositions([
        ...delete_list,
        ...update_list,
      ]);

      Promise.allSettled([...creation_promises, ...update_promises])
        .then((all_settled_result) => {
          const rejected_promises = all_settled_result.filter(
            (value: PromiseSettledResult<PouchDB.Core.Response>) =>
              value.status === 'rejected'
          );
          if (rejected_promises.length > 0) {
            console.error('Rejected Promises: ', rejected_promises);
            console.warn('All Promises: ', all_settled_result);
            this.$message.error({
              showClose: true,
              duration: 0,
              message: `Error! Could not store ${rejected_promises.length} of
                        ${all_settled_result.length} positions.`,
            } as MessageOptions);
          } else if (all_settled_result.length == 0) {
            this.$message.info({
              duration: 5000,
              message: 'No positions were changed, so none stored.',
            } as MessageOptions);
            this.closeModal(false);
          } else {
            this.$message.success({
              duration: 5000,
              message:
                all_settled_result.length > 1
                  ? `All ${all_settled_result.length} changed positions for
                         ${this.itemTemplateText} have been stored`
                  : `The changed position for ${this.itemTemplateText} has been stored`,
            } as MessageOptions);
            this.closeModal(false);
          }
        })
        .catch((error) => {
          console.error('Oh-oh while waiting for promises to settle:', error);
          this.$message.error({
            duration: 0,
            showClose: true,
            message: 'Internal Error: Positions could not be changed.',
          } as MessageOptions);
        });
    },

    positionChanged(
      position: DbPosition,
      historical_position: DbPosition
    ): boolean {
      let position_changed: boolean = false;
      for (const field_name of Object.keys(position)) {
        if (STORABLE_POSITION_FIELDS.includes(field_name)) {
          const field_changed: boolean =
            position[field_name] != historical_position[field_name];
          position_changed = position_changed || field_changed;
        }
      }
      return position_changed;
    },

    updateMapSettingsField(field: string, event: any): void {
      this.form_data!.map_settings![field] = event;
    },

    updatePropertiesField(field: string, event: any): void {
      this.form_data!.properties![field] = event;
    },

    /**
     * Async function that updates a DatetimeMulti field.
     *
     * As DatetimeMulti field types contain many comma-separated values,
     *   updating a single of these requires us to split the comma-separated string,
     *   replace just the one value with its replacement, and then join them all back
     *   together again.
     * @param fieldname The name of the field to be updated.
     * @param event The changed text (for text fallback) or ISO-formatted timestamp
                    string (for DatetimePicker return values)
     * @param singleTimestampId The ID of the ;-separated value to be replaced
     */
    async updateDatetimeMultiPropertiesField(
      fieldname: string,
      event: string,
      singleTimestampId: number
    ): Promise<void> {
      const activeElement = document.activeElement as HTMLOrSVGElement | null;
      this.updatePropertiesField(
        fieldname,
        String(this.form_data?.properties[fieldname] ?? '')
          .split(';')
          .map((ts, i) => (i === singleTimestampId ? event : ts))
          .filter((ts) => ts.length > 0)
          .join(';')
      );
      // In order to have the UI widget for text (or theoretically also for dates)
      // to be updated with the new contents after the substring replacement operation,
      // a quick solution is to make it lose focus and then give it focus again.
      // This will cause Vue to update it's value. Without this, the text fields value get
      // set to an empty string (''), which may cause data loss if the user does not
      // notice this and clicks "Save".
      activeElement?.blur();
      await this.$nextTick();
      activeElement?.focus();
      await this.$nextTick();
    },

    fieldHasChanged(fieldname: string): boolean {
      if (!this.form_data || !this.historical_form_data) {
        return false;
      }
      return (
        this.form_data.properties[fieldname] !==
        this.historical_form_data.properties[fieldname]
      );
    },

    showTrashWarning(): void {
      if (this.form_data?.soft_deleted) {
        ElMessageBox.alert(
          `Do you really want to move this ${this.current_template} to trash?
          Checking this box and then hitting save will move this ${this.current_template} to the trash.
          If you do not wish to move this ${this.current_template} to trash,
          please uncheck the red box you just clicked.
          Otherwise if you decide to go through with it, you will always be able to restore
          this ${this.current_template} later by choosing the "Trash" filter on the left-hand of
          the screen and unchecking the same box.`,
          `Do you really want to move this ${this.current_template} to trash?`,
          {
            confirmButtonText: 'I understand.',
          }
        );
      } else {
        ElMessageBox.alert(
          `Do you really want to restore this ${this.current_template}?
          Unchecking this box and then hitting save will restore this ${this.current_template}.
          You will always be able to move this ${this.current_template} to trash again by checking this
          box and then hitting save.`,
          `Do you want to restore this ${this.current_template} ?`,
          {
            confirmButtonText: 'I understand.',
          }
        );
      }
    },
    capitalize(str: string): string {
      return str
        .split(' ')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
    },
  },
});
</script>

<template>
  <div class="background">
    <div
      v-if="form_data"
      class="form-style-6 rounded-xl border-2 border-primary"
    >
      <h1>{{ capitalize(headerText) }}</h1>
      <form @submit.prevent="saveItem" @change="formchanged = true">
        <el-collapse v-model="collapse_list" accordion>
          <el-collapse-item key="Positions" title="Positions" name="Positions">
            <div
              v-if="
                form_data &&
                form_data.map_settings &&
                form_data.map_settings.tracking_type != 'MANUAL'
              "
              id="position"
            >
              ℹ️ Automatic tracking is selected by default for template
              {{ current_template }}. If you wish to enter positions manually,
              please deactivate automatic tracking in Map Settings -> Tracking
              Type.
            </div>

            <div v-else>
              <PositionsTable
                ref="positions-table-element"
                v-model:current_positions="current_positions"
                v-model:original_positions="historicalPositionsMap"
                :historical-item="mode === 'SHOW' ? historical_form_data : []"
                :given-positions="positions"
              />
            </div>
          </el-collapse-item>

          <el-collapse-item
            v-for="(field_group, field_group_id) in groupedTemplateFields"
            :key="field_group_id"
            :title="field_group_id"
            :name="field_group_id"
          >
            <div v-for="field in field_group" :key="field.name">
              <div v-if="!field.hidden">
                <div :class="fieldHasChanged(field.name) ? 'changed-row' : ''">
                  <span :title="field.info">{{ field.title }}&nbsp;</span>
                  <el-tooltip :content="field.info" placement="top">
                    <el-icon>
                      <InfoFilled />
                    </el-icon>
                  </el-tooltip>

                  <el-tooltip
                    v-if="fieldHasChanged(field.name)"
                    placement="top"
                  >
                    <template #content>
                      The contents of field '{{ field.title }}' have changed:
                      <br />
                      <table alt="">
                        <tr>
                          <th>Old:</th>
                          <td>
                            {{ historical_form_data?.properties[field.name] }}
                          </td>
                        </tr>
                        <tr>
                          <th>New:</th>
                          <td>
                            {{ form_data.properties[field.name] }}
                          </td>
                        </tr>
                      </table>
                    </template>
                    <el-icon>
                      <Edit />
                    </el-icon>
                  </el-tooltip>

                  <span
                    v-if="form_data.properties[field.name] &&
                    field.options &&
                    !field.options[form_data.properties[field.name] as string]
                    "
                    class="deprecated_field_option"
                  >
                    (unknown value: "{{ form_data.properties[field.name] }}")
                  </span>
                </div>

                <!-- iconwrapper start -->
                <div v-if="field.type == 'icon'" class="iconwrapper">
                  <input
                    v-model="form_data.properties[field.name]"
                    :name="field.name"
                    :placeholder="field.title"
                    type="text"
                    class="icon"
                  />
                  <span
                    class="preview-icon"
                    :class="'el-icon-' + form_data.properties[field.name]"
                    >&nbsp;</span
                  >
                </div>
                <!-- iconwrapper end -->

                <!-- Tag  start -->
                <div v-else-if="field.type == 'tag'">
                  <TagInput
                    v-model:field="form_data!.properties[field.name]"
                    :prop-tag-settings="field.tag_settings!"
                    @submit="updatePropertiesField(field.name, $event)"
                  />
                </div>
                <!-- Tag end -->

                <!-- Date Picker start -->
                <DatetimePicker
                  v-else-if="field.type === 'datetime'"
                  :datetime-local="form_data!.properties[field.name] as string"
                  :placeholder="field.title"
                  @update_datetime="updatePropertiesField(field.name, $event)"
                />
                <!-- Date Picker end -->

                <!-- DatetimeMulti Picker start -->
                <div v-else-if="field.type == 'datetimemulti'">
                  <!-- Make a row per ;-separated value -->
                  <span
                    v-for="(singleTimestamp, singleTimestampId) in String(
                      form_data?.properties[field.name] ?? ''
                    ).split(';')"
                    style="white-space: nowrap; overflow: scroll; display: flex"
                  >
                    &nbsp;&nbsp;{{ singleTimestampId + 1 }}.&nbsp;
                    <!-- Decide if we should show a DatetimePicker or a textbox
                      by trying to parse the singleTimestamp as a datetime. -->
                    <span
                      v-if="
                        Date.parse(`${singleTimestamp}`) >= 0 ||
                        singleTimestamp.length === 0
                      "
                    >
                      <!-- The DatetimePicker should only be clearable if it is the
                        last row. This is because Vue does not update its values
                        correctly if the component keeps being rendered after the
                        underlying number of ;-separated values changes. So the
                        simplest solution was to just disallow the
                        deletion/clearing of values from anywhere. Keeping the
                        last DatetimePicker clearable is fine, because the Vue
                        component then just gets removed as the array length is
                        reduced. Also, we want to keep at least one DatetimePicker
                        clearable, so that deletion of values remains possible. -->
                      <!-- The "new Date($event).toISOString()" part below is a failfast,
                        so that this is supposed to crash if for any reason the incoming
                        data from DatetimePicker is not parseable as a date. -->
                      <DatetimePicker
                        :datetime-local="singleTimestamp"
                        :placeholder="field.title"
                        format="YYYY-MM-DD HH:mm"
                        :title="singleTimestamp"
                        :clearable="
                          singleTimestampId ===
                          String(form_data?.properties[field.name] ?? '').split(
                            ';'
                          ).length -
                            1
                        "
                        @update_datetime="
                          updateDatetimeMultiPropertiesField(
                            field.name,
                            $event ? new Date($event).toISOString() : '',
                            singleTimestampId
                          )
                        "
                      />
                      <el-tooltip
                        content="A timestamp was recognised and you're
                        now seeing a date/time picker here. 🤝"
                        placement="bottom"
                      >
                        <el-icon>
                          <Check />
                        </el-icon>
                      </el-tooltip>
                    </span>
                    <span v-else>
                      <!-- If this span is rendered (v-else), then the value was not parseable
                        as a datetime and we want to render a textbox instead. -->
                      <el-input
                        :ref="`input_${singleTimestampId}`"
                        :value="singleTimestamp"
                        type="text"
                        placeholder="Please use ISO 8601 format! Hover for more info..."
                        @input="
                          updateDatetimeMultiPropertiesField(
                            field.name,
                            $event,
                            singleTimestampId
                          )
                        "
                      />
                      <!-- Explain to the user why they are seeing a textbox
                        instead of a DatetimePicker, and what to do about it: -->
                      <el-tooltip placement="bottom">
                        <template #content>
                          You are seeing this textbox because the existing data
                          cannot be understood. <br />
                          <br />
                          Multiple timestamps: <br />
                          &nbsp;&nbsp;Text can be separated via a semicolon (;)
                          character to represent multiple times, if needed.
                          <br />

                          Timestamp format: <br />
                          &nbsp;&nbsp;Please specifiy timestamps as ISO 8601
                          format, e.g. 2024-01-05T09:04:15.000Z <br />
                          &nbsp;&nbsp;to make them machine-readable! (Sorry,
                          pasting in any time format is not implemented yet)<br />
                          <br />
                          After that, you'll be presented with a proper
                          date/time picker popup as reward. 😉 <br />
                        </template>
                        <el-icon style="color: orange">
                          <WarnTriangleFilled />
                        </el-icon>
                      </el-tooltip>
                    </span>
                    <br />
                  </span>

                  <!-- Allow users to add another row (i.e. append a semicolon): -->
                  <el-button
                    size="small"
                    icon="Plus"
                    @click="
                      updatePropertiesField(
                        field.name,
                        String(form_data?.properties[field.name] ?? '') + ';'
                      )
                    "
                  />
                </div>
                <!-- DatetimeMulti Picker end -->

                <!-- Select start -->
                <select
                  v-else-if="field.type == 'select'"
                  v-model="form_data.properties[field.name]"
                >
                  <option
                    v-for="(option_name, option) in field.options"
                    :key="option"
                    :value="option"
                  >
                    {{ option_name }}
                  </option>
                </select>
                <!-- Select end -->

                <!-- NumberInput start -->
                <div v-else-if="field.type == 'number'">
                  <NumberInput
                    v-model:field.number="form_data.properties[field.name]"
                    :label="'Unknown'"
                    :prop-value="parseInt(form_data.properties[field.name] as string, 10)"
                    :prop-minimum-value="field.minimum_value"
                    :prop-maximum-value="field.maximum_value"
                    :prop-step="field.step"
                  />
                </div>
                <!-- NumberInput end -->

                <!-- InputTable start -->
                <div v-else-if="field.type == 'inputtable'">
                  <InputTable
                    v-model:field="form_data.properties[field.name]"
                  />
                </div>
                <!-- InputTable end -->

                <!-- Checkbox start -->
                <el-checkbox
                  v-else-if="field.type == 'checkbox'"
                  v-model="form_data.properties[field.name]"
                  border
                  :label="field.title"
                />
                <!-- Checkbox end -->

                <!-- Default start -->
                <el-input
                  v-else
                  v-model="form_data.properties[field.name]"
                  :name="field.name"
                  :type="field.type"
                  :step="field.step"
                />
                <!-- Default end -->
              </div>
            </div>
          </el-collapse-item>

          <el-collapse-item
            key="Map Settings"
            title="Map Settings"
            name="Map Settings"
          >
            <div v-if="form_data.map_settings">
              <p>
                Marker Color:
                <el-color-picker
                  v-model="form_data.map_settings.marker_color"
                />
              </p>

              <p>
                Marker Icon:
                <i>
                  {{ form_data.map_settings.marker_icon }}
                </i>
              </p>

              <p>Get historical data since (days)</p>
              <div>
                <NumberInput
                  :label="'Use Default'"
                  :prop-value="form_data.map_settings.get_historical_data_since"
                  @update:field="
                    updateMapSettingsField('get_historical_data_since', $event)
                  "
                />
              </div>

              <p>
                Tracking Type
                <select v-model="form_data.map_settings.tracking_type">
                  <option key="MANUAL" value="MANUAL">
                    No Automatic Tracking
                  </option>
                  <option key="AIS" value="AIS">AIS (via MMSI number)</option>
                  <option key="ADSB" value="ADSB">ADS-B Aircraft Id</option>
                  <option key="IRIDIUMGO" value="IRIDIUMGO">Iridium Go</option>
                </select>
              </p>

              <div v-if="form_data.map_settings.tracking_type != 'MANUAL'">
                <span>Tracking ID</span>
                <input
                  v-model="form_data.map_settings.tracking_id"
                  name="tracking_id"
                  type="text"
                />
                <span>Tracking Token</span>
                <el-input
                  v-model="form_data.map_settings.tracking_token"
                  disabled
                  placeholder="Tracking Token"
                />
              </div>
              <div>
                <el-checkbox
                  v-model="form_data.soft_deleted"
                  @change="showTrashWarning"
                >
                  Trash this {{ template }}
                </el-checkbox>
              </div>
            </div>
            <div v-else>This item has no separate map settings yet.</div>
          </el-collapse-item>
        </el-collapse>
        <a @click="showExportModal(showItem.id!)">
          <el-link type="info">Export Location</el-link>
        </a>
        <br />
        <a @click="showCommentBox = true">
          <el-link type="info">Comment Change</el-link>
        </a>
        <input
          id="commentCheck"
          v-model="showCommentBox"
          type="checkbox"
          value="true"
          style="width: auto !important"
        />
        <div v-if="showCommentBox">
          <textarea id="comment" v-model="comment" />
        </div>

        <div class="save_cancel_buttons mt-2">
          <input type="submit" :value="'Save' + savePositionText" />
          <input type="cancel" value="Cancel" @click="closeModal(true)" />
        </div>
      </form>
    </div>
    <div v-else class="form-style-6">
      <el-collapse>
        <p class="mt-4 mb-8 text-xl">Loading...</p>
        <div class="save_cancel_buttons">
          <input
            type="cancel"
            value="Cancel Loading"
            @click="closeModal(false)"
          />
        </div>
      </el-collapse>
    </div>
  </div>
</template>
