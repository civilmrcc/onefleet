<script lang="ts">
import SinglePosition from './SinglePosition.vue';

import { formatTimestamp, timeSince } from '@/utils/datetimeUtils';
import { asDMS, positionIsValid } from '@/utils/map/positionUtils';

import type {
  DbPosition,
  DisplayPosition,
  NewPosition,
} from '@/types/db-position';
import { STORABLE_POSITION_FIELDS } from '@/types/db-position';
import type { PropType } from 'vue';
import { defineComponent } from 'vue';
import type { ElTable, MessageOptions } from 'element-plus';
import type { DbItem } from '@/types/db-item';

export default defineComponent({
  name: 'PositionsTable',

  components: {
    SinglePosition,
  },

  props: {
    givenPositions: {
      type: Array as PropType<NewPosition[]>,
      default: () => [],
    },
    addInitialPosition: {
      type: Boolean,
      default: false,
    },
    historicalItem: {
      type: Object as PropType<DbItem>,
      default: () => null,
      required: false,
    },
  },

  emits: ['update:current_positions', 'update:original_positions'],

  data() {
    return {
      show_age: false,
      display_positions: [] as DisplayPosition[],
      current_positions: [...this.givenPositions] as (
        | DbPosition
        | NewPosition
      )[],
      original_positions: {} as Record<string, DbPosition>,
    };
  },

  computed: {},

  watch: {
    current_positions() {
      this.$emit('update:current_positions', this.current_positions);
    },
    original_positions() {
      this.$emit('update:original_positions', this.original_positions);
    },
  },

  mounted(): void {
    // already add a first empty new position to start an empty list, if requested:
    if (this.addInitialPosition) {
      this.addNewPosition();
    }

    // load first page of positions from the database
    this.loadMoreExistingPositions(false);
  },

  created(): void {},

  methods: {
    /**
     * Extends position data (of type either `NewPosition` or `DbPosition`) into a full `DisplayPosition`
     * that can be shown in the UI. A `DisplayPosition` includes human-readable representations for
     * the position's timestamp and coordinates, as well as providing an easy way to see if a position
     * is in any way broken or soft-deleted.
     */
    extendPositionData(pos: NewPosition | DbPosition): DisplayPosition {
      return {
        ...pos,
        ...{
          time: formatTimestamp(pos.timestamp),
          age: timeSince(pos.timestamp, null) + ' ago',
          dms: asDMS(pos),
          altitude_feet: pos.altitude
            ? pos.altitude + ' ft'
            : (pos.altitude ?? '')?.toString(),
          broken: !positionIsValid(pos),
          soft_deleted: pos.soft_deleted ?? false,
        },
      };
    },

    /**
     * Update all display positions only when this method is called.
     * We avoid updating the display_positions while editing a SinglePosition,
     * as updating display_positions will lead to a re-rendering of the el-table and
     * any expanded rows will be closed. So if we were to re-render the table mid-edit,
     * the expanded row(s) would close on every key press, which would make editing a
     * SinglePosition highly annoying and near-impossible.
     */
    updateDisplayPositions(): void {
      this.display_positions = this.current_positions.map((pos) =>
        this.extendPositionData(pos)
      );
    },

    /**
     * Decide which CSS styles to use for highlighting table rows depending on their content.
     */
    tableRowCSS({ row }: { row: DisplayPosition }) {
      // The `change_status` is shown via background colour of each row of the table.
      let change_status = '';
      if (row._id) {
        const orig_pos = this.original_positions[row._id];
        let row_changed = false;
        for (const field_name of Object.keys(row)) {
          if (
            STORABLE_POSITION_FIELDS.includes(field_name) &&
            field_name != 'soft_deleted' // handle the `soft_deleted` field separately below
          ) {
            const field_changed = row[field_name] != orig_pos[field_name];
            row_changed = row_changed || field_changed;
          }
        }
        if (!(row.soft_deleted ?? false) && (orig_pos.soft_deleted ?? false)) {
          // undeleting previously soft-deleted positions shows up as a change (blue background)
          row_changed = row_changed || true;
        }
        if (row_changed) {
          // show blue background (i.e. "changed position") if any fields have changed
          change_status = 'changed-row';
        }
        if (row.soft_deleted && !(orig_pos.soft_deleted ?? false)) {
          // show red background (i.e. "about to delete") only if an existing position is about to be deleted
          change_status = 'deleted-row';
        }
      } else {
        // show green background (i.e. "new position") if a row has no _id field yet
        change_status = 'new-row';
      }

      // The `info_status` is show via the foreground color and text-decoration of each row of the table.
      let info_status = '';
      if (row.soft_deleted) {
        // show grey foreground text with strike-through when the position has been "deleted".
        info_status = 'soft-deleted';
      } else if (row.broken) {
        // show red foreground text with strike-through when the position data is somehow broken.
        info_status = 'broken';
      }
      return `${change_status} ${info_status}`;
    },
    /** Toggle row expansion to show SinglePosition component when clicking anywhere on a row in the table */
    rowClick(row: any) {
      const positionsTable = this.$refs['positionsTable'] as typeof ElTable;
      positionsTable.toggleRowExpansion(row);
    },

    /**
     * Add a new position to the PositionsTable for further editing
     */
    addNewPosition() {
      // NewPositions live only in current_positions because we do not need to track changes on them
      this.current_positions.push({
        lat: 0,
        lon: 0,
        source: 'onefleet',
        timestamp: new Date().toISOString(),
      } as NewPosition);
      this.updateCurrentPositions();
      this.updateDisplayPositions();
    },

    /**
     * Load another page of positions!
     */
    async loadMoreExistingPositions(
      showInfoMessage: boolean = true
    ): Promise<void> {
      // guard condition: Only try to load more positions if we have a DbItem to load them for.
      if (!this.historicalItem) {
        return;
      }

      // request a new page of positions from the database:
      const originalLength = Object.keys(this.original_positions).length;
      const pageLength = 50;
      const page = Math.floor(originalLength / pageLength);
      const loadedPageOfExistingPositions = await this.$db.getAllPositionsPaged(
        this.historicalItem,
        page,
        pageLength
      );
      // Insert the new page of positions into key-value record to avoid duplicates
      // and so that they can be found with less effort later when comparing changes:
      for (const pos of loadedPageOfExistingPositions) {
        this.original_positions[pos._id] = pos;
      }
      // ensure Vue notices the change: shallow copy
      this.original_positions = { ...this.original_positions };

      // Show info message if requested:
      if (showInfoMessage) {
        const loadedPositionsCount =
          Object.keys(this.original_positions).length - originalLength;
        const pluralOrNot = loadedPositionsCount === 1 ? '' : 's';
        const infoText =
          loadedPositionsCount > 0
            ? `Displaying ${loadedPositionsCount} more position${pluralOrNot} from the database.`
            : 'No more positions to display.';
        this.$message.info({
          duration: 3000,
          message: infoText,
        } as MessageOptions);
      }

      this.updateCurrentPositions();
      this.updateDisplayPositions();
    },

    /**
     * The current changes in the position array needs to be independently changeable from
     * what we received as props, so that users can also duplicate existing positions later,
     * and thereby change/increase the length of this array. Also, sort by timestamp:
     *
     * When loading new EXISTING positions from the database, we want to insert only those
     * that actually are freshly loaded into the current_positions array, so that we don't
     * loose any changes that have been made since the last load. This is also why we make
     * a shallow copy of existing positions: So that we can later tell what has been modified
     * and needs to be saved.
     *
     * In contrast, NEW positions live directly within current_positions and no where else,
     * because no changes need to be detected on them. They are new.
     */
    updateCurrentPositions(): void {
      const currentIds = this.current_positions.map((pos) => pos['_id']);
      const additionalOriginals = Object.values(this.original_positions)
        .filter((pos) => !currentIds.includes(pos._id))
        .map((pos) => ({ ...pos })); // shallow copy so that we can detect changes later

      this.current_positions = [
        ...this.current_positions,
        ...additionalOriginals,
      ].sort((a, b) => -(a.timestamp || '').localeCompare(b.timestamp || ''));
    },
  },
});
</script>
<template>
  <div class="positionbox">
    <el-table
      v-if="display_positions.length > 0"
      ref="positionsTable"
      :data="display_positions"
      style="width: 100%"
      size="small"
      max-height="400"
      :row-class-name="tableRowCSS"
      @row-click="rowClick"
    >
      <el-table-column type="expand">
        <template #default="props">
          <SinglePosition
            v-model:position="current_positions[props.$index]"
            :original-position="
              original_positions[current_positions[props.$index]['_id']] ?? {
                ...current_positions[props.$index],
              }
            "
            @finished_editing="updateDisplayPositions()"
          />
        </template>
      </el-table-column>
      <el-table-column
        :prop="show_age ? 'age' : 'time'"
        :label="show_age ? 'Age' : 'Time'"
        :width="show_age ? 110 : 195"
      />
      <el-table-column prop="dms" label="Lat/Lon" width="190" />
      <el-table-column prop="source" label="Datasource" width="140" />
      <el-table-column prop="heading" label="CoG" width="50" />
      <el-table-column prop="speed" label="SoG" width="50" />
      <el-table-column prop="altitude_feet" label="Altitude" width="80" />
    </el-table>

    <div v-else>This item has no positions.</div>

    <div class="flex mt-2">
      <el-button size="small" class="my-auto" @click="addNewPosition()">
        Add new Position
      </el-button>
      <el-button
        v-if="display_positions.length > 0 && historicalItem"
        size="small"
        class="mr-4 my-auto"
        @click="loadMoreExistingPositions()"
      >
        Load More
      </el-button>

      <div class="my-auto">
        <el-checkbox v-if="display_positions.length > 0" v-model="show_age">
          Show Relative Time
        </el-checkbox>
      </div>
    </div>
  </div>
</template>

<style>
.positionbox > label,
.positionbox > div > label {
  margin-left: 5px;
  min-width: 100px;
  display: inline-block;
  margin-bottom: 3px;
  padding-bottom: 0px;
}

.positionbox .el-collapse-item__header {
  font-size: 18px;
  font-weight: 200;
  padding-left: 5px;
  background: none;
}

.positionbox .el-collapse-item__wrap {
  background: none;
}

.el-table td {
  padding: 0px;
  word-break: keep-all;
  line-height: 10px;
  font-size: 90%;
}

.el-table input[type='text'] {
  height: 8px;
  margin-bottom: 0px;
  padding: 8 px;
}

.el-table .new-row {
  background: #f0f9eb;
  border: 4px red solid;
}

.el-table .changed-row {
  background: rgb(230, 242, 253);
}

.el-table .deleted-row {
  background: rgb(253, 231, 230);
}

.el-table .soft-deleted {
  color: lightgray;
  text-decoration: line-through;
}

.el-table .broken {
  color: red;
  text-decoration: line-through;
}

.new-row {
  background: #f0f9eb;
  border: 4px red solid;
}

.changed-row {
  background: rgb(230, 242, 253);
}

.deleted-row {
  background: rgb(253, 231, 230);
}

.soft-deleted {
  color: lightgray;
  text-decoration: line-through;
}

.broken {
  color: red;
  text-decoration: line-through;
}
</style>
