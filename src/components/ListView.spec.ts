import type { DbItem } from '@/types/db-item';
import { mount } from '@vue/test-utils';
import ListView from './ListView.vue';
import type { DbPosition, DbPositionsPerItem } from '@/types/db-position';
import { createPinia, setActivePinia } from 'pinia';

const base_items: DbItem[] = [
  {
    _id: 'sdf',
    _rev: 'se',
    template: 'case',
    time_created: new Date().toISOString(),
    time_modified: new Date().toISOString(),
    properties: { je: 'undefined' },
    prefix_created: 'v_test',
    prefix_converted: 'v_test',
    prefix_modified: 'v_test',
  },
  {
    _id: 'ddfg',
    _rev: 'sea',
    template: 'case',
    time_created: new Date().toISOString(),
    time_modified: new Date().toISOString(),
    properties: { je: 'undefined' },
    prefix_created: 'v_test',
    prefix_converted: 'v_test',
    prefix_modified: 'v_test',
  },
];
const positions: DbPosition[] = [
  {
    _id: 'wergdf', // item_identifier + '_' + timestamp (isotime)
    lat: 2,
    lon: 4,
    source: 'sdjf',
    timestamp: '2020-03-02',
    _rev: 'sdf',
    altitude: 23,
    heading: 23,
    speed: 37,
  },
] as DbPosition[];
const positionsPerItem: DbPositionsPerItem = {
  wergf: positions,
};

function createWrapperAndMocks(
  props = { base_items, positionsPerItem },
  data = {
    base_items: base_items as DbItem[],
    latestPositionTimestampPerItem: {} as Record<string, string>,
  }
) {
  setActivePinia(createPinia());
  const wrapper = mount(ListView, {
    data() {
      return {
        ...data,
      };
    },
    props: props,
    global: {
      mocks: {
        ...{
          $db: {
            getBaseItems: vi.fn(() => {
              return Promise.resolve({
                then: vi.fn(() => {}),
              });
            }),
            getPositionsForDbItemPromise: vi.fn((base_item) => {
              if (base_item._id === positions[0].item_id) {
                return Promise.resolve(positions);
              } else return Promise.resolve([] as DbPosition[]);
            }),
          },
        },
      },
    },
  });
  return {
    wrapper,
  };
}

describe('ListView', () => {
  it('renders', () => {
    expect(createWrapperAndMocks()).toBeTruthy();
  });
});
describe('Data Properties', () => {
  it('returns the correct active categories ', () => {
    expect(createWrapperAndMocks().wrapper.vm['activeCategories']).toEqual([
      'Ships',
    ]);
  });
});
describe('Computed Properties', () => {
  it('allCategories: categoryTabs.category_base_items returns array with two items.', () => {
    const { wrapper } = createWrapperAndMocks();
    expect(wrapper.vm['allCategories']).toMatchObject({
      case: {
        category_base_items: [
          { _id: base_items[0]._id },
          { _id: base_items[1]._id },
        ],
      },
    });
  });
});
