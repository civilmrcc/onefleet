import type { DbItem } from '@/types/db-item';
import { mount } from '@vue/test-utils';
import ListViewCategory from './ListViewCategory.vue';

const base_items: DbItem[] = [
  {
    _id: 'sdf',
    _rev: 'se',
    template: 'case',
    time_created: new Date().toISOString(),
    time_modified: new Date().toISOString(),
    properties: { je: 'undefined' },
    prefix_created: 'v_test',
    prefix_converted: 'v_test',
    prefix_modified: 'v_test',
  },
  {
    _id: 'ddfg',
    _rev: 'sea',
    template: 'case',
    time_created: new Date().toISOString(),
    time_modified: new Date().toISOString(),
    properties: { je: 'undefined' },
    prefix_created: 'v_test',
    prefix_converted: 'v_test',
    prefix_modified: 'v_test',
  },
];

const category = {
  fields: [{ name: 'name', test_field: 'test_field' }],
  category_base_items: base_items,
};
const latestPositionTimestampPerItem = {};

function createWrapperAndMocks(
  props = { category, latestPositionTimestampPerItem },
  data = {
    base_items: base_items as DbItem[],
    latestPositionTimestampPerItem: {} as Record<string, string>,
  }
) {
  const wrapper = mount(ListViewCategory, {
    data() {
      return {
        ...data,
      };
    },
    props: props,
    global: {
      mocks: {
        ...{
          $db: {
            getBaseItems: vi.fn(() => {
              return Promise.resolve({
                then: vi.fn(() => {}),
              });
            }),
          },
        },
      },
    },
  });
  return {
    wrapper,
  };
}

describe('ListViewCategory', () => {
  it('renders', () => {
    expect(createWrapperAndMocks()).toBeTruthy();
  });
});

describe('ListViewCategory', () => {
  it('setup creates data', () => {
    expect(createWrapperAndMocks().wrapper.vm['data'][0]).toHaveProperty('id');
    expect(createWrapperAndMocks().wrapper.vm['data'][0]).toHaveProperty(
      'date_created'
    );
    expect(createWrapperAndMocks().wrapper.vm['data'][0]).toHaveProperty(
      'last_modified'
    );
    expect(createWrapperAndMocks().wrapper.vm['data'][0]).toHaveProperty(
      'last_seen'
    );
    expect(createWrapperAndMocks().wrapper.vm['data'][1]).toHaveProperty('id');
    expect(createWrapperAndMocks().wrapper.vm['data'][1]).toHaveProperty(
      'date_created'
    );
    expect(createWrapperAndMocks().wrapper.vm['data'][1]).toHaveProperty(
      'last_modified'
    );
    expect(createWrapperAndMocks().wrapper.vm['data'][1]).toHaveProperty(
      'last_seen'
    );
  });
});

describe('ListViewCategory', () => {
  it('format header works', () => {
    expect(
      createWrapperAndMocks().wrapper.vm['formatHeader']({
        columns: [
          { title: 'abc_def' },
          { title: 'wer_rew_wer' },
          { title: 'ABC_BBC' },
          { title: 'abc' },
          { title: 'Abc' },
        ],
      })
    ).toEqual({
      columns: [
        { title: 'Abc Def' },
        { title: 'Wer Rew Wer' },
        { title: 'ABC BBC' },
        { title: 'Abc' },
        { title: 'Abc' },
      ],
    });
  });
});
