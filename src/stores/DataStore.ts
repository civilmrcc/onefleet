import all_filters from '../constants/filters';

import type { DbItem } from '@/types/db-item';
import type { DbPosition, DbPositionsPerItem } from '@/types/db-position';
import type {
  FilteredItemSection,
  ItemFilter,
  ItemFilterGroup,
} from '@/types/item-filter-and-sorting';

import { DatabaseEventKey } from '../utils/databasesApi/DatabaseApi';
import { DbWrapper } from '../utils/dbWrapper';
import storage from '../utils/storageWrapper';

import { defineStore } from 'pinia';
import { useShowStore } from './ShowStore';

type DataState = {
  baseItems: DbItem[];
  positionsPerItem: DbPositionsPerItem;
  filters: ItemFilter[][];
  /**
   * LeftNavigation Loading status
   */
  isLoading: boolean;
  /**
   * DbWrapper
   */
  $db: any;
  isInitialReplicationOfPositionsCompleted: boolean;
  /**
   * last time when data was pulled from database
   */
  lastDbChange?: Date;
  lastPositionTimestamp?: Date;
  /*
   * items changed? for listview to get the update
   */
  itemsReloaded?: Date;
};

/**
 * Holds data and provides function for data flow (excluding map-related tasks)
 */
export const useDataStore = defineStore<string, DataState, any, any>('data', {
  state: () => ({
    baseItems: [] as DbItem[],
    positionsPerItem: {} as DbPositionsPerItem,
    filters: [] as ItemFilter[][],
    isLoading: true as boolean,
    $db: new DbWrapper(),
    isInitialReplicationOfPositionsCompleted: false as boolean,
    lastDbChange: undefined,
    itemsReloaded: undefined,
    lastPositionTimestamp: undefined,
  }),

  getters: {
    /**
     * This computed value just returns an indexed version of the baseItems.
     * The index is the item's id field.
     */
    mappedBaseItems(): Record<string, DbItem> {
      return this.baseItems.reduce((map, item: DbItem) => {
        map[item._id] = item;
        return map;
      }, {} as Record<string, DbItem>);
    },

    /**
     * This computed value always contains an array of filtered item sections.
     * It is used by the LeftNavigationBar to display all items by tab section,
     * and by the MapArea to display the visible items in one layer per filter section.
     * Some map Popup components also use this to display available items in popups.
     */
    allFilteredItems(): FilteredItemSection[] {
      const all_filter_groups: ItemFilterGroup[] =
        all_filters.get_filter_groups;
      const filteredItemGroups: FilteredItemSection[] = [];

      // set up tabs for the tabs bar so that they are shown even before items are loaded
      for (const s_id in all_filter_groups) {
        // two-stage filtering for computing hidden items per section:
        const active_filters: ItemFilter[] = this.filters[s_id].filter(
          (f: ItemFilter) => f.active
        );
        const section_filters: ItemFilter[] = active_filters.filter(
          (f: ItemFilter) => f.always_active
        );

        // filter all items for this section:
        const sectionbaseItems: DbItem[] = this.baseItems.filter(
          (baseItem: DbItem) =>
            section_filters.every((section_filter) =>
              this.matchesFilter(baseItem, section_filter)
            )
        );

        // filter additional items based on active filters:
        const filteredBaseItems: DbItem[] = sectionbaseItems.filter(
          (baseItem) =>
            active_filters.every((active_filter) =>
              this.matchesFilter(baseItem, active_filter)
            )
        );

        filteredItemGroups.push({
          title: all_filter_groups[s_id].title,
          base_items: filteredBaseItems,
          hidden_items: sectionbaseItems.length - filteredBaseItems.length,
          filter_group_settings: all_filter_groups[s_id],
        });
      }
      return filteredItemGroups;
    },
  },

  actions: {
    // Load all active items initially
    async loadItemsInitial(): Promise<null[]> {
      this.isLoading = true;
      this.baseItems = [];
      const allFilterGroups: ItemFilterGroup[] = all_filters.get_filter_groups;

      // set up tabs for the tabs bar so that they are shown even before items are loaded
      const promises: (Promise<null> | null)[] = allFilterGroups.map(
        async (filter_group, s_id) => {
          // two-stage filtering for computing hidden items per section:
          const activeFilters: ItemFilter[] = this.filters[s_id].filter(
            (f: ItemFilter) => f.active
          );
          const sectionFilters: ItemFilter[] = activeFilters.filter(
            (f: ItemFilter) => f.always_active
          );
          const result: DbItem[] = await this.$db.getFilteredItems(
            sectionFilters
          );

          if (result) {
            const knownItemIds = Object.keys(this.mappedBaseItems);
            for (const dbItem of result) {
              if (!knownItemIds.includes(dbItem._id)) {
                this.baseItems.push(dbItem);
              }
            }
          }
          return null;
        }
      );
      return Promise.all(promises);
    },

    // Load changed items
    async loadItems(ids: string[]): Promise<void | DbItem[]> {
      this.isLoading = true;
      const result: DbItem[] = await this.$db.getItemsById(ids);
      if (result) {
        // Remove old items
        const baseItems: DbItem[] = this.baseItems.filter(
          (baseItem: DbItem) => !ids.includes(baseItem._id)
        );
        // Add updated items
        baseItems.push(...result);
        this.baseItems = baseItems;
      }
      return result;
    },

    // Load positions for all active items initially
    async loadPositionsForItemsInitial(): Promise<void> {
      this.positionsPerItem = await this.loadPositionsForItems(
        this.baseItems,
        {} as DbPositionsPerItem
      );
    },

    // Load positions for changed items
    async loadPositionsForChangedItems(baseItems: DbItem[]): Promise<void> {
      this.positionsPerItem = await this.loadPositionsForItems(
        baseItems,
        this.positionsPerItem
      );
    },

    async loadPositionsForItems(
      baseItems: DbItem[],
      positionsPerItem: DbPositionsPerItem
    ): Promise<DbPositionsPerItem> {
      const promises: (Promise<string | null> | null)[] = baseItems.map(
        async (baseItem: DbItem) => {
          const dbPositions: DbPosition[] =
            await this.$db.getPositionsForDbItem(
              baseItem,
              storage.get_map_tracks_config()
            );
          positionsPerItem[baseItem._id] = dbPositions;
          return dbPositions[0] ? dbPositions[0].timestamp : null;
        }
      );

      const latestPositionTimestamps = await Promise.all(promises);
      // tell TopNavigation the newest item position:
      this.updateLastPositionTimestamp(
        latestPositionTimestamps
          .sort()
          .reverse()
          .filter((timestamp) => timestamp != null)[0] || null
      );
      return positionsPerItem;
    },

    /** Start of Item Filter functions */
    initFilters(): void {
      /** Get filters and set up their active state */
      const allFilterGroups: ItemFilterGroup[] = all_filters.get_filter_groups;
      const filters: ItemFilter[][] = [];
      for (const section_index in allFilterGroups) {
        filters[section_index] = allFilterGroups[section_index].filters.map(
          (filter: ItemFilter) => {
            filter.active =
              filter.always_active || filter.initially_active ? true : false;
            return filter;
          }
        );
      }
      this.filters = filters;
    },
    matchesFilter(baseItem: DbItem, filter: ItemFilter): boolean {
      const item_value = filter.field
        .split('.')
        .reduce((o, i) => (o ?? {})[i], baseItem);
      return filter.values.some((filterValue) =>
        this.fitsFilterValue(String(item_value), filterValue)
      );
    },
    fitsFilterValue(item_value: string, filterValue: string): boolean {
      switch (filterValue[0]) {
        case '!':
          return item_value != filterValue.substring(1);
        default:
          return item_value == filterValue;
      }
    },
    /** End of Item Filter functions */
    async addEventListenersToItemsDb(databaseItems) {
      /**
       * Add a complete event listener for the items database.
       */
      databaseItems?.addEventListener(DatabaseEventKey.complete, async () => {
        try {
          await this.loadItemsInitial();
          useShowStore().setShowLoading(false);
          this.isLoading = false;
          //loadPositionsForItemsInitial needs to be called here
          //to make sure positions are loaded even if there are very few positions
        } catch (error) {
          console.error(error);
        }
      });
      /**
       * Add a change event listener for the items database.
       */
      databaseItems?.addEventListener(DatabaseEventKey.change, (info) => {
        if (info) {
          try {
            this.loadItems(info.change.docs.map((doc) => doc._id));
          } catch (error) {
            console.error(error);
          }
        }
      });
    },
    async addEventListenersToPositionsDb(databasePositions) {
      /**
       * Add a complete event listener for the positions database.
       */
      databasePositions?.addEventListener(DatabaseEventKey.complete, () => {
        try {
          this.updateLastDbChangeDate();
          this.loadPositionsForItemsInitial();
        } catch (error) {
          console.error(error);
        }
      });
      /**
       * Add a change event listener for positions database.
       * There are push and pull events. They need to be handled differently.
       */
      databasePositions?.addEventListener(DatabaseEventKey.change, (info) => {
        if (info) {
          try {
            if (info.direction === 'push') {
              this.positionsDbChangePushEvent(info);
            } else if (info.direction === 'pull') {
              this.positionsDbChangePullEvent(info);
            }
            this.updateLastDbChangeDate();
          } catch (error) {
            console.error(error);
          }
        }
      });
    },
    /**
     * The change push event.
     */
    positionsDbChangePushEvent(info) {
      const baseItems: DbItem[] = info.change.docs.map(
        (doc) => this.mappedBaseItems[(doc as DbPosition).item_id]
      );
      this.loadPositionsForChangedItems(baseItems);
    },
    /**
     * The change pull event.
     *
     * The event only returns the positions _id
     * and is missing the item_id. Therefore the positions doc
     * needs to be loaded first. Afterwards, then item_id can
     * be used similar to the push event to load the positions
     * into the ui.
     */
    async positionsDbChangePullEvent(info) {
      const baseItems: DbItem[] = [];
      const result = await this.$db.getPositionsById(
        info.change.docs.map((doc) => doc._id)
      );
      try {
        if (result instanceof Array) {
          result.forEach((position) =>
            baseItems.push(this.mappedBaseItems[position.item_id])
          );
        }
        this.loadPositionsForChangedItems(baseItems);
      } catch (error) {
        console.error(error);
      }
    },
    /**
     * @param id itemID
     * Reload item and its positions.
     * Fired when item is saved in EditItem component.
     */
    async reloadItem(id: string) {
      await this.loadItems([id]);
      this.loadPositionsForChangedItems([this.mappedBaseItems[id]]);
      this.triggerItemsReloaded();
    },
    reloadPositionsFromLocalDatabase(id: string) {
      if (id) {
        this.loadPositionsForChangedItems([this.mappedBaseItems[id]]);
      } else {
        this.loadPositionsForItemsInitial();
      }
    },
    setInitialReplicationOfPositionsCompleted(databaseName: string): void {
      if (databaseName === 'positions') {
        this.isInitialReplicationOfPositionsCompleted = true;
      }
    },
    updateLastDbChangeDate(): void {
      this.lastDbChange = new Date();
    },
    updateLastPositionTimestamp(val: string | null): void {
      this.lastPositionTimestamp = val;
    },
    triggerItemsReloaded(): void {
      this.itemsReloaded = new Date();
    },
  },
});
