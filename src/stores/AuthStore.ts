import { defineStore } from 'pinia';

type AuthState = {
  /**
   * Name of the currently logged in user
   */
  username?: string;
  isCookieSet: boolean;
};

/**
 * Holds data and provides function for authentication controls, e.g. user session related data
 */
export const useAuthStore = defineStore<string, AuthState, any, any>('auth', {
  state: () => ({
    username: undefined,
    isCookieSet: false,
  }),
  persist: {
    storage: sessionStorage,
  },
  getters: {},
  actions: {
    logout(): void {
      this.$reset();
      // more has to happen here
    },
    setUsername(val: string): void {
      this.username = val;
    },
    setCookie(val: boolean): void {
      this.isCookieSet = val;
    },
  },
});
