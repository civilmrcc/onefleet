import type { ItemFilterGroup } from '@/types/item-filter-and-sorting';

export const sarFilters: ItemFilterGroup = {
  title: 'SAR Zones',
  selectable_in_sidebar: false,
  selectable_on_map: false, // TODO: implement item zones
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['sarzone'],
      always_active: true,
    },
    {
      name: 'Only Central Med',
      field: 'properties.category',
      values: ['centralmed'],
    },
    {
      name: 'Only Show Trash',
      field: 'soft_deleted',
      values: ['true'],
    },
    {
      name: 'Hide Trash',
      field: 'soft_deleted',
      values: ['false', 'undefined'],
      initially_active: true,
    },
  ],
  sortings: [],
};
