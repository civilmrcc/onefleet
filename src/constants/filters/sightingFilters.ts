import type { ItemFilterGroup } from '@/types/item-filter-and-sorting';

export const sightingFilters: ItemFilterGroup = {
  title: 'Sightings',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['sighting', 'droppoint'],
      always_active: true,
    },
    {
      name: 'Status: Open',
      field: 'properties.status',
      values: ['open'],
      initially_active: true,
    },
    {
      name: 'Status: Closed',
      field: 'properties.status',
      values: ['closed'],
    },
    {
      name: 'Only Sightings',
      field: 'template',
      values: ['sighting'],
    },
    {
      name: 'Only Droppoints',
      field: 'template',
      values: ['droppoint'],
    },
    {
      name: 'Only Show Trash',
      field: 'soft_deleted',
      values: ['true'],
    },
    {
      name: 'Hide Trash',
      field: 'soft_deleted',
      values: ['false', 'undefined'],
      initially_active: true,
    },
  ],
  sortings: [],
};
