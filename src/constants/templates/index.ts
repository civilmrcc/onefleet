import type { ItemTemplate } from '@/types/item-template-schema';
import { aircraft } from './aircraft';
import { caseTemplate } from './case';
import { droppoint } from './droppoint';
import { landmark } from './landmark';
import { sighting } from './sighting';
import { ship } from './ship';
import type { DbItemProperty } from '@/types/db-item';

/**
 * The Templates singleton class.
 */
export class Templates {
  /**
   * The available templates.
   */
  public static readonly templates = {
    case: caseTemplate,
    ship: ship,
    aircraft,
    landmark,
    sighting,
    droppoint,
  };

  /**
   * Get all available template keys.
   * @returns The available template keys.
   */
  public get keys(): (keyof typeof Templates.templates)[] {
    return Object.keys(
      Templates.templates
    ) as (keyof typeof Templates.templates)[];
  }

  /**
   * Get those available template keys that are allowed to be created via the UI.
   * @returns The available template keys for UI creation.
   */
  public get uiKeys(): (keyof typeof Templates.templates)[] {
    return Object.entries(Templates.templates)
      .filter(([, template]) => template.allow_creation_via_ui)
      .map(
        ([templateName]) => templateName
      ) as (keyof typeof Templates.templates)[];
  }

  /**
   * Get all templates.
   * @returns All templates.
   */
  public getAll(): typeof Templates.templates {
    return Templates.templates;
  }

  /**
   * Get a single template by it's key.
   * @param name - The template key.
   * @returns A single template.
   */
  public get(name: keyof typeof Templates.templates): ItemTemplate {
    return Templates.templates[name];
  }

  /**
   * Get a template's field title from it's name
   */

  public getTitleFromName(
    templateKey: keyof typeof Templates.templates,
    name: string
  ): string | undefined {
    const template = this.get(templateKey);
    const field = template.fields.find((field) => field.name === name);
    return field ? field.title : undefined;
  }

  public getTypeByName(
    itemTemplate: ItemTemplate,
    name: string
  ): string | undefined {
    const field = itemTemplate.fields.find((field) => field.name === name);
    return field ? field.type : undefined;
  }

  public getDefaultValueByName(
    itemTemplate: ItemTemplate,
    name: string
  ): DbItemProperty | undefined {
    const field = itemTemplate.fields.find((field) => field.name === name);
    return field ? field.default_value : undefined;
  }

  public getOptionsByName(
    itemTemplate: ItemTemplate,
    name: string
  ): Record<string, string> | undefined {
    const field = itemTemplate.fields.find((field) => field.name === name);
    return field ? field.options : undefined;
  }
}

export default new Templates();
