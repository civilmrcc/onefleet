import type { ItemTemplate } from '@/types/item-template-schema';

export const sighting: ItemTemplate = {
  plural: 'Sightings',
  pouch_identifier: 'SIGHTING',
  enforce_initial_position: true,
  default_tracking_type: 'MANUAL',
  allow_creation_via_ui: true,
  type: 'line',
  default_marker_icon: 'sighting',
  always_show_markers: false,
  fields: [
    {
      name: 'name',
      title: 'Name',
      info: 'Name',
      field_group: 'Important',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Sighting Category',
      info: 'Sighting Category',
      type: 'select',
      options: {
        military: 'Military Spotting',
        coastguard: 'Coast Guard Spotting',
        unknown: 'Unknown',
      },
    },
    {
      name: 'status',
      title: 'Status',
      info: 'What is the current status of the sighting?',
      type: 'select',
      field_group: 'Important',
      options: {
        open: 'Open',
        closed: 'Closed',
      },
      default_value: 'open',
    },
    {
      name: 'flagstate',
      title: 'Possible Flagstate',
      info: 'Possible Flagstate',
      type: 'text',
    },
    {
      name: 'asset',
      title: 'Asset',
      info: 'Asset',
      type: 'text',
    },
    {
      name: 'time',
      title: 'Time',
      info: 'Time',
      type: 'datetime',
    },
    {
      name: 'comment',
      title: 'Comments',
      info: 'Comments',
      type: 'text',
    },
  ],
};
