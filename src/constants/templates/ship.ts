import type { ItemTemplate } from '@/types/item-template-schema';

export const ship: ItemTemplate = {
  plural: 'Ships',
  pouch_identifier: 'SHIP',
  enforce_initial_position: false,
  default_tracking_type: 'AIS',
  allow_creation_via_ui: true,
  type: 'line',
  default_marker_icon: 'ship',
  always_show_markers: false,
  fields: [
    {
      name: 'name',
      title: 'Ship Name',
      info: 'Ship Name',
      field_group: 'Important',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Affiliation Category',
      info: 'Affiliation Category',
      field_group: 'Important',
      type: 'select',
      options: {
        unknown: 'Still Unknown',
        import: 'Import',
        civilfleet: 'Civilfleet',
        commercial: 'Commercial',
        military: 'Military',
        other: 'Other',
      },
      default_value: 'unknown',
    },
    {
      name: 'flagstate',
      title: 'Flagstate',
      info: 'Flagstate',
      type: 'text',
    },
    {
      name: 'active',
      title: 'Active',
      info: 'Active',
      type: 'select',
      options: {
        true: 'true',
        false: 'false',
      },
      default_value: 'true',
    },
  ],
};
