import type { CalcTemplate } from '@/types/calc-template-schema';
import { alarmPhoneTemplate } from './alarm-phone';
/**
 * The Calc Templates singleton class.
 */
export class CalcTemplates {
  /**
   * The available calc templates.
   */
  public static readonly calcTemplates = {
    alarmPhoneTemplate,
  };

  /**
   * Get all available calc template keys.
   * @returns The available template keys.
   */
  public get keys(): (keyof typeof CalcTemplates.calcTemplates)[] {
    return Object.keys(
      CalcTemplates.calcTemplates
    ) as (keyof typeof CalcTemplates.calcTemplates)[];
  }

  /**
   * Only get calc templates currently supported by the tool
   * @returns The calc templates supported by the tool.
   */
  public get supportedTemplates(): CalcTemplate[] {
    return Object.values(CalcTemplates.calcTemplates).filter(
      (value) => !value.deprecated
    );
  }

  /**
   * Get all templates.
   * @returns All templates.
   */
  public getAll(): typeof CalcTemplates.calcTemplates {
    return CalcTemplates.calcTemplates;
  }

  public getTemplateInfoSheetNames(): string[] {
    const availableTemplates = this.getAll();
    return Object.values(availableTemplates).map(
      (obj) => obj.templateInfoSheetname
    );
  }

  /**
   * Get a single calc template using its name and version.
   * @param name - The calc template name.
   * @param version - The calc template version
   * @returns A single calc template or an error message.
   */
  public get(name: string, version: string): CalcTemplate | string {
    for (const template of Object.values(CalcTemplates.calcTemplates)) {
      template.deprecated;
      if (
        template.name === name &&
        template.version === version &&
        template.deprecated == false
      ) {
        return template;
      } else if (
        template.name === name &&
        template.version === version &&
        template.deprecated == true
      ) {
        return `The template information found matches a deprecated template. 
        Please refer to the documentation for the current actives templates and update your file accordingly`;
      }
    }
    return `The template information found does not match any known templates`;
  }
}

export default new CalcTemplates();
