import storageWrapper from './storageWrapper';

describe('storageWrapper', () => {
  const STORAGE_KEY = 'test';
  const STORAGE_VALUE = '123';

  describe('.set()', () => {
    it('should successfully set a new item in storage', () => {
      expect(() => {
        storageWrapper.set(STORAGE_KEY, STORAGE_VALUE);
      }).not.toThrow();
    });
  });

  describe('.get()', () => {
    it('should successfully retrieve an existing item from storage', () => {
      storageWrapper.set(STORAGE_KEY, STORAGE_VALUE);
      expect(storageWrapper.get(STORAGE_KEY) === STORAGE_VALUE).toBe(true);
    });
  });
});
