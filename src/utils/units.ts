/**
 * Convert seconds to milliseconds.
 *
 * @param seconds - The seconds to convert to milliseconds.
 * @returns The value in milliseconds.
 */
export function secondsToMilliseconds(seconds: number): number {
  return seconds * 1000;
}

/**
 * Convert minutes to milliseconds.
 *
 * @param minutes - The minutes to convert to milliseconds.
 * @returns The value in milliseconds.
 */
export function minutesToMilliseconds(minutes: number): number {
  const seconds = minutes * 60;
  return secondsToMilliseconds(seconds);
}
