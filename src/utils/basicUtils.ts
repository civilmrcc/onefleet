/**
 * A class for general utility functions
 */
export default class BasicUtils {
  /**
   * Gets the value at path of object.
   * Note: If provided path does not exist inside the object js will generate error.
   *
   * @remarks
   * From https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore#_get
   *
   * @param obj - object to query
   * @param path - e.g. 'properties.name'
   * @param defaultValue - default return value if queried value is undefined
   */
  public static get(obj, path, defaultValue = undefined): any {
    const travel = (regexp) =>
      String.prototype.split
        .call(path, regexp)
        .filter(Boolean)
        .reduce(
          (res, key) => (res !== null && res !== undefined ? res[key] : res),
          obj
        );
    const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
    return result === undefined || result === obj ? defaultValue : result;
  }
}
