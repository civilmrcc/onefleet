export interface UrlArgs {
  /**
   * The URL protocol to use.
   */
  protocol: string;
  /**
   * The URL host.
   */
  host: string;
  /**
   * The URL port.
   */
  port: number;
}

/**
 * Generate an URL from the given arguments.
 *
 * @param args - The URL arguments.
 * @returns The URL.
 */
export function generateUrl({ protocol, host, port }: UrlArgs): string {
  // strip the colon from the protocol if it has one (e.g. when reading
  // from window.location.protocol)
  if (protocol.endsWith(':')) {
    protocol = protocol.slice(0, -1);
  }
  return `${protocol}://${host}:${port}`;
}
