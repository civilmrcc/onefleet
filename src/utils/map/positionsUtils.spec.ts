import { whatDaysShouldBeReplicated } from './positionUtils';
import type { MapTracksConfig } from '@/types/config';

const mapTracksConfigNbOfDaysUpToPast: MapTracksConfig = {
  type: 'number_of_days',
  oldest_date_iso: '2023-06-03T00:00:00.000Z',
  newest_date_iso: '2023-06-10T00:00:00.000Z',
  show_past_days: 4,
  length_limit: 10000,
};

const mapTracksConfigNbOfDaysUpToNow: MapTracksConfig = {
  type: 'number_of_days',
  oldest_date_iso: '2023-06-03T00:00:00.000Z',
  newest_date_iso: null,
  show_past_days: 4,
  length_limit: 10000,
};

const mapTracksConfigFullDateRange: MapTracksConfig = {
  type: 'date_range',
  oldest_date_iso: '2023-06-03T00:00:00.000Z',
  newest_date_iso: '2023-06-10T00:00:00.000Z',
  show_past_days: 3,
  length_limit: 10000,
};

const mapTracksConfigOnlyOldestDateRange: MapTracksConfig = {
  type: 'date_range',
  oldest_date_iso: '2023-06-10T00:00:00.000Z',
  newest_date_iso: null,
  show_past_days: 3,
  length_limit: 10000,
};

vi.setSystemTime('2023-06-16T00:00:00.000Z');

describe('positionUtils', () => {
  describe('whatDaysShouldBeReplicated', () => {
    describe('number_of_days up to given endDate', () => {
      localStorage.setItem(
        'map_tracks',
        JSON.stringify(mapTracksConfigNbOfDaysUpToPast)
      );
      const [startDateIso, endDateIso] = whatDaysShouldBeReplicated();
      it('Returns the correct startDateIso before newest_date_iso with number_of_days difference', () => {
        expect(startDateIso).toEqual('2023-06-06T00:00:00.000Z');
      });

      it('Returns the given endDateIso when given a number_of_days MapTracksConfig with a newest_date_iso', () => {
        expect(endDateIso).toEqual('2023-06-10T00:00:00.000Z');
      });
    });
    describe('number_of_days up to Now', () => {
      localStorage.setItem(
        'map_tracks',
        JSON.stringify(mapTracksConfigNbOfDaysUpToNow)
      );
      const [startDateIso, endDateIso] = whatDaysShouldBeReplicated();
      it('Returns the correct startDateIso before NOW when given a number_of_days MapTracksConfig', () => {
        expect(startDateIso).toEqual('2023-06-12T00:00:00.000Z');
      });

      it('Returns an undefined endDateIso when given a number_of_days MapTracksConfig without a newest_date_iso', () => {
        expect(endDateIso).toBeTypeOf('undefined');
      });
    });
    describe('date_range up to given endDate', () => {
      localStorage.setItem(
        'map_tracks',
        JSON.stringify(mapTracksConfigFullDateRange)
      );
      const [fullStartDateIso, fullEndDateIso] = whatDaysShouldBeReplicated();
      it('Returns the correct startDateIso and endDateIso when given a full date_range MapTracksConfig', () => {
        expect([fullStartDateIso, fullEndDateIso]).toEqual([
          '2023-06-03T00:00:00.000Z',
          '2023-06-10T00:00:00.000Z',
        ]);
      });
    });
    describe('date_range up to Now', () => {
      localStorage.setItem(
        'map_tracks',
        JSON.stringify(mapTracksConfigOnlyOldestDateRange)
      );

      const [partialStartDateIso, partialEndDateIso] =
        whatDaysShouldBeReplicated();
      it('Returns correct startDateIso and endDateIso when given a date_range MapTracksConfig with only oldest_date_iso', () => {
        expect([partialStartDateIso, partialEndDateIso]).toEqual([
          '2023-06-10T00:00:00.000Z',
          undefined,
        ]);
      });
    });
  });
});
