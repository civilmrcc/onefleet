import * as L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import type * as geojson from 'geojson';

import { formatTimestamp } from '@/utils/datetimeUtils';

import { static_geojson, dynamicGeojson } from './layerData';

/**
 * Loads the static info layers which exist as geojson files.
 * This only needs to be loaded once at startup, i.e., page reload in browser.
 */
export function loadStaticGeoJSONLayers() {
  for (const [path, layer] of Object.entries(static_geojson)) {
    updateGeoJSONLayer(path, layer);
  }
}

/**
 * Loads the dynamic layers which exist as geojson files.
 * As the contents of these files change over time, they need to be reloaded
 * every few minutes so that changes show up on the map.
 */
export function loadDynamicGeoJSONLayers() {
  dynamicGeojson.forEach((element) => {
    updateGeoJSONLayer(element.path, element.layer, element.layer_info);
  });
}

/**
 * Update the given geojson layer with new data from the given URL.
 * If URL could not be reached, do nothing and keep old contents.
 *
 * This function can either be called once at startup (for constant data like SAR zones)
 * or repeatedly via a timer (for regularly updating data such as the "All Ships" layer).
 *
 * @param url The URL to fetch the geojson data from.
 * @param layer The layer into which the geojson data should be written.
 * @param info_id A HTML element ID telling us which info text should actually be updated.
 */
export function updateGeoJSONLayer(
  url: string,
  layer: L.GeoJSON,
  info_id: string | undefined = undefined
) {
  fetch(url)
    .then((res: Response) => res.json())
    .then((geojson_data) => {
      layer.clearLayers();
      layer.options = {
        pointToLayer: _pointToLayer,
        onEachFeature: _onEachFeature,
        style: geojson_data['style'],
      };
      layer.addData(geojson_data);
      if (info_id) {
        _updateLayerInfo(geojson_data['features'] ?? [], info_id);
      }
    })
    .catch(() => {
      // Do nothing. Ok maybe just update info text if given and there is no previous data.
      if (info_id && layer.getLayers().length == 0) {
        _updateLayerInfo([], info_id);
      }
    });
}

/**
 * Constructs a leaflet marker from a geojson feature.
 * @param feature The geojson feature that needs to be transformed into a leaflet layer.
 * @param latlng The latitude / longitude position.
 * @returns A leaflet circleMarker that uses properties defined by the given geojson feature.
 */
function _pointToLayer(feature: geojson.Feature, latlng: L.LatLngExpression) {
  let geojsonMarkerOptions = {
    radius: 4,
    fillColor: feature.properties ? feature.properties['marker-color'] : 'blue',
    color: '#000',
    weight: 0.5,
    opacity: 1,
    fillOpacity: 0.8,
  };
  if (feature['markerStyle']) {
    geojsonMarkerOptions = feature['markerStyle'];
  }
  return L.circleMarker(latlng, geojsonMarkerOptions);
}

/**
 * Postprosessing on each new marker, such as binding a popup to it.
 * Other (short!) actions may also be done here in the future.
 * @param feature The geojson feature with all the information.
 * @param layer The layer/marker that this information is used for.
 */
function _onEachFeature(feature: geojson.Feature, layer: L.Layer) {
  if (feature.properties) {
    // quick workaround before we implement proper AllItemsMarkerPopup.vue:
    let popup_contents = '';
    Object.keys(feature.properties).forEach((key) => {
      if (null !== feature.properties?.[key]) {
        popup_contents += `${key}: ${feature.properties?.[key]}<br>`;
      }
    });
    layer.bindPopup(popup_contents);
    layer.bindTooltip(feature.properties?.['Name'], {
      permanent: false, //layer labels are deactivated by default
      direction: 'center',
      className: 'geojson-labels',
    });
  }
}

/**
 * Update LayerControl texts to reflect number of geojson features.
 * Also set text color and timestamp-tooltip accordingly.
 * @param features The geojson features to be used
 * @param info_id A HTML element ID telling us which info text should actually be updated.
 */
function _updateLayerInfo(features: Record<any, any>[], info_id: string) {
  const ts_field = 'position_received';
  const no_timestamp = '0'; // needs to be lexicographically smaller than any iso timestamp string
  const latest_feature_timestamp = features.reduce(
    (max, feature) =>
      feature.properties[ts_field] > max ? feature.properties[ts_field] : max,
    no_timestamp
  );
  const elem = L.DomUtil.get(info_id);
  if (elem) {
    elem.textContent = `(${features.length})`;
    if (elem.parentElement) {
      elem.parentElement.title =
        latest_feature_timestamp != no_timestamp
          ? 'Latest timestamp in layer: ' +
            formatTimestamp(latest_feature_timestamp)
          : 'No timestamps available for this layer';
      elem.parentElement.style.color = features.length > 0 ? 'black' : 'grey';
    }
  }
}

/**
 * Sets up listeners for when the labelsLayer is added to or removed from the map.
 * - On add, all marker-tooltips of each loadedLayer are set to permanent
 * - On remove, all marker-tooltips of each loadedLayer are set to only show on hover.
 * @param labelsLayer The layers that gets added and removed from the map via the LayerControl. It does not (seem to) contain any actual labels.
 * @param loadedLayers The array of layers that each have a bunch of marklers with tooltips, where tooltip options are to be changed.
 */
export function setupLabelListeners(
  labelsLayer: L.LayerGroup,
  loadedLayers: L.LayerGroup[]
) {
  labelsLayer.on('add', () => {
    loadedLayers.forEach(setTooltipVisibilitiesToPermanent);
  });
  labelsLayer.on('remove', () => {
    loadedLayers.forEach(setTooltipVisibilitiesToHover);
  });
}

/**
 * Add map labels for a certain layergroup
 *
 * @param layerGroup The LayerGroup to add the labels (tooltips) from.
 */
function setTooltipVisibilitiesToPermanent(layerGroup: L.LayerGroup) {
  layerGroup.eachLayer((marker) => {
    const tooltip = marker.getTooltip();
    if (tooltip) {
      marker.unbindTooltip();
      tooltip.options.permanent = true;
      marker.bindTooltip(tooltip);
    }
  });
}

/**
 * Remove map labels for a certain layergroup
 *
 * @param layerGroup The LayerGroup to remove the labels (tooltips) from.
 */
function setTooltipVisibilitiesToHover(layerGroup: L.LayerGroup) {
  layerGroup.eachLayer((marker) => {
    const tooltip = marker.getTooltip();
    if (tooltip) {
      marker.unbindTooltip();
      tooltip.options.permanent = false;
      marker.bindTooltip(tooltip);
    }
  });
}
