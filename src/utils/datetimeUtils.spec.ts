import {
  cutTimestamp,
  formatTimestamp,
  overrideTimezoneWithUtc,
  addOffsetToDatetime,
  computeOldestDateIso,
} from './datetimeUtils';
import { DateTime, FixedOffsetZone } from 'luxon';

describe('datetimeUtilsWrapper', () => {
  describe('.overrideTimezoneWithUtc()', () => {
    // A date selected in el-date-picker with client timezone set to CEST turns '2022-04-21T12:36:04.000Z' to '2022-04-21T10:36:04.000Z'
    const iso_local = '2022-04-21T10:36:04.000Z';
    const offsetInMinutes = new Date(iso_local).getTimezoneOffset();
    const luxon_local = DateTime.fromISO(iso_local, {
      zone: new FixedOffsetZone(offsetInMinutes),
    });

    const luxon_utc = luxon_local.minus({ hours: offsetInMinutes / 60 });
    it('should successfully override datetime captured in local timezone but stored as UTC time with actual UTC time', () => {
      expect(
        overrideTimezoneWithUtc(luxon_local.toJSDate()).toISOString()
      ).toEqual(luxon_utc.toJSDate().toISOString());
    });
  });

  describe('.addOffsetToDatetime()', () => {
    const iso_utc = '2022-04-21T10:36:04.000Z';
    const offsetInMinutes = new Date(iso_utc).getTimezoneOffset();
    const luxon_utc = DateTime.fromISO(iso_utc, { zone: 'utc' });
    const luxon_local = luxon_utc.plus({ minutes: offsetInMinutes });

    it('should successfully offset timezone with given offset', () => {
      expect(
        addOffsetToDatetime(luxon_utc.toJSDate(), offsetInMinutes).toISOString()
      ).toEqual(luxon_local.toJSDate().toISOString());
    });
  });

  describe('.formatTimestamp()', () => {
    // Deals with issue of NNBSP used in some places on some machines
    function replaceNarrowNoBreakSpaceWithSpace(aString: string): string {
      return aString.replace('\u202f', ' ');
    }

    const datetime = '2022-04-21T10:36:04.000Z';
    it('shows `no time` when datetime is null', () => {
      expect(formatTimestamp(null)).toEqual('no time');
    });
    it('shows only time for ISO string', () => {
      expect(
        replaceNarrowNoBreakSpaceWithSpace(
          formatTimestamp(datetime, false, 'utc')
        )
      ).toEqual('10:36 AM UTC');
    });
    it('shows date and time for ISO string', () => {
      expect(
        replaceNarrowNoBreakSpaceWithSpace(
          formatTimestamp(datetime, true, 'utc')
        )
      ).toEqual('10:36 AM UTC (Thu, Apr 21, 2022)');
    });
    it('shows only time for Date object', () => {
      expect(
        replaceNarrowNoBreakSpaceWithSpace(
          formatTimestamp(new Date(datetime), false, 'utc')
        )
      ).toEqual('10:36 AM UTC');
    });
    it('shows date and time for Date object', () => {
      expect(
        replaceNarrowNoBreakSpaceWithSpace(
          formatTimestamp(new Date(datetime), true, 'utc')
        )
      ).toEqual('10:36 AM UTC (Thu, Apr 21, 2022)');
    });
  });

  describe('.cutTimestamp()', () => {
    const datetime = '2022-04-21T10:36:04.000Z';
    it('returns "" when datetime is null', () => {
      expect(cutTimestamp(undefined)).toEqual('');
    });
    it('shows correct string for test string', () => {
      expect(cutTimestamp(datetime)).toEqual('2022-04-21 10:36');
    });
  });

  describe('computeOldestDateIso()', () => {
    vi.setSystemTime('2023-06-16T00:00:00.000Z');
    const daysToSubtract = 3;
    const newestDateIso = new Date(Date.now());
    const newestDateIsoString = newestDateIso.toISOString();

    it('Returns the correct ISO Date string when given an ISO date', () => {
      expect(computeOldestDateIso(daysToSubtract, newestDateIso)).toEqual(
        '2023-06-13T00:00:00.000Z'
      );
    });

    it('Returns the correct ISO Date string when given an ISO date string', () => {
      expect(computeOldestDateIso(daysToSubtract, newestDateIsoString)).toEqual(
        '2023-06-13T00:00:00.000Z'
      );
    });

    it('Returns the correct ISO Date string when only given daysToSubtract', () => {
      expect(computeOldestDateIso(daysToSubtract)).toEqual(
        '2023-06-13T00:00:00.000Z'
      );
    });
  });
});
