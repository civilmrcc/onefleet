import type { CalcTemplate } from '@/types/calc-template-schema';
import { CalcImportTool } from './calcUtils';
import { CalcTemplates } from '@/constants/calc_templates';

import { setActivePinia, createPinia } from 'pinia';
import { useDataStore } from '@/stores/DataStore';

setActivePinia(createPinia());
const importTool = new CalcImportTool();
const templates = new CalcTemplates();

beforeEach(() => {
  importTool.template = templates.get('AP', '0.1') as CalcTemplate;
  importTool.initialized = true;
  importTool.dataJson = [
    {
      B: 146,
      C: 21290702,
      E: 'rubber',
      F: 'black',
      G: 78,
      H: 'LIBYA',
      I: null,
      J: '2022-02-02T00:00:00.000Z',
      K: '@15:16',
      L: 'SAR1',
      M: '41.40338, 2.17403E @ 2022-09-29T16:06:30Z',
      O: 3,
      P: 1,
      R: 'autonomous',
      S: '35.406961N, 14.238281E @ 2022-01-18T11:33:13',
      W: '2022-02-01T00:00:00.000Z',
      Z: 'https://twitter.com/user/status/0000000000000000000|||https://twitter.com/user/status/0000000000000000000',
    },
    {
      B: null,
      C: null,
      E: null,
      F: null,
      G: null,
      H: 'TUN',
      I: 'Mahdia',
      J: null,
      K: null,
      L: null,
      M: null,
      O: null,
      P: 5,
      R: 'SHIPWRECK MAHDIA',
      S: null,
      W: '2022-01-31T23:00:00.000Z',
      Z: 'Red cross Sfax',
    },
    {
      B: 147,
      C: null,
      E: 'Iron',
      F: null,
      G: 39,
      H: 'TUN',
      I: 'Sfax',
      J: null,
      K: '@15:16',
      L: null,
      M: null,
      O: null,
      P: null,
      R: null,
      S: null,
      W: '2022-02-01T00:00:00.000Z',
      Z: 'POB',
    },
  ];
  importTool.dataStore = useDataStore();
  importTool['createBlankCasesArray']();
});

afterEach(() => {
  importTool.reset();
});

describe('AP@0.1 template', () => {
  describe('parseMandatoryFields()', () => {
    it('Correctly checks that incomplete cases are flagged as such', () => {
      importTool['parseMandatoryFields']();
      expect(importTool.incompleteRows).toEqual([
        'complete',
        'incomplete',
        'complete',
      ]);
    });
  });

  describe('parseCasesAndPositions', () => {
    it('Correctly marks partially full cases while keeping incomplete cases as such', () => {
      importTool.incompleteRows = ['complete', 'incomplete', 'complete'];
      importTool['parseCasesAndPositions']();
      expect(importTool.incompleteRows).toEqual([
        'complete',
        'incomplete',
        'partial',
      ]);
    });
  });

  describe('parseDateTimeFields()', () => {
    it('Correctly parses a datetime field with no fallback', () => {
      const timestampFieldWithNoFallback = importTool.template?.CalcFields.find(
        (field) => field.caseTemplateFieldEquivalentName === 'time_of_departure'
      );
      const output = importTool['parseDateTimeFields'](
        importTool.dataJson[0],
        timestampFieldWithNoFallback!
      );

      expect(output).toEqual({
        success: true,
        result: '2022-02-02T15:16:00.000Z',
      });
    });
  });

  it('Correctly parses a datetime field where the date fallback cell is needed', () => {
    const timestampFieldWithNoFallback = importTool.template?.CalcFields.find(
      (field) => field.caseTemplateFieldEquivalentName === 'occurred_at'
    );
    const output = importTool['parseDateTimeFields'](
      importTool.dataJson[2],
      timestampFieldWithNoFallback!
    );

    expect(output).toEqual({
      success: true,
      result: '2022-02-01T00:00:00.000Z',
    });
  });

  describe('parseNumberFields()', () => {
    it('Correctly parses a number field with no fallback from a single column', () => {
      const singleNumberField = importTool.template?.CalcFields.find(
        (field) => field.caseTemplateFieldEquivalentName === 'pob_total'
      );
      const output = importTool['parseNumberFields'](
        importTool.dataJson[0],
        singleNumberField!
      );
      expect(output).toEqual({
        success: true,
        result: 78,
      });
    });

    it('Correctly parses a number field with no fallback from multiple columns', () => {
      const multipleNumberField = importTool.template?.CalcFields.find(
        (field) => field.caseTemplateFieldEquivalentName === 'people_dead'
      );
      const output = importTool['parseNumberFields'](
        importTool.dataJson[0],
        multipleNumberField!
      );
      expect(output).toEqual({
        success: true,
        result: 4,
      });
    });
  });

  describe('parseSelectFields', () => {
    it('Correctly parses a select field when the row contains a value', () => {
      const singleSelectField = importTool.template?.CalcFields.find(
        (field) => field.caseTemplateFieldEquivalentName === 'sar_region'
      );
      const output = importTool['parseSelectFields'](
        importTool.dataJson[0],
        singleSelectField!
      );
      expect(output).toEqual({
        success: true,
        result: 'sar1',
      });
    });
    it("Correctly assigns the calc template's default select value when the row is null", () => {
      const singleSelectField = importTool.template?.CalcFields.find(
        (field) => field.caseTemplateFieldEquivalentName === 'outcome'
      );
      const output = importTool['parseSelectFields'](
        importTool.dataJson[2],
        singleSelectField!
      );
      expect(output).toEqual({
        success: true,
        result: 'unknown',
      });
    });
  });

  describe('parseUrlFields', () => {
    it('Correctly parses two urls separated by ||| ', () => {
      const urlField = importTool.template?.CalcFields.find(
        (field) => field.caseTemplateFieldEquivalentName === 'url'
      );
      const output = importTool['parseUrlsFields'](
        importTool.dataJson[0],
        urlField!
      );
      expect(output).toEqual({
        result:
          'https://twitter.com/user/status/0000000000000000000\nhttps://twitter.com/user/status/0000000000000000000',
        success: true,
      });
    });
  });

  describe('parsePositions', () => {
    it('Parses all possible positions from a row and returns them in an array', () => {
      const output = importTool['parsePositions'](importTool.dataJson[0]);
      expect(output).toEqual([
        {
          lat: '41.40338',
          lon: '2.17403',
          source: 'onefleet',
          timestamp: '2022-09-29T16:06:30.000Z',
        },
        {
          lat: '35.406961',
          lon: '14.238281',
          source: 'onefleet',
          timestamp: '2022-01-18T11:33:13.000Z',
        },
      ]);
    });
  });
  describe('deleteIncompleteRows', () => {
    it('Correctly deletes cases that are marked as incomplete', () => {
      importTool.parsedCases.forEach((value) => {
        value.map_settings = {
          marker_color: '#0ccf60',
          marker_icon: 'case',
          get_historical_data_since: -1,
          tracking_type: 'MANUAL',
          tracking_id: '',
          tracking_token: '',
        };
      });
      importTool['parseMandatoryFields']();
      importTool['parseCasesAndPositions']();
      importTool['deleteIncompleteRows']();
      expect(importTool.parsedCases).toEqual([
        {
          template: 'case',
          properties: {
            status: 'ready_for_documentation',
            name: '146',
            occurred_at: '2022-02-02T15:16:00.000Z',
            boat_type: 'rubber',
            boat_color: 'black',
            phonenumber: '21290702',
            place_of_departure: 'LIBYA',
            sar_region: 'sar1',
            time_of_departure: '2022-02-02T15:16:00.000Z',
            pob_total: 78,
            people_dead: 4,
            url: 'https://twitter.com/user/status/0000000000000000000\nhttps://twitter.com/user/status/0000000000000000000',
            outcome: 'autonomous',
          },
          map_settings: {
            marker_color: '#0ccf60',
            marker_icon: 'case',
            get_historical_data_since: -1,
            tracking_type: 'MANUAL',
            tracking_id: '',
            tracking_token: '',
          },
          import_template: 'AP@0.1',
        },
        {
          template: 'case',
          properties: {
            status: 'ready_for_documentation',
            name: '147',
            occurred_at: '2022-02-01T00:00:00.000Z',
            boat_type: 'iron',
            boat_color: 'unknown',
            phonenumber: null,
            place_of_departure: 'Sfax',
            sar_region: 'unknown',
            time_of_departure: null,
            pob_total: 39,
            people_dead: undefined,
            url: undefined,
            outcome: 'unknown',
          },
          map_settings: {
            marker_color: '#0ccf60',
            marker_icon: 'case',
            get_historical_data_since: -1,
            tracking_type: 'MANUAL',
            tracking_id: '',
            tracking_token: '',
          },
          import_template: 'AP@0.1',
        },
      ]);
    });
    it('Correctly deletes positions from cases that are marked as incomplete', () => {
      const expectedPositionsMap = new Map();
      expectedPositionsMap.set(0, [
        {
          lat: '41.40338',
          lon: '2.17403',
          timestamp: '2022-09-29T16:06:30.000Z',
          source: 'onefleet',
        },
        {
          lat: '35.406961',
          lon: '14.238281',
          timestamp: '2022-01-18T11:33:13.000Z',
          source: 'onefleet',
        },
      ]);
      expectedPositionsMap.set(2, null);

      importTool['parseMandatoryFields']();
      importTool['parseCasesAndPositions']();
      importTool['deleteIncompleteRows']();
      expect(importTool.parsedPositions).toEqual(expectedPositionsMap);
    });
  });

  describe('Clean incomplete positions', () => {
    it('Deletes positions that are undefined before saving to the DB to avoid an error being thrown', () => {
      importTool.parsedCases = [];
      importTool.incompleteRows = [];
      const expectedPositionsMap = new Map();
      expectedPositionsMap.set(0, [
        {
          lat: '41.40338',
          lon: '2.17403',
          timestamp: '2022-09-29T16:06:30.000Z',
          source: 'onefleet',
        },
        {
          lat: '35.406961',
          lon: '14.238281',
          timestamp: '2022-01-18T11:33:13.000Z',
          source: 'onefleet',
        },
      ]);
      expectedPositionsMap.set(1, null);
      expectedPositionsMap.set(2, null);
      expectedPositionsMap.set(3, [
        {
          lat: '33.58',
          lon: '12.65',
          timestamp: '2024-02-02T16:51:00.000Z',
          source: 'onefleet',
        },
      ]);
      importTool.dataJson.push({
        A: 'yes',
        B: 'AP0128-2024',
        C: 8821621287379,
        D: null,
        E: 'rubber',
        F: 'green',
        G: 56,
        H: 'Libya ',
        I: 'Zawiyah',
        J: '2024-02-01T00:00:00.000Z',
        K: null,
        L: 'sar1',
        M: '33.58N, 12.65E @ 2024-02-02T16:51:00Z',
        N: null,
        O: null,
        P: null,
        Q: null,
        R: 'returned',
        S: 'N33 35 08 E012 39 24 @ 17.51 CET 02 FEB',
        T: null,
        U: null,
        V: null,
        W: null,
        X: null,
        Y: null,
        Z: null,
      });
      importTool['createBlankCasesArray']();
      importTool['parseMandatoryFields']();
      importTool['parseCasesAndPositions']();
      importTool['cleanIncompletePositions']();
      expect(importTool.parsedPositions).toEqual(expectedPositionsMap);
    });
  });
});
