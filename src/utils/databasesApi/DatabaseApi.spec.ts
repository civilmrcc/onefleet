import { expect } from 'vitest';
import { DatabaseApi } from './DatabaseApi';
import { setupRemotePouchDbDatabase } from './setup';

/** We use setupRemotePouchDbDatabase() twice because otherwise
 * we get a local db folder and no actual
 * DB is used in the tests, they're just necessary to create an
 * instance of DatabaseApi */
const localItems = setupRemotePouchDbDatabase({
  protocol: 'https',
  host: 'localhost',
  port: 5984,
  databasePrefix: 'v0_3_',
  databaseName: 'items',
});
const remoteItems = setupRemotePouchDbDatabase({
  protocol: 'https',
  host: 'localhost',
  port: 5984,
  databasePrefix: 'v0_3_',
  databaseName: 'items',
});
const dbApiItems = new DatabaseApi({
  name: 'items',
  local: localItems,
  remote: remoteItems,
});

const localPositions = setupRemotePouchDbDatabase({
  protocol: 'https',
  host: 'localhost',
  port: 5984,
  databasePrefix: 'v0_3_',
  databaseName: 'positions',
});
const remotePositions = setupRemotePouchDbDatabase({
  protocol: 'https',
  host: 'localhost',
  port: 5984,
  databasePrefix: 'v0_3_',
  databaseName: 'positions',
});
const dbApiPositions = new DatabaseApi({
  name: 'items',
  local: localPositions,
  remote: remotePositions,
});

describe('DatabaseApi', () => {
  describe('getFilterOptions', () => {
    describe('Items DB', () => {
      describe('Pull', () => {
        it('returns an undefined selector when asking for a pull items db selector', () => {
          const pullItemsSelector = dbApiItems['getPullFilterOptions'](
            'items',
            '2023-06-13T00:00:00.000Z',
            '2023-06-16T00:00:00.000Z'
          );
          expect(pullItemsSelector.selector).toBeTypeOf('undefined');
        });
      });
      describe('Push', () => {
        it('returns an undefined selector when asking for a push items db selector', () => {
          const pushItemsSelector = dbApiItems['getPushFilterOptions']('items');
          expect(pushItemsSelector.selector).toBeTypeOf('undefined');
        });
      });
    });
  });
  describe('Positions DB', () => {
    describe('Pull', () => {
      it('Returns a selector with both a $gte and a $lte when given a defined startDateIso and endDateIso', () => {
        const pullPositionsFullSelector = dbApiPositions[
          'getPullFilterOptions'
        ]('positions', '2023-06-13T00:00:00.000Z', '2023-06-16T00:00:00.000Z');
        expect(
          pullPositionsFullSelector.selector!['$or'][0]['item_id'].$regex
        ).toEqual('LANDMARK.*');
        expect(
          pullPositionsFullSelector.selector!['$or'][1]['timestamp'].$gte
        ).toEqual('2023-06-13T00:00:00.000Z');
        expect(
          pullPositionsFullSelector.selector!['$or'][1]['timestamp'].$lte
        ).toEqual('2023-06-16T00:00:00.000Z');
      });
      it('Returns a selector with a defined $gte and an undefined $lte when given a valid startDateIso and an undefined endDateIso', () => {
        const pullPositionsPartialSelector = dbApiPositions[
          'getPullFilterOptions'
        ]('positions', '2023-06-13T00:00:00.000Z', undefined);

        expect(
          pullPositionsPartialSelector.selector!['$or'][0]['item_id'].$regex
        ).toEqual('LANDMARK.*');
        expect(
          pullPositionsPartialSelector.selector!['$or'][1]['timestamp'].$gte
        ).toEqual('2023-06-13T00:00:00.000Z');

        expect(
          pullPositionsPartialSelector.selector!['$or'][1]['timestamp'].$lte
        ).toBeTypeOf('undefined');
      });
    });
    describe('Push', () => {
      it(`Returns a selector with source: 'onefleet'`, () => {
        const pushPositionsSelector =
          dbApiPositions['getPushFilterOptions']('positions');

        expect(pushPositionsSelector.selector!['source'].$eq).toEqual(
          'onefleet'
        );
      });
    });
  });
});
