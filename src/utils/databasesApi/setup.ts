import PouchDB from 'pouchdb';
import type { UrlArgs } from '@/utils/url';
import { generateUrl } from '@/utils/url';

/**
 * Setup a local PouchDB database for an existing CouchDB.
 *
 * @param args - The setup arguments.
 * @returns The locale PouchDB database.
 *
 * @throws {@link Error}
 * Thrown if the setup fails.
 */
export function setupLocalPouchDbDatabase(
  databaseName: string,
  databasePrefix: string
): PouchDB.Database {
  let databaseLocal: PouchDB.Database;
  const pouchDbName = databasePrefix + databaseName;
  try {
    databaseLocal = new PouchDB(pouchDbName);
  } catch (error) {
    console.error(error);
    throw new Error(`Could not setup local database "${databaseName}"`);
  }
  return databaseLocal;
}

/**
 * Setup a PouchDB client for an existing CouchDB.
 *
 * @param args - The setup arguments.
 * @returns The PouchDB client.
 *
 * @throws {@link Error}
 * Thrown if the setup fails.
 */
export function setupRemotePouchDbDatabase({
  protocol,
  host,
  port,
  databasePrefix,
  databaseName,
}: UrlArgs & {
  databasePrefix: string;
  databaseName: string;
}): PouchDB.Database {
  const couchDbUrl = generateUrl({ protocol, host, port });
  const databaseUrl = `${couchDbUrl}/${databasePrefix}${databaseName}/`;
  // set options of remote database
  const opts = {
    skip_setup: true,
    fetch(url: string, opts: RequestInit): Promise<Response> {
      // In PouchDB 7.0 they dropped this and it breaks cookie authentication, so we set this explicitly
      opts.credentials = 'include';
      return PouchDB.fetch(url, opts);
    },
  };

  let databaseRemote: PouchDB.Database;
  try {
    databaseRemote = new PouchDB(databaseUrl, opts as any);
  } catch (error) {
    console.error(error);
    throw new Error(`Could not setup remote database "${databaseName}"`);
  }
  return databaseRemote;
}
