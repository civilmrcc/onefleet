import PouchDB from 'pouchdb';
import { setupLocalPouchDbDatabase, setupRemotePouchDbDatabase } from './setup';
import { whatDaysShouldBeReplicated } from '../map/positionUtils';

import { useAuthStore } from '../../stores/AuthStore';
import { useShowStore } from '../../stores/ShowStore';
import { useDataStore } from '@/stores/DataStore';

/**
 * Database event listener.
 * @param info - the sync result in the event of 'change'. null by default, when the event is 'complete'.
 */
export type DatabaseEventListener = (
  info: PouchDB.Replication.SyncResult<{}> | null
) => void;

/**
 * Database event key.
 */
export enum DatabaseEventKey {
  'change' = 'change',
  'complete' = 'complete',
}

/**
 * Database event listeners.
 */
export type DatabaseEventListeners = {
  [key in DatabaseEventKey]: DatabaseEventListener[];
};

/**
 * API for a single application database.
 */
export class DatabaseApi {
  /**
   * Create a new Database API instance.
   *
   * @param args - Database API creation arguments.
   * @returns The new Database API instance.
   *
   * @throws {@link Error}
   * Thrown if there is any error with setting up the local or remote databases.
   */
  public static create({
    protocol,
    host,
    port,
    databasePrefix,
    databaseName,
  }: {
    protocol: string;
    host: string;
    port: number;
    databasePrefix: string;
    databaseName: string;
  }): DatabaseApi {
    try {
      const local = setupLocalPouchDbDatabase(databaseName, databasePrefix);
      const remote = setupRemotePouchDbDatabase({
        protocol,
        host,
        port,
        databasePrefix,
        databaseName,
      });
      return new DatabaseApi({
        local,
        remote,
        name: databaseName,
      });
    } catch (error) {
      console.error(error);
      throw new Error(
        `Unknown error when setting up local or remote database "${databaseName}"`
      );
    }
  }

  /**
   * The database name.
   */
  private name: string;

  /**
   * The local PouchDB database.
   */
  private local: PouchDB.Database;

  /**
   * The PouchDB database for the remote CouchDB.
   */
  private remote: PouchDB.Database;

  /**
   * The database's event listeners.
   */
  private listeners: DatabaseEventListeners = {
    change: [],
    complete: [],
  };

  /**
   * The PouchDB sync object.
   *
   * @see {@link https://pouchdb.com/api.html#sync}
   */
  public sync: PouchDB.Replication.Sync<{}> | undefined;

  /**
   * Create a new Database API instance.
   *
   * @param args - The constructor args.
   */
  constructor({
    name,
    local,
    remote,
  }: {
    name: string;
    local: PouchDB.Database;
    remote: PouchDB.Database;
  }) {
    this.name = name;
    this.local = local;
    this.remote = remote;
  }

  /**
   * Add event listener(s) for a specific event.
   *
   * @param eventKey - The event key.
   * @param listeners - The event listeners to add.
   */
  public addEventListener(
    eventKey: keyof typeof DatabaseEventKey,
    ...listeners: DatabaseEventListener[]
  ): void {
    this.listeners[eventKey].push(...listeners);
  }

  /**
   * Get pull filter options for database replication and two way sync.
   *
   * @param dbName - database name
   * @param startDateIso - Oldest date to get positions from, cannot be undefined
   * @param endDateIso - Closest date to get positions from, can be undefined
   * @returns The pull filter options.
   */
  private getPullFilterOptions(
    dbName: string,
    startDateIso: string,
    endDateIso: string | undefined
  ): PouchDB.Find.Selector {
    switch (dbName) {
      case 'positions':
        return {
          selector: {
            $or: [
              {
                item_id: { $regex: 'LANDMARK.*' },
              },
              {
                timestamp: {
                  $gte: startDateIso,
                  ...(endDateIso ? { $lte: endDateIso } : undefined),
                },
              },
            ],
          },
        };
      default:
        return {};
    }
  }

  /**
   * Get push filter options for database replication and two way sync.
   *
   * @param dbName - database name
   * @returns The push filter options.
   */
  private getPushFilterOptions(dbName: string): {
    selector?: PouchDB.Find.Selector;
  } {
    switch (dbName) {
      case 'positions':
        return {
          selector: { source: { $eq: 'onefleet' } },
        };
      default:
        return {};
    }
  }

  /**
   * Start to synchronize the local and remote databases.
   * It starts with firstReplication(), on complete the secondReplication()
   * is triggered, which itself triggers the startTwoWaySynchronization() on complete.
   *
   * This staggered loading is necessary to make the app usable while
   * loading the remaining positions in the background.
   *
   * Note: local changes will only be pushed to the remote CouchDB once
   * the startTwoWaySynchronization() is triggered.
   */
  public async startSynchronization(): Promise<void> {
    if (this.name === 'positions') {
      const positionIds = await this.getPositionIdsFromAllTimeStampsView();
      this.oneTimeReplication(positionIds).on('complete', () => {
        useDataStore().setInitialReplicationOfPositionsCompleted(this.name);
        this.startTwoWaySynchronization();
      });
    } else {
      this.oneTimeReplication().on('complete', () => {
        useDataStore().setInitialReplicationOfPositionsCompleted(this.name);
        this.startTwoWaySynchronization();
      });
    }
  }

  /**
   *  Queries the ID's to replicate from the allTimeStamps view.
   *
   *  @returns The position ids in an array
   */
  private async getPositionIdsFromAllTimeStampsView(): Promise<
    string[] | undefined
  > {
    const [startDateIso, endDateIso] = whatDaysShouldBeReplicated();
    let positionIds: string[] | undefined = [];

    try {
      const result = await this.remote.query('allTimeStamps/by_timestamp', {
        startkey: endDateIso ? endDateIso : undefined,
        endkey: startDateIso,
        include_docs: false,
        descending: true,
        stale: 'update_after',
      });

      console.log(
        `CouchDB: Query on allTimeStamps view finished. Found ${result.rows.length} documents.`
      );
      positionIds = result.rows.map((row) => row.id) as string[];
    } catch (err) {
      console.error('CouchDB: Query on allTimeStamps view error:', err);
      useAuthStore().logout();
      useShowStore().setLogin({
        show: true,
        errorMessage:
          'Sync was interrupted during the first loading of data. Please stay online until all data is completely loaded from the remote database.',
      });
    }

    return positionIds;
  }

  /**
   * Replicate database once from remote to local.
   *
   * @param docIds - The document ids to replicate if an index exists for that DB to pre-query the IDs to replicate
   *
   * @returns The replication object.
   */
  private oneTimeReplication(
    docIds?: string[]
  ): PouchDB.Replication.Replication<{}> {
    console.log(`Starting replication of ${this.name} DB`);
    const [startDateIso, endDateIso] = whatDaysShouldBeReplicated();

    let syncOptions: PouchDB.Replication.ReplicateOptions = {
      batch_size: 10000,
      style: 'main_only',
      /** Despite not being properly typed, this option is valid, you can see it in their doc and in the requests in the browser.
       * This makes replication only ask for the winning rev of a document instead of the whole history.
       * @see {@link https://pouchdb.com/api.html#replication}
       */
    };
    if (docIds) {
      syncOptions = { ...syncOptions, doc_ids: docIds };
    } else {
      syncOptions = {
        ...syncOptions,
        selector: this.getPullFilterOptions(
          this.name,
          startDateIso,
          endDateIso
        ),
      };
    }
    const rep = PouchDB.replicate(this.remote, this.local, {
      ...syncOptions,
    })
      .on('change', (info) => {
        console.log(
          `${info.docs_written} ${this.name} replicated from remote DB`
        );
      })
      .on('complete', () => {
        console.log(`Finished replication from remote DB for ${this.name} DB`);
        // We need to remove the event listeners here, as those will still be
        // triggered even if we start the two-way synchronization at this point
        // Note: this needs to happen after this.triggerEventListeners('complete')
        // otherwise the complete event is not triggered
        rep.removeAllListeners();
        // Complete event Listener which triggers a first load
        // of the items and positions into the UI
        this.triggerEventListeners('complete');
      })
      .on('error', (err) => {
        console.error(`CouchDB: Sync error: ${JSON.stringify(err)}`);
        useAuthStore().logout();
        useShowStore().setLogin({
          show: true,
          errorMessage:
            'Sync was interrupted during first loading of data. Please stay online until all data is completely loaded from the remote database.',
        });
      });
    return rep;
  }

  /**
   * Stop synchronizing the local and remote databases.
   *
   * @throws {@link Error}
   * Thrown if there's an error with stopping the databases synchronization.
   */
  public stopSynchronization(): void {
    try {
      this.sync?.cancel();
    } catch (error) {
      console.log(`Stop synchronization for ${this.name} failed: ${error}`);
    }
  }

  /**
   * Start to synchronize the local and remote databases in both ways.
   */
  private async startTwoWaySynchronization(): Promise<void> {
    const [startDateIso, endDateIso] = whatDaysShouldBeReplicated();
    const syncOptions = {
      pull: this.getPullFilterOptions(this.name, startDateIso, endDateIso),
      push: this.getPushFilterOptions(this.name),
      live: true,
      retry: true,
      //back_off_function controls reconnection interval when offline
      back_off_function: function (delay: number) {
        if (delay === 0) {
          return 1000;
        }
        if (delay >= 27000) {
          return 30000;
        } else {
          return delay * 3;
        }
      },
      batch_size: 10000,
    };
    this.sync = this.local
      .sync(this.remote, syncOptions)
      .on('change', (info) => {
        this.triggerEventListeners('change', info);
      })
      .on('complete', () => {
        //for two way sync there will never be a complete event
      })
      .on('denied', (err) => {
        console.error(`CouchDB: Access denied: ${JSON.stringify(err)}`);
        useAuthStore().logout();
        useShowStore().setLogin({
          show: true,
          errorMessage:
            'Error authenticating to the remote database, please login again.',
        });
      })
      .on('error', (err) => {
        console.error(`CouchDB: Sync error: ${JSON.stringify(err)}`);
        useAuthStore().logout();
        useShowStore().setLogin({
          show: true,
          errorMessage: 'There was an error while syncing. Please login again.',
        });
      });
  }

  /**
   * Trigger the event listeners for a specific event key.
   *
   * @param eventKey - The event key for which to trigger the events.
   * @param info - the sync result in the event of 'change'. null by default, when the event is 'complete'.
   */
  private triggerEventListeners(
    eventKey: keyof typeof DatabaseEventKey,
    info: PouchDB.Replication.SyncResult<{}> | null = null
  ): void {
    this.listeners[eventKey].forEach((fn) => fn(info));
  }
}
