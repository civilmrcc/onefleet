import type { WorkBook } from 'xlsx';
import { utils, read } from 'xlsx';
import calcTemplates from '../constants/calc_templates';
import { caseTemplate } from '../constants/templates/case';
import templates from '@/constants/templates';
import type {
  CalcTemplate,
  CalcTemplateField,
} from '@/types/calc-template-schema';
import type { NewDbItem } from '@/types/db-item';
import { isValidIsoDatetime, overrideTimezoneWithUtc } from './datetimeUtils';
import type { UploadFile } from 'element-plus';
import { useDataStore } from '@/stores/DataStore';
import type { NewPosition } from '@/types/db-position';
import { positionIsValid } from './map/positionUtils';
import { ulid } from 'ulid';
import type { ParsingResult } from '@/types/calc-parsing-response';

type DataStoreType = ReturnType<typeof useDataStore>;

export class CalcImportTool {
  workbook: WorkBook | undefined;
  template: CalcTemplate | undefined;
  initialized: boolean;
  initializationMessage: string;
  parsedCases: NewDbItem[];
  incompleteRows: string[];
  parsedPositions: Map<number, (NewPosition | undefined)[] | null>;
  dataJson: any[];
  dataStore: DataStoreType;

  constructor() {
    this.workbook = undefined;
    this.template = undefined;
    this.initialized = false;
    this.initializationMessage = '';
    this.parsedCases = [];
    this.incompleteRows = [];
    this.parsedPositions = new Map();
    this.dataJson = [];
    this.dataStore = useDataStore();
  }

  /**
   * Parses the workbook object to find a compatible template.
   * @param workbook
   */
  public async initialize(workbook: WorkBook): Promise<void> {
    this.workbook! = workbook;
    const matchedTemplateOrErrorMessage = this.parseActiveTemplate();
    if (typeof matchedTemplateOrErrorMessage === 'string') {
      this.initializationMessage = matchedTemplateOrErrorMessage;
    } else {
      this.template = matchedTemplateOrErrorMessage;
      this.initialized = true;
    }
  }

  /**
   * Resets the import tool either when the user resets the tool manually
   * after changing their file or after they save the parsed data
   */
  public reset(): void {
    this.workbook = undefined;
    this.template = undefined;
    this.initialized = false;
    this.initializationMessage = '';
    this.incompleteRows = [];
    this.parsedCases = [];
    this.dataJson = [];
    this.parsedPositions.clear();
  }

  /**
   * Extracts the sheet names from the workbook to compare them to the possible template sheet names
   * before trying to find template info if a sheet is susceptible to contain it.
   * @returns Returns a CalcTemplate or an error message depending on if a match was found or not.
   */
  private parseActiveTemplate(): string | CalcTemplate {
    let matchedTemplateOrErrorMessage:
      | CalcTemplate
      | string = `No sheet found with information about the template to use. Please create a sheet named "Template" with template information or rename the sheet containg this info to "Template" if there is one.`;
    const templateSheetName: string | null = this.findTemplateSheetName(
      calcTemplates.getTemplateInfoSheetNames()
    );

    if (templateSheetName) {
      matchedTemplateOrErrorMessage = this.deduceTemplate(templateSheetName);
    }
    return matchedTemplateOrErrorMessage;
  }

  /**
   * Compares the possible names for sheets containing Template info and the name of all the sheets contained in the workbook
   * @param possibleTemplateSheetNames The content of all the 'templateInfoSheetname' field in all templates
   * @param workbookSheetNames The list of all the workbook sheets
   * @returns The first template sheet match it finds and null if there is no match.
   */
  private findTemplateSheetName(
    possibleTemplateSheetNames: string[]
  ): string | null {
    const workbookSet = new Set(this.workbook!.SheetNames);
    for (const item of possibleTemplateSheetNames) {
      if (workbookSet.has(item)) {
        return item;
      }
    }
    return null;
  }

  /**
   *
   * @param sheetNameMatch The name of the sheet containing the template info
   * @returns The CalcTemplate to use for parsing or an error message.
   */
  private deduceTemplate(sheetNameMatch: string): CalcTemplate | string {
    const sheet = this.workbook!.Sheets[sheetNameMatch];
    const regex = /^(.*)@(.*)$/;
    const firstCell = 'A1';
    const templateString = sheet[firstCell].v;
    const match = templateString.match(regex);

    if (match) {
      return calcTemplates.get(match[1], match[2]);
    }
    return `No Template information found, please check that the cell ${firstCell} in the sheet called ${sheetNameMatch} contains a string formatted like this "name@number"`;
  }

  /**
   * Creates a sample item with minimal information to be filled by the tool.
   * @returns A barebones item
   */
  private createSampleItem(): NewDbItem {
    return {
      template: this.template!.typeOfItemToParse,
      properties: { status: this.template?.defaultStatus },
      map_settings: {
        marker_color:
          '#' + ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, '0'),
        marker_icon: caseTemplate.default_marker_icon,
        get_historical_data_since: -1,
        tracking_type: 'MANUAL',
        tracking_id: '',
        tracking_token: '',
      },
      import_template: `${this.template!.name}@${this.template!.version}`,
    };
  }

  /**
   * Returns an A1-Style range for the XLXS sheet_to_json utility function
   * @linkplain https://docs.sheetjs.com/docs/csf/general/#cell-ranges-1
   * @param endRow The last row containing items to be parsed by the tool
   */
  private construstRange(endRow: number): string {
    return `A${this.template?.casesRowStart}:${this.template
      ?.letterColumnEnd!}${endRow}`;
  }

  /**
   * Returns an array of objects, each representing a row of the file.
   * @param lastCaseRow The last row to parse items from.
   * @returns
   */
  private sheetToJson(lastCaseRow: number): any[] {
    const range = this.construstRange(lastCaseRow);
    return utils.sheet_to_json(
      this.workbook!.Sheets[this.template!.dataSheetName],
      {
        blankrows: true,
        header: 'A',
        range: range,
        raw: true,
        defval: null,
        UTC: true,
      }
    );
  }

  /**
   * Parses mandatory fields and keeps track of which items should be deleted before saving them to the DB
   * @returns An array of numbers representing the corresponding index in this.parsedCases that have to be deleted before writing to the DB.
   */
  private parseMandatoryFields(): void {
    const mandatoryFields = this.template?.CalcFields.filter(
      (field) => field.requiredForCreation
    );
    for (const [index, rowObject] of this.dataJson!.entries()) {
      for (const mandatoryField of mandatoryFields || []) {
        const fieldType = templates.getTypeByName(
          caseTemplate,
          mandatoryField.caseTemplateFieldEquivalentName
        );
        switch (fieldType) {
          case 'datetime' || 'datetimemulti':
            const dateTimeValue = this.parseDateTimeFields(
              rowObject,
              mandatoryField
            );
            if (!dateTimeValue.success) {
              this.incompleteRows[index] = 'incomplete';
            }
            this.parsedCases![index].properties[
              mandatoryField.caseTemplateFieldEquivalentName
            ] = dateTimeValue.result;
            break;
          case 'text' || 'textarea':
            const fieldValue = this.parseTextFields(rowObject, mandatoryField);
            this.parsedCases![index].properties[
              mandatoryField.caseTemplateFieldEquivalentName
            ] = fieldValue.result;
            if (fieldValue.result === null || undefined) {
              this.incompleteRows[index] = 'incomplete';
            }
            break;
        }
      }
    }
  }

  /**
   * Tries to parse a datetime or datetimemulti field in rowObject based on the specification in the given field template.
   * Uses tryGetDateTime() to parse individual sets of colums if there are fallbacks columns set in the template.
   * @param rowObject Object representation of the current row being parsed
   * @param calcFieldTemplate The template used to parse data for a specific Onefleet field
   * @returns Returns either an ISO timestamp UTC string,
   * null if all the cells parsed were empty,
   * or undefined if they had no parseable data
   */
  private parseDateTimeFields(
    rowObject: any,
    calcFieldTemplate: CalcTemplateField
  ): ParsingResult {
    let output = this.tryGetDateTime(rowObject, calcFieldTemplate.columnLetter);
    if (!output.success) {
      for (const fallback of calcFieldTemplate.fallbackCells || []) {
        output = this.tryGetDateTime(rowObject, fallback.column);
        if (output.success) break;
      }
    }
    return output;
  }

  private tryGetDateTime(rowObject: any, columns: string[]): ParsingResult {
    const timeStampRegex: RegExp = /@(\d{2}:\d{2})/;
    const cellDateValue = rowObject[columns[0]];
    let cellTimeValue: string = '00:00:00.000Z';
    let cellTimeMatch: RegExpMatchArray | null;

    if (cellDateValue !== null) {
      if (timeStampRegex.test(rowObject[columns[1]])) {
        cellTimeMatch = rowObject[columns[1]].match(timeStampRegex);
        cellTimeValue = `${cellTimeMatch![1]}:00.000Z`;
      }

      if (isValidIsoDatetime(new Date(cellDateValue))) {
        const dateString = overrideTimezoneWithUtc(new Date(cellDateValue))
          .toISOString()
          .split('T')[0];
        return { success: true, result: `${dateString}T${cellTimeValue}` };
      }
      return { success: false, result: undefined };
    } else {
      return { success: false, result: null };
    }
  }

  /**
   * Tries to parse a number field in rowObject based on the specification in the given field template.
   * Returns an undefined value is no valid number was found, whether it's because the cells were empty or contained unparseable data.
   * @param rowObject Object representation of the current row being parsed
   * @param calcFieldTemplate The template used to parse data for a specific Onefleet field
   */
  private parseNumberFields(
    rowObject: any,
    calcFieldTemplate: CalcTemplateField
  ): ParsingResult {
    const output: ParsingResult = { success: false, result: undefined };
    const tryGetNumber = (columns: string[]): boolean => {
      let cellValue: number = 0;
      for (const column of columns) {
        if (!isNaN(rowObject[column])) {
          cellValue += rowObject[column];
        }
      }
      if (cellValue != 0) {
        output.success = true;
        output.result = cellValue;
        return true;
      }
      return false;
    };

    if (!tryGetNumber(calcFieldTemplate.columnLetter)) {
      for (const fallback of calcFieldTemplate.fallbackCells || []) {
        if (tryGetNumber(fallback.column)) break;
      }
    }
    return output;
  }

  /**
   * Tries to parse a select field in rowObject based on the specification in the given field template.
   * Defaults to the default value specified in the calc template is the cells were empty because select field have default values.
   * Defaults to the item template default value if the calc template has no custom default.
   * @param rowObject Object representation of the current row being parsed
   * @param calcFieldTemplate The template used to parse data for a specific Onefleet field
   */
  private parseSelectFields(
    rowObject: any,
    calcFieldTemplate: CalcTemplateField
  ): ParsingResult {
    const options = templates.getOptionsByName(
      caseTemplate,
      calcFieldTemplate.caseTemplateFieldEquivalentName
    );

    const output: ParsingResult = { success: false, result: undefined };
    const tryGetSelect = (columns: string[]): boolean => {
      for (const column of columns) {
        let tempResult = rowObject[column];
        if (typeof tempResult === 'string') {
          for (const [key, entry] of Object.entries(options!)) {
            tempResult = tempResult.toLocaleLowerCase();
            if (tempResult === key || tempResult === entry) {
              output.result = tempResult;
              output.success = true;
              return true;
            } else {
              output.result = undefined;
            }
          }
        } else if (tempResult == null) {
          output.result = null;
        }
      }
      return false;
    };

    if (!tryGetSelect(calcFieldTemplate.columnLetter)) {
      for (const fallback of calcFieldTemplate.fallbackCells || []) {
        if (tryGetSelect(fallback.column)) break;
      }
    }
    /* We choose to set the success to true when the result is null (ie empty cell)
     * because select fields have a default value for when the information is missing.
     * We do not do this for undefined because in that situation, it's assumed that the info exists but is badly formatted in the cell.
     */
    if (output.result === null) {
      output.success = true;
      if (calcFieldTemplate.defaultValue) {
        output.result = calcFieldTemplate.defaultValue;
      } else {
        output.result = templates.getDefaultValueByName(
          caseTemplate,
          calcFieldTemplate.caseTemplateFieldEquivalentName
        );
      }
    }

    return output;
  }

  /**
   * Tries to parse a text field in rowObject based on the specification in the given field template.
   * @param rowObject Object representation of the current row being parsed
   * @param calcFieldTemplate The template used to parse data for a specific Onefleet field
   */
  private parseTextFields(
    rowObject: any,
    calcFieldTemplate: CalcTemplateField
  ): ParsingResult {
    const output: ParsingResult = { success: false, result: null };
    const tryGetText = (columns: string[]): boolean => {
      let cellValue: string = '';
      for (const column of columns) {
        if (
          typeof rowObject[column] === 'string' ||
          typeof rowObject[column] === 'number'
        ) {
          cellValue += rowObject[column].toString();
          break;
        }
      }
      if (cellValue.length > 0) {
        output.success = true;
        output.result = cellValue;
      }
      return output.success;
    };
    if (!tryGetText(calcFieldTemplate.columnLetter)) {
      for (const fallback of calcFieldTemplate.fallbackCells || []) {
        if (tryGetText(fallback.column)) break;
      }
    }
    return output;
  }

  /**
   Tries to parse URLs from the rowObject based on the specification in the given field template.
   * @param rowObject Object representation of the current row being parsed
   * @param calcFieldTemplate The template used to parse data for a specific Onefleet field
   */
  private parseUrlsFields(
    rowObject: any,
    calcFieldTemplate: CalcTemplateField
  ): ParsingResult {
    const output: ParsingResult = { success: false, result: null };
    const tryGetUrl = (columns: string[]): boolean => {
      for (const column of columns) {
        if (
          typeof rowObject[column] === 'string' &&
          rowObject.hasOwnProperty(column)
        ) {
          const matches = rowObject[column].split('|||').map((part: string) => {
            const matchResult = part.match(calcFieldTemplate.regex!);
            return matchResult ? matchResult.flat().filter(Boolean) : [];
          });

          if (matches.length > 0 && matches[0].length > 0) {
            output.result = matches.flat().join('\n');
            output.success = true;
          } else {
            output.result = undefined;
          }
        } else if (typeof rowObject[column] !== null) {
          output.result = undefined;
        }
      }
      return output.success;
    };

    if (!tryGetUrl(calcFieldTemplate.columnLetter)) {
      for (const fallback of calcFieldTemplate.fallbackCells || []) {
        if (tryGetUrl(fallback.column)) break;
      }
    }
    return output;
  }
  /**
   * Tries to parse all positions from the passed rowObject.
   * Returns an array made up of NewPosition or undefined if some data was found in a cell but could not be parsed.
   * Returns null if not a single position was found for this row.
   * @param rowObject Object representation of the current row being parsed
   */
  private parsePositions(rowObject: any): (NewPosition | undefined)[] | null {
    const positionsArray: (NewPosition | undefined)[] = [];
    this.template?.CalcPositions.forEach((calcPosition) => {
      const positionString = rowObject[calcPosition.positionCellColumn];
      if (positionString !== null) {
        if (calcPosition.format == 'REGEX') {
          const match = positionString.match(calcPosition.regex);
          if (match) {
            const position: NewPosition = {
              lat: match[1],
              lon: match[2],
              timestamp: `${match[3]}.000Z`,
              source: 'onefleet',
            };
            if (positionIsValid(position)) {
              positionsArray.push(position);
            } else {
              positionsArray.push(undefined);
            }
          } else {
            positionsArray.push(undefined);
          }
        }
      }
    });
    if (positionsArray.length > 0) {
      return positionsArray;
    }
    return null;
  }

  /**
   * Creates an array of blank cases to be filled and marks each row as complete.
   */
  private createBlankCasesArray(): void {
    for (let i = 0; i < this.dataJson!.length; i++) {
      this.parsedCases.push(this.createSampleItem());
      this.incompleteRows.push('complete');
    }
  }

  /**
   * Deletes item and position data for all items where data could not be parsed in a mandatory field.
   */
  private deleteIncompleteRows(): void {
    for (let i = this.incompleteRows.length - 1; i >= 0; i--) {
      if (this.incompleteRows[i] === 'incomplete') {
        this.parsedCases!.splice(i, 1);
        this.parsedPositions.delete(i);
      }
    }
  }

  /**
   * Before saving the positions to the DB,
   * we need to clean the ones that could not be fully parsed yet are present
   * in the parsedPositions Map to provide feedback on  the parsing done to the user
   */
  private cleanIncompletePositions(): void {
    this.parsedPositions.forEach((positions, key) => {
      if (positions) {
        const undefinedLessArray = positions.filter(
          (item) => item !== undefined
        );
        this.parsedPositions.set(key, undefinedLessArray);
      }
    });
  }

  private parseCasesAndPositions(): void {
    const fields = this.template?.CalcFields.filter(
      (field) => !field.requiredForCreation
    );
    this.dataJson!.forEach((rowObject, index) => {
      fields?.forEach((calcField) => {
        const fieldType = templates.getTypeByName(
          caseTemplate,
          calcField.caseTemplateFieldEquivalentName
        );
        if (['datetime', 'datetimemulti'].includes(fieldType!)) {
          const output = this.parseDateTimeFields(rowObject, calcField);
          this.checkIncompleteStatus(
            index,
            output,
            calcField.caseTemplateFieldEquivalentName
          );
        } else if (['number'].includes(fieldType!)) {
          const output = this.parseNumberFields(rowObject, calcField);
          this.checkIncompleteStatus(
            index,
            output,
            calcField.caseTemplateFieldEquivalentName
          );
        } else if (['select'].includes(fieldType!)) {
          const output = this.parseSelectFields(rowObject, calcField);
          this.checkIncompleteStatus(
            index,
            output,
            calcField.caseTemplateFieldEquivalentName
          );
        } else if (['text', 'textarea'].includes(fieldType!)) {
          const output = this.parseTextFields(rowObject, calcField);
          this.checkIncompleteStatus(
            index,
            output,
            calcField.caseTemplateFieldEquivalentName
          );
        } else if (['inputtable'].includes(fieldType!)) {
          const output = this.parseUrlsFields(rowObject, calcField);
          this.checkIncompleteStatus(
            index,
            output,
            calcField.caseTemplateFieldEquivalentName
          );
        }
      });
      this.parsedPositions.set(index, this.parsePositions(rowObject));
    });
  }

  /**
   * Checks if the case isn't missing a mandatory field before marking it as partial if output.result is undefined.
   * Follows by setting the result data.
   */
  private checkIncompleteStatus(
    index: number,
    output: ParsingResult,
    fieldName: string
  ): void {
    if (this.incompleteRows[index] !== 'incomplete' && !output.result) {
      this.incompleteRows[index] = 'partial';
    }
    this.parsedCases[index].properties[fieldName] = output.result;
  }

  /**
   * Creates cases and positions after having been checked by the user
   */
  public async createCasesAndPositions(): Promise<void> {
    if (!this.parsedCases) return;
    this.deleteIncompleteRows();
    this.cleanIncompletePositions();
    for (const [index, caseObject] of this.parsedCases.entries()) {
      const pouchIdentifier = this.template?.typeOfItemToParse;
      const newItemId = `${pouchIdentifier}_${ulid()}`.toUpperCase();

      const response: PouchDB.Core.Response | PouchDB.Core.Error =
        await this.dataStore.$db.createItem(caseObject, newItemId);

      if ((response as PouchDB.Core.Response).ok) {
        await this.dataStore.reloadItem(newItemId); // Await for reloadItem

        const positions = this.parsedPositions.get(index);
        if (positions) {
          await this.dataStore.$db.createPositions(newItemId, positions); // Await for positions creation
        }
      }
    }
  }

  /**
   * Overarching parsing function
   * @param lastCaseRow Last row containing data to be parsed
   */
  public async parseFile(lastCaseRow: number): Promise<void> {
    return new Promise((resolve, reject) => {
      try {
        this.dataJson = this.sheetToJson(lastCaseRow);
        this.createBlankCasesArray();
        this.parseMandatoryFields();
        this.parseCasesAndPositions();
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  }
}

/**
 *
 * @param file The raw uploaded file
 * @returns Returns a workbook XLXS object
 */
export async function extractWorkbookFromEventTarget(
  file: UploadFile
): Promise<WorkBook> {
  const rawFile = file.raw!;
  const data = await rawFile.arrayBuffer();
  const workbook = read(data, {
    raw: true,
    cellDates: true,
    UTC: true,
    type: 'string',
  });

  return workbook;
}
