/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_DB_REMOTE_HOST?: string;
  readonly VITE_DB_REMOTE_PROTOCOL?: string;

  readonly VITE_DB_PREFIX: string;
  readonly VITE_DB_REMOTE_PORT: number;

  readonly VITE_DB_COOKIE_VALIDATION_TIMEOUT: number;

  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
