import { getDotenvVariable } from '@/utils/dotenv';
import { config } from '@vue/test-utils';
import ResizeObserver from 'resize-observer-polyfill';
import { vi } from 'vitest';

vi.stubGlobal('ResizeObserver', ResizeObserver);

vi.stubEnv('VITE_DB_REMOTE_HOST', getDotenvVariable('VITE_DB_REMOTE_HOST'));
vi.stubEnv(
  'VITE_DB_REMOTE_PROTOCOL',
  getDotenvVariable('VITE_DB_REMOTE_PROTOCOL')
);
vi.stubEnv('VITE_DB_REMOTE_PORT', getDotenvVariable('VITE_DB_REMOTE_PORT'));
vi.stubEnv('VITE_DB_PREFIX', getDotenvVariable('VITE_DB_PREFIX'));

config.global.stubs = {};
