const config_path: string = './config';
const config = await import(`${config_path}`);

import MailListener from 'mail-listener2';
import ulid from 'ulid';
import DBInstance from './DBInstances';

/*mailService will handle creating an imap connection to a remote mailserver, 
fetching the new emails and then parsing the results into a database friendly format, and saving them to backend*/
export default class MailService {
  DB: DBInstance; // dbInstance handles connection to the DB
  private static instance: MailService; //so we only ever have one mailservice

  constructor() {
    this.DB = DBInstance.getInstance();
  }

  //gets called from elsewhere to get any existing instances of mailservice, and if one doesnt exist, set it up.
  public static getInstance(): MailService {
    if (!MailService.instance) {
      MailService.instance = new MailService();
    }
    return MailService.instance;
  }

  public initMail() {
    try {
      console.log('starting mail listener...');
      this.DB.initDBs();
      console.log('mail init');
      this.initMailListener({
        imap_username: config.mailServiceConfig.imap_username,
        imap_password: config.mailServiceConfig.imap_password,
        imap_host: config.mailServiceConfig.imap_host,
        mail_callback: (mail, seqno, attributes) => {
          if (mail) this.positionCallback(mail, seqno, attributes);
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
  public async positionCallback(mail, seqno, attributes) {
    try {
      const sender_mail = this.getEmailsFromString(mail.headers.from);

      console.log('getting vehicles...');

      const self = this;
      await this.DB.initDBs();
      const items = await this.DB.getItems('VEHICLE');
      for (const i in items) {
        const tracking_type =
          items[i].doc.map_settings?.tracking_type ??
          items[i].doc.properties?.tracking_type;
        if (tracking_type) {
          if (typeof items[i].doc != 'undefined') {
            console.log('item #' + i);

            const item = items[i].doc;
            console.log(item._id);
            if (tracking_type)
              switch (tracking_type) {
                case 'IRIDIUMGO':
                  //message looks like "I am here Lat+35.777135 Lon+14.291735 Alt+2554ft GPS Sats seen 09 http://map.iridium.com/m?lat=35.777135&lon=14.291735 http://map.iridium.com/m?lat=35.777135&lon=14.291735 Sent via Iridium GO!"

                  const iridium_sender_mail =
                    item.map_settings?.tracking_id ??
                    item.properties?.iridium_sender_mail;

                  if (sender_mail.indexOf(iridium_sender_mail) > -1) {
                    console.log('Mail from gateway sender received:');

                    if (mail.text.indexOf('I am here') > -1) {
                      const lat = parseFloat(
                        mail.text
                          .substring(
                            mail.text.indexOf('Lat+') + 4,
                            mail.text.indexOf('Lon')
                          )
                          .trim()
                      );

                      const lon = parseFloat(
                        mail.text
                          .substring(
                            mail.text.indexOf('Lon+') + 4,
                            mail.text.indexOf('Alt')
                          )
                          .trim()
                      );

                      let alt = mail.text
                        .substring(
                          mail.text.indexOf('Alt+') + 4,
                          mail.text.indexOf('GPS')
                        )
                        .trim();

                      alt = parseFloat(alt.replace('ft', ''));

                      console.log('adding position to db:');
                      this.DB.insertLocation(items[i].doc._id, {
                        timestamp: new Date(mail.headers.date),
                        lat: lat,
                        lon: lon,
                        altitude: alt,
                        speed: -1,
                        course: -1,
                        source: 'iridium_mailservice',
                      });
                      console.log({
                        timestamp: new Date(mail.headers.date),
                        latitude: lat,
                        longitude: lon,
                        altitude: alt,
                        speed: -1,
                        course: -1,
                        source: 'iridium_mailservice',
                      });
                    } else {
                      // TODO: alke + jula: Add else branch for DropPoint
                      console.log('not a position update, so a new case!');

                      const lat = parseFloat(
                        mail.text
                          .substring(
                            mail.text.indexOf('Lat+') + 4,
                            mail.text.indexOf('Lon')
                          )
                          .trim()
                      );

                      const lon = parseFloat(
                        mail.text
                          .substring(
                            mail.text.indexOf('Lon+') + 4,
                            mail.text.indexOf('Alt')
                          )
                          .trim()
                      );

                      const alt = mail.text
                        .substring(
                          mail.text.indexOf('Alt') + 4,
                          mail.text.indexOf('GPS')
                        )
                        .trim();

                      const id = `DROPPOINT_${Math.floor(
                        new Date(mail.headers.date).getTime() / 1000
                      )}_${ulid.ulid()}`;
                      console.log('no found! create droppoint');
                      const item = {
                        //generate id like VEHICLE_SHIPSNAME
                        _id: id,
                        template: 'droppoint',
                        id: id,
                        properties: {
                          created_by_vehicle: items[i].doc.id,
                          name: items[i].doc.id,
                          comment: 'droppoint created by ' + items[i].doc.id,
                        },
                      };
                      this.DB.insertItem(item, function (err, result) {
                        if (err) {
                          if (err.name == 'conflict')
                            console.log(
                              'The id is already taken, please choose another one'
                            );
                          else
                            console.log(
                              err,
                              'An unknown error occured while creating the item'
                            );
                        } else if (result.ok == true) {
                          console.log(
                            `droppoint ${item._id} created. add position...`
                          );

                          self.DB.insertLocation(items[i].doc._id, {
                            timestamp: new Date(mail.headers.date),
                            lat: lat,
                            lon: lon,
                            altitude: alt,
                            speed: -1,
                            course: -1,
                            source: 'iridium_mailservice',
                          });
                        } else {
                          console.log(result);
                        }
                      });
                    }
                  } else {
                    console.log('Mail is not from gateway sender');
                  }

                  break;
              }
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
  /*
   *@strObj string String containg a mail like "Joe Smith <joe.smith@somemail.com>"
   *returns mail (e.g. joe.smith@somemail.com)
   */
  public getEmailsFromString(StrObj) {
    const separateEmailsBy = ', ';
    let email = '<none>'; // if no match, use this
    const emailsArray = StrObj.match(
      /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi
    );

    if (emailsArray) {
      email = '';
      for (let i = 0; i < emailsArray.length; i++) {
        if (i != 0) email += separateEmailsBy;
        email += emailsArray[i];
      }
    }
    return email;
  }
  public initMailListener(listener_config) {
    console.log(listener_config);
    const mailListener = new MailListener({
      username: listener_config.imap_username,
      password: listener_config.imap_password,
      host: listener_config.imap_host,

      port: 993, // imap port
      tls: true,
      connTimeout: 10000, // Default by node-imap
      authTimeout: 5000, // Default by node-imap,
      debug: console.log, // Or your custom function with only one incoming argument. Default: null
      tlsOptions: { rejectUnauthorized: false },
      mailbox: 'INBOX', // mailbox to monitor
      searchFilter: ['UNSEEN'], // the search filter being used after an IDLE notification has been retrieved
      markSeen: true, // all fetched email willbe marked as seen and not fetched next time
      fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
      mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
      attachments: true, // download attachments as they are encountered to the project directory
      attachmentOptions: { directory: 'attachments/' }, // specify a download directory for attachments
    });
    mailListener.start(); // start listening

    // stop listening
    //mailListener.stop();

    mailListener.on('server:connected', function () {
      console.log('imapConnected');
    });

    mailListener.on('server:disconnected', function () {
      console.log('imapDisconnected');
    });

    mailListener.on('error', function (err) {
      console.log('err:');
      console.log(err);
    });

    mailListener.on('mail', function (mail, seqno, attributes) {
      console.log('got mail!');
      listener_config.mail_callback(mail, seqno, attributes);
    });
  }
}
