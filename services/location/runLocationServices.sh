#!/bin/bash
#
# Server-side script to periodically run the AIS-part of the location service via cron.
# Example crontab row for running this script once every hour:
# */60 * * * *	/var/www/services/location/runLocationServices.sh   2>/var/www/cronjob_status/location_errors.log  1>/var/www/cronjob_status/location_output.log

# change into the location-service code directory so that node can find all node-modules:
cd $(dirname -- "$0")

# export variables from .env file
set -o allexport; source .env; set +o allexport

# run the AIS parts of the location service using the proper .env variables that were loaded above
node --experimental-specifier-resolution=node runLocationServices.js
