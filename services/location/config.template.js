import * as dotenv from 'dotenv';
dotenv.config();

function getEnvValueByKey(key) {
  const val = process.env[key];
  if (val == null) {
    throw Error(`Startup: ENVIRONMENT (${key}) not provided.`);
  }
  return val;
}

export const locationConfig = {
  aisUrl: process.env.AIS_URL ?? 'http://localhost:5001',
  desiredResolutionMinutes: process.env.ACCEPTABLE_RESOLUTION_MINUTES ?? 15,
  locationsOnce: true,
  marine_traffic_exportvessel_api_key:
    process.env.MARINE_TRAFFIC_EXPORTVESSEL_API_KEY,
  marine_traffic_exportvesseltrack_api_key:
    process.env.MARINE_TRAFFIC_EXPORTVESSELTRACK_API_KEY,
  fleetmon_api_key: process.env.FLEETMON_API_KEY,
};

export const dbConfig = {
  dbUrl: getEnvValueByKey('DB_URL'),
  dbUser: getEnvValueByKey('DB_USER'),
  dbPassword: getEnvValueByKey('DB_PASSWORD'),
  dbPrefix: getEnvValueByKey('DB_PREFIX'),
};

export const mailServiceConfig = {
  imap_username: process.env.IMAP_USERNAME,
  imap_password: process.env.IMAP_PASSWORD,
  imap_host: process.env.IMAP_DOMAIN,
  allowDomainList: process.env.ALLOW_LIST_DOMAIN,
};
