export interface NewPosition {
  timestamp: string | Date; // ISO-formatted string or a Date object. May only be changed before _id field is created.
  source: string; // which datasource inserted this position
  soft_deleted?: boolean; // invalid positions should never be deleted by the frontend app, only maked as "soft_deleted"
  lat: number; // latitude
  lon: number; // longitude
  altitude?: number;
  course?: number; // Travel direction.
  heading?: number; // Pointing direction. Previously used interchangably here as Course over Ground, CoG
  speed?: number; // Speed over Ground, SoG
  comments?: string; // additional notes on a single position. Use wisely.
}

export interface DbPosition extends NewPosition {
  readonly _id: string; // item_identifier + '_' + timestamp (isotime) + '_' random id (ulid)
  readonly item_id: string; // must never change. Rather copy & soft-delete a position.
  readonly timestamp: string; // Must be an ISO-formatted string. Must never change. Rather copy & soft-delete a position.
  time_created?: Date; // creation time. Should be set just before writing to DB the first time.
  time_modified?: Date; // last modification time. Should be set just before writing to DB each time.
}
