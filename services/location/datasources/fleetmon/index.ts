import axios from 'axios';

export const source_title = 'fleetmon';
export const priority = 20;
export async function getPosition(item, nconfig) {
  const doc = item.doc;
  const mmsi_string = doc.map_settings?.tracking_id ?? doc.properties?.MMSI;
  const mmsi = parseInt(mmsi_string, 10);

  if (!nconfig.fleetmon_api_key) {
    throw new Error(
      new Date().toISOString + 'No api key for fleetmon provided'
    );
  }
  const options = {
    headers: {
      'Api-Key': nconfig.fleetmon_api_key,
    },
    url: '',
  };
  let vessel_id_string =
    doc.map_settings?.tracking_token ?? doc.properties?.fleetmon_vessel_id;
  if (!vessel_id_string) {
    options.url = `https://apiv2.fleetmon.com/vesselsearch/?mmsi_number=${mmsi}&max_pos_age_days=2`;
    const response = await axios.get(options.url, options);
    if (response.status != 200) {
      const error_message = `${new Date().toISOString()}: Bad HTTP status while searching for fleetmon vessel. status: ${
        response.status
      }, statusText: ${response.statusText} mmsi: ${mmsi} name: ${
        doc.properties.name
      }`;
      throw new Error(error_message);
    }
    if (response.data.vessels.length == 0) {
      throw new Error(
        `${new Date().toISOString()}: The given MMSI (${mmsi}) returned no ships on fleetmon. Is the MMSI correct? The ship is named ${
          doc.properties.name
        }`
      );
    }
    if (response.data.vessels.length > 1) {
      for (const foundVessel of response.data.vessels) {
        console.info(
          `${new Date().toISOString()}: mmsi: ${mmsi}, vessel_id: ${
            foundVessel.vessel_id
          }, name: ${foundVessel.name}`
        );
      }
    }
    vessel_id_string = response.data.vessels[0].vessel_id;
  }
  const fleetmon_vessel_id = parseInt(vessel_id_string, 10);
  // Fleetmon API docs "Vessel Positions Only":
  // https://developer.fleetmon.com/reference/#tag/Vessel-Positions-Only
  options.url = `https://apiv2.fleetmon.com/vessel/${fleetmon_vessel_id}/position?request_limit_info=true`;
  const response = await axios.get(options.url, options);
  // throw an error if the request had any errors:
  if (response.status != 200) {
    const error_message = `${new Date().toISOString()}: Bad HTTP status while fetching fleetmon position. status: ${
      response.status
    }, statusText: ${response.statusText}, responseData: ${response.data}`;
    throw new Error(error_message);
  }

  // datasource-specific format for latest position:
  // https://developer.fleetmon.com/reference/#tag/Vessel-Positions-Only
  const latest_fleetmon_position = response.data;
  if (
    latest_fleetmon_position &&
    latest_fleetmon_position.latitude &&
    latest_fleetmon_position.longitude &&
    latest_fleetmon_position.received
  ) {
    console.log(
      `${new Date().toISOString()}: Number of requests left for positions: ${
        latest_fleetmon_position.request_limit_info.left_requests
      }, Max numbers request: ${
        latest_fleetmon_position.request_limit_info.max_requests
      }`
    );
    return {
      // mmsi: mmsi,
      lat: latest_fleetmon_position.latitude,
      lon: latest_fleetmon_position.longitude,
      timestamp: latest_fleetmon_position.received,
      source: 'fleetmon',
    };
  } else {
    const error_message = `${new Date().toISOString()}: Fleetmon returned no coordinates for the following Fleetmon Vessel Id ${fleetmon_vessel_id}`;
    console.error(error_message, latest_fleetmon_position);
    throw new Error(error_message);
  }
}
