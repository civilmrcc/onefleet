#!/bin/bash
#
# Server-side script to periodically update the all_ships layer via cron.
# Example crontab row for running this script once every hour:
# */60 * * * *	/home/alpha/services/allships/main.sh   2>/home/alpha/cronjob_status/allships_errors.log  1>/home/alpha/cronjob_status/allships_output.log
set -euo pipefail

# change into the allships code directory so that node can find all node-modules:
cd $(dirname -- "$0")

# export variables from .env file
set -o allexport; source .env; set +o allexport

# get the data from fleetmon and save it into a fleetmon-specific json file:
curl --no-progress-meter "https://apiv2.fleetmon.com//regional_ais/tracking/?apikey=$FLEETMON_APIKEY&request_limit_info=true"  --header 'Accept: application/json' > $ALLSHIPS_CURRENT/fleetmon.json

# make a copy with human-readable timestamp for history:
cp $ALLSHIPS_CURRENT/fleetmon.json $ALLSHIPS_HISTORY/fleetmon_history_$(date '+%Y-%m-%d_%H%M').json

# run the geojson converter and write to a file that is symlinked from /var/www/all_layers/
node ./main.js -i $ALLSHIPS_CURRENT/fleetmon.json -o $ALLSHIPS_CURRENT/all_ships.geojson

# copy the geojson file to the public geojosn folder, if it has more than 100 bytes length (i.e. more than zero ships)
if [ $(wc -c <"$ALLSHIPS_CURRENT/all_ships.geojson") -ge 100 ]; then
  cp $ALLSHIPS_CURRENT/all_ships.geojson $ALLSHIPS_PUBLIC/all_ships.geojson
fi
