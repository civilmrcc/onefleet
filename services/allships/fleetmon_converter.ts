import * as fs from 'fs';
import type { GeoJSONConverter } from './types';

export default class FleetmonGeoJSONConverter implements GeoJSONConverter {
  /**
   * Do the actual conversion using the .map() function of typescript arrays.
   */
  private convert_v1(input_json: Object): Object {
    if (!input_json['vessels']) {
      // input_json is probably an error message from the server, so just throw it:
      throw input_json;
    }

    const output_json = {
      type: 'FeatureCollection',
      features: input_json['vessels'].map((vessel) => ({
        type: 'Feature',
        properties: {
          name: vessel.name,
          callsign: vessel.callsign,
          cn_iso2: vessel.cn_iso2,
          mmsi_number: vessel.mmsi_number,
          imo_number: vessel.imo_number,
          type: vessel.type,
          type_class: vessel.type_class,
          type_code: vessel.type_code,
          vessel_id: vessel.vessel_id,
          'marker-color': this.stringToColour(vessel.type_class ?? 'unknown'),
          'marker-symbol': vessel.type_class,
          position_course_over_ground: vessel.position.course_over_ground,
          position_nav_status: vessel.position.nav_status,
          position_received: vessel.position.received,
          position_speed: vessel.position.speed,
          position_heading: vessel.position.true_heading,

          voyage_destination: vessel.voyage.destination,
          voyage_eta: vessel.voyage.eta,
          voyage_received: vessel.voyage.received,
          datasource: 'fleetmon',
        },
        geometry: {
          type: 'Point',
          coordinates: [vessel.position.longitude, vessel.position.latitude],
        },
      })),
    };
    return output_json;
  }

  /**
   * Creates a repeatable HTML color for the given string.
   * Taken from stack-overflow somewhere ;)
   * @param str
   */
  private stringToColour(str: string): string {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let colour = '#';
    for (let i = 0; i < 3; i++) {
      const value = (hash >> (i * 8)) & 0xff;
      colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
  }

  /**
   * Read the input file, parse as JSON, call the converter function,
   * and write back to GeoJSON file.
   */
  public convert(
    input_filename: string,
    output_filename: string,
    input_format_version: string
  ): void {
    const rawdata = fs.readFileSync(input_filename, { encoding: 'utf8' });
    const input_json = JSON.parse(rawdata);
    let output_json;

    switch (input_format_version) {
      case 'v1':
        output_json = this.convert_v1(input_json);
        break;
      case 'latest':
        output_json = this.convert_v1(input_json);
        break;
      default:
        throw new Error('Unknown version of fleetmon input file format');
    }

    const data = JSON.stringify(output_json, null, 2);
    fs.writeFileSync(output_filename, data);
  }
}
