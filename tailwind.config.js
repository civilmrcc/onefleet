const colors = require('tailwindcss/colors');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#17affa',
        primary_transparent: '#17affa95',
        secondary: '#04202e',
        secondary_transparent: '#04202ec5',
        secondary_dark: '#c45454',
        white: '#ffffff',
        light_gray: '#eeeeee',
        dark_gray: '#666666',
        red: '#ff3333',
        light_red: '#f56c6c',
        black: '#151515',
      },
      zIndex: {
        100: '100',
        1001: '1001',
        2000: '2000',
      },
    },
  },
  plugins: [],
};
