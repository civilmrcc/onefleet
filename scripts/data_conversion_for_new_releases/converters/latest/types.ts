import type { DbItemProperty } from './types/db-item';
import type { ItemTemplateFieldType } from './types/item-template-schema';

export type CheckingResult = DisallowedResult | MatchResult;

/**
 * Bad Result of a single match check of a single database item. Will be summarized later.
 *
 * If an item's template is not found, then no further tests can be performed.
 * This type is then used instead, so that the user of the conversion script can
 * at least see which of the items used a disallowed template and can maybe fix it manually.
 */
export interface DisallowedResult {
  /**
   * This is any DbItem-like item. But as we cannot be sure what we have
   * received in the dump, we don't want to give a false sense of security here.
   *
   * So that is why the type here is 'any' instead of DbItem.
   */
  item: any;

  /**
   * The template key that was not found in the existing templates.
   * The user can search for this string in the data to find out more
   * about why the disallowed template was found.
   */
  disallowedTemplate: string;
}

/** Result of a single match check of a single database item. Will be summarized later. */
export type MatchResult = {
  /**
   * This is any DbItem-like item. But as we cannot be sure what we have
   * received in the dump, we don't want to give a false sense of security here.
   *
   * So that is why the type here is 'any' instead of DbItem.
   */
  item: any;

  /** Results of match-check of the base (i.e. top-level) attributes of a single item */
  baseAttributeValidity: Record<string, boolean>;

  /** Results of match-check of the templated properties of a single item */
  propertiesValidity: PropertyMatchResult;

  /** Results of match-check of the map settings of a single item */
  mapSettingsValidity: Record<string, boolean>;
};
/** Results of match-check of the templated properties of a single item */
export interface PropertyMatchResult {
  /**
   * Each of the property names of this item are put into one of these arrays so that we can
   * tell which properties are missing, recognised or superfluous in comparison to the template.
   */
  propertyNames: {
    /** Missing property field names are present in the template but were not found in this database item. */
    missing: string[];

    /** Recognised property fields exist both in the template and in the database item. */
    recognised: string[];

    /**
     * Superfluous property fields exist only in the database item but not in the template.
     * This means that we also have no information about their type or further boundaries or
     * specifications, as that information is only present in the template.
     * So superfluous properties will not be included in the type match checks below.
     */
    superfluous: string[];
  };

  /**
   * Property types record which type a given property has, which eases further processing later.
   * (might not be needed any more?)
   *
   * key: propertyName
   * value: type, according to the template
   */
  propertyTypes: Record<string, string>;

  /** Whether the contents of each property adhere to its type, as speficied by the template. */
  propertyTypeMatches: Record<string, FieldMatchOutcome>;

  /** Examples for type matches and type mismatches for quickly iterating data quality problems. */
  propertyTypeMatchExamples: Record<string, Record<FieldMatchOutcome, any[]>>;
}

/**
 * A cumulative summary of all the single results of each match check.
 *
 * As the summary is built up via an Array.reduce() operation, it has to fulfil some constraints
 * such as being able to build upon itself during aggregation.
 */
export type ResultSummary = {
  /**
   * Any item checks that produced a DisallowedResult will be aggregated here.
   *
   * key: disallowed template key
   * values: an array of items that use this disallowed template key
   */
  disallowedTemplateItems: Record<string, string[]>;

  /** Aggregated summary of the base (i.e. top-level) attributes of all the database items */
  baseSummary: BaseResultSummary;

  /** Aggregated summary of the map settings of all the database items */
  mapSummary: MapResultSummary;

  /** Aggregated summary of the property fields of all the database items */
  propSummary: PropertyResultSummary;
};

/** Aggregated summary of the base (i.e. top-level) attributes of all the database items */
export interface BaseResultSummary {
  /**
   * Counter of how many items were encountered by template.
   *
   * key: template key
   * value: number of items that use this template
   */
  encounteredTemplates: Record<string, number>;

  /** Counts and example lists of match-checks for base and/or map attributes. */
  attributeMatches: Record<string, AttributeMatchStats>;
}

/** Aggregated summary of the map settings of all the database items */
export interface MapResultSummary {
  /** Counts and example lists of match-checks for base and/or map attributes. */
  attributeMatches: Record<string, AttributeMatchStats>;
}

/** Counts and example lists of match-checks for base and/or map attributes. */
export interface AttributeMatchStats {
  /** Counter of how many items had valid attributes here */
  count: number;

  /** Size-restricted array of examples for valid attributes. (Size default: 3) */
  true: any[];

  /** Size-restricted array of examples for invalid attributes. (Size default: 3) */
  false: any[];
}

/** Aggregated summary of the property fields of all the database items */
export interface PropertyResultSummary {
  /**
   * Aggregated result of whether all property names of all templates exist in the data.
   * This only talks about existence of properties. Thier types are checked below.
   *
   * key: template key
   * value: ..
   *   key: property name
   *   value: see PropCounts (see jsdoc there)
   */
  names: Record<string, Record<string, PropCounts>>;

  /**
   * Aggregated result of whether the types of property contents match their type, as defined by the templates.
   *
   * key: template key
   * value:
   *   key: property name
   *   value: see FieldMatchSummary
   */
  typeMatches: Record<string, Record<string, FieldMatchSummary>>;

  /**
   * Aggregated examples of type matches and mismatches.
   *
   * key: template key
   * value:
   *   key: property name
   *   value:
   *     matches_type: Array of positive examples of property contents
   *     no_match: Array of negaqtive examples of property contents
   */
  typeMismatchExamples: Record<
    string,
    Record<string, Record<FieldMatchOutcome, DbItemProperty[]>>
  >;
}

/**
 * Although PropCounts and FieldMatchSummary are aggregated in different ways,
 * they should still be displayed together. So this type combines both for that purpose.
 */
export type PropertySummary = PropCounts & FieldMatchSummary;

/** This type keeps track of how many properties overall were found to be missing, recognised or superfluous. */
export interface PropCounts {
  /** Aggregated count of **missing** property field names. See PropertyMatchResult for more info. */
  missing: number;

  /** Aggregated count of **recognised** property field names. See PropertyMatchResult for more info. */
  recognised: number;

  /** Aggregated count of **superfluous** property field names. See PropertyMatchResult for more info. */
  superfluous: number;

  /** The property's field_type for display in the Text-UI output. Only filled if the item's template exists. */
  field_type?: ItemTemplateFieldType | null;
}

/** Used in PropertyResultSummary.typeMismatchExamples above. */
export type FieldMatchOutcome = keyof FieldMatchSummary;
/** A Text-UI displayable aggregated count of how many field contents match their template field type */
export interface FieldMatchSummary {
  /** Counts how many fields match their template's type */
  matches_type: number;

  /** Counts how many fields do not match their template's type */
  no_match: number;
}
