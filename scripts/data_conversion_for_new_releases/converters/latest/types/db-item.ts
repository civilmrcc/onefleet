import type { Templates } from '../templates';

export interface DbItem {
  /**
   * The database id of this item. It is assumed to consist of the following:
   * Template.pouchdb_identifier + random id (ulid)
   */
  _id: string;

  /** A DbItem that was read from the db has a revision. Newly created DbItems in memory do not. */
  _rev?: string;

  /** The time at which this item was created, in ISO date format. */
  time_created: string;

  /** The time at which this item was last modified, in ISO date format. */
  time_modified: string;

  /** The template of this item. The template contains field types and other boundaries for data */
  template: keyof typeof Templates.templates;

  /** see item templates for available properties */
  properties: Record<string, DbItemProperty>;

  /** soft-delete of items should only be allowed if a duplicate exists */
  soft_deleted?: boolean;

  /** TODO: make map_settings always be present */
  map_settings?: DbItemMapSettings;

  /** Template Version in which the item has been created */
  prefix_created: string;

  /**
   * Template Version in which the item was last converted, i.e., during data migration.
   * This field must never be changed in the frontend code.
   */
  prefix_converted?: string;

  /** Template Version in which the item has last been saved */
  prefix_modified: string;
}

export interface DbItemMapSettings {
  /** The color that this item's marker should have on the map */
  marker_color: string;

  /** The icon that should be used to render this item on the map (not used yet) */
  marker_icon: string;

  /** number of days that should be loaded for this item */
  get_historical_data_since?: number;

  /** The tracking type decides how an item may be automatically tracked by the location service */
  tracking_type?: TrackingTypeOptions;

  /** e.g. MMSI number for AIS or Iridium Sender Mail or ADSB id */
  tracking_id?: string;

  /** e.g. fleetmon vessel id or other temporary token */
  tracking_token?: string;
}

export type TrackingTypeOptions = 'MANUAL' | 'AIS' | 'ADSB' | 'IRIDIUMGO';

/** See type ItemTemplateField for available property types */
export type DbItemProperty = string | number | boolean | null | undefined;
