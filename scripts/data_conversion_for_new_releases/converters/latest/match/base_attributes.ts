import templates from '../templates';
import type { DbItem as DbItemV03 } from '../types/db-item';
import type { ItemTemplate } from '../types/item-template-schema';

export function matchBaseAttributes(
  convertedItem: DbItemV03
): Record<string, boolean> {
  const itemTemplate: ItemTemplate = templates.get(convertedItem.template);

  return {
    _id:
      // does the _id start with the template's pouch_identifier?
      itemTemplate
        ? convertedItem._id.startsWith(itemTemplate.pouch_identifier + '_')
        : false,

    template:
      // does the template exist? (kinda redundant due to the guard statement earlier)
      !!itemTemplate,

    time_created:
      // does the time exist as an ISO string?
      Date.parse(`${convertedItem.time_created}`) &&
      new Date(`${convertedItem.time_created}`).toISOString() ==
        `${convertedItem.time_created}`,

    time_modified:
      // does the time exist as an ISO string?
      Date.parse(`${convertedItem.time_modified}`) &&
      new Date(`${convertedItem.time_modified}`).toISOString() ==
        `${convertedItem.time_modified}`,

    prefix_created:
      // Template Version in which the item was created. Should not change.
      !!convertedItem.prefix_created,

    prefix_converted:
      // Template Version to which the item was last converted must be this converter's version.
      convertedItem.prefix_converted === 'v0_3_',

    prefix_modified:
      // Template Version in which the item was last saved needs to exist.
      !!convertedItem.prefix_modified,

    soft_deleted:
      // check that the field contains a boolean or is undefined
      convertedItem.soft_deleted === true ||
      convertedItem.soft_deleted === false ||
      convertedItem.soft_deleted === undefined,
  };
}
