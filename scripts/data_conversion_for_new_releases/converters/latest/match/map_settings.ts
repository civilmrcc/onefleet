import type { DbItem as DbItemV03 } from '../types/db-item';

export function matchMapSettings(
  convertedItem: DbItemV03
): Record<string, boolean> {
  return {
    marker_color:
      // must be a HTML color string
      /^#([A-Fa-f0-9]{6})$/.test(convertedItem.map_settings.marker_color),

    marker_icon:
      // no constraints here yet! (will check against an array of registered icons in the future)
      true,

    get_historical_data_since:
      // must be an integer number between -1 and positive infinity
      Number.isInteger(convertedItem.map_settings.get_historical_data_since) &&
      convertedItem.map_settings.get_historical_data_since >= -1,

    tracking_type:
      // must be one of the following:
      ['MANUAL', 'AIS', 'ADSB', 'IRIDIUMGO'].includes(
        convertedItem.map_settings.tracking_type
      ),

    tracking_id:
      // no constraints here yet! (might check MMSI validity or IridiumId depending on tracking_type in the future)
      true,

    tracking_token:
      // no constraints here yet!
      true,
  };
}
