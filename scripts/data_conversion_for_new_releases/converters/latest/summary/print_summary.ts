import type {
  ResultSummary,
  BaseResultSummary,
  MapResultSummary,
  PropertyResultSummary,
  PropertySummary,
} from '../types';

export function printSummary(checkingSummary?: ResultSummary | null): void {
  if (!checkingSummary) {
    console.info('No ResultSummary given. exiting.');
    process.exit(1);
  }
  printBaseResultSummary(checkingSummary.baseSummary);
  printMapResultSummary(checkingSummary.mapSummary);
  printPropertiesSummary(
    checkingSummary.propSummary,
    checkingSummary.baseSummary
  );
  console.info(
    'Disallowed templates:',
    checkingSummary.disallowedTemplateItems
  );
}

function printBaseResultSummary(baseSummary: BaseResultSummary): void {
  console.info('\n========== Base attribute matches ==========');
  console.table(baseSummary.attributeMatches);
}

function printMapResultSummary(mapSummary: MapResultSummary): void {
  console.info('\n========== Map attribute matches ==========');
  console.table(mapSummary.attributeMatches);
}

function printPropertiesSummary(
  propSummary: PropertyResultSummary,
  baseSummary: BaseResultSummary
): void {
  console.info('\n========== Properties: Existence and Types ==========');
  for (const templateName of Object.keys(propSummary.names)) {
    // title:
    const itemCount = baseSummary.encounteredTemplates[templateName];
    console.info(`\nTemplate '${templateName}' (${itemCount} items):`);

    // combine propSummary.names and propSummary.typeMatches for printing:
    const propertyNameTypeMatches: Record<string, PropertySummary> = {};
    for (const propName of Object.keys(
      propSummary.names[templateName]
    ).sort()) {
      propertyNameTypeMatches[propName] = {
        ...propSummary.names[templateName][propName],
        ...propSummary.typeMatches[templateName][propName],
      };
    }
    console.table(propertyNameTypeMatches);
  }
  console.info('\n========== Properties: Mismatch Examples ==========');
  for (const templateName of Object.keys(propSummary.names)) {
    for (const examplePropName of Object.keys(
      propSummary.typeMismatchExamples[templateName]
    )) {
      const exampleMismatches =
        propSummary.typeMismatchExamples[templateName][examplePropName];
      if (exampleMismatches['no_match'].length > 0) {
        console.info(
          `Examples for property field '${examplePropName}' of template '${templateName}':`,
          exampleMismatches
        );
      }
    }
  }
}
