import templateHelper from '../templateHelper';
import type {
  CheckingResult,
  ResultSummary,
  DisallowedResult,
  MatchResult,
  BaseResultSummary,
  PropertyResultSummary,
  MapResultSummary,
  AttributeMatchStats,
} from '../types';

export function summariseResult(
  summary: ResultSummary | null,
  result: CheckingResult
): ResultSummary {
  // init the summary object on first call:
  if (!summary) {
    summary = {
      disallowedTemplateItems: {},
      baseSummary: {
        encounteredTemplates: {},
        attributeMatches: {},
      },
      mapSummary: {
        attributeMatches: {},
      },
      propSummary: {
        names: {},
        typeMatches: {},
        typeMismatchExamples: {},
      },
    };
  }

  // skip to next result if template is disallowed (guard statement):
  const badTemplate = (result as DisallowedResult).disallowedTemplate;
  if (badTemplate) {
    const badId = result.item._id;
    const disallowedItems = summary.disallowedTemplateItems[badTemplate] ?? [];
    disallowedItems.push(badId);
    summary.disallowedTemplateItems[badTemplate] = disallowedItems;
    return summary; // if the template is unknown/disallowed, then there is no use in continuing.
  }

  return {
    disallowedTemplateItems: summary.disallowedTemplateItems,
    baseSummary: summariseBase(summary.baseSummary, result as MatchResult),
    mapSummary: summariseMap(summary.mapSummary, result as MatchResult),
    propSummary: summariseProps(summary.propSummary, result as MatchResult),
  };
}

function summariseBase(
  baseSummary: BaseResultSummary,
  matchResult: MatchResult
): BaseResultSummary {
  // count how many items of each template are being encountered, for the "(895 items)" string in the summary:
  if (!baseSummary.encounteredTemplates[matchResult.item.template]) {
    baseSummary.encounteredTemplates[matchResult.item.template] = 0;
  }
  baseSummary.encounteredTemplates[matchResult.item.template]++;

  // check if the given base attributes are defined and well:
  baseSummary.attributeMatches = countMatches(
    baseSummary.attributeMatches,
    matchResult.baseAttributeValidity,
    matchResult.item,
    1
  );

  return baseSummary;
}

function summariseMap(
  mapSummary: MapResultSummary,
  matchResult: MatchResult
): MapResultSummary {
  // check if the given map_settings attributes are defined and well:
  mapSummary.attributeMatches = countMatches(
    mapSummary.attributeMatches,
    matchResult.mapSettingsValidity,
    matchResult.item.map_settings
  );
  return mapSummary;
}

function countMatches(
  attributeMatches: Record<string, AttributeMatchStats>,
  validity: Record<string, boolean>,
  attributeContents: Record<string, any>,
  numExamples: number = 3
): Record<string, AttributeMatchStats> {
  for (const [attributeName, match] of Object.entries(validity)) {
    const matchStats = attributeMatches[attributeName] ?? {
      count: 0,
      true: [],
      false: [],
    };
    matchStats.count += match ? 1 : 0;
    if (matchStats[String(match)].length < numExamples) {
      matchStats[String(match)].push(attributeContents[attributeName]);
    }
    attributeMatches[attributeName] = matchStats;
  }
  return attributeMatches;
}

function summariseProps(
  propSummary: PropertyResultSummary,
  matchResult: MatchResult
): PropertyResultSummary {
  // prepare summary.propCounts and propTypeCounts for this result's item's template:
  if (!propSummary.names[matchResult.item.template]) {
    propSummary.names[matchResult.item.template] = {};
    propSummary.typeMatches[matchResult.item.template] = {};
    propSummary.typeMismatchExamples[matchResult.item.template] = {};
  }
  // prepare summary.propCounts for each propName:
  for (const propName of [
    ...matchResult.propertiesValidity.propertyNames.missing,
    ...matchResult.propertiesValidity.propertyNames.recognised,
    ...matchResult.propertiesValidity.propertyNames.superfluous,
  ]) {
    if (!propSummary.names[matchResult.item.template][propName]) {
      propSummary.names[matchResult.item.template][propName] = {
        superfluous: 0,
        missing: 0,
        recognised: 0,

        // the field type is <undefined> if a property cannot be found
        // amongst the item's template fields, i.e., for superfluous properties:
        field_type:
          templateHelper.templateFieldSpecifications[
            matchResult.item.template
          ]?.[propName]?.type,
      };
    }
  }

  // summarise propertyNames that are missing, recognised, or superfluous:
  for (const countKey of Object.keys(
    matchResult.propertiesValidity.propertyNames
  )) {
    for (const missingProperty of matchResult.propertiesValidity.propertyNames[
      countKey
    ]) {
      propSummary.names[matchResult.item.template][missingProperty][
        countKey
      ] += 1;
    }
  }

  // summarise propertyTypeMatches and include the field_type in summary:
  for (const [propName, matchKey] of Object.entries(
    matchResult.propertiesValidity.propertyTypeMatches ?? {}
  )) {
    const propMatches = propSummary.typeMatches[matchResult.item.template][
      propName
    ] ?? {
      matches_type: 0,
      no_match: 0,
    };
    propMatches[matchKey]++;
    propSummary.typeMatches[matchResult.item.template][propName] = propMatches;

    const summaryExamples = propSummary.typeMismatchExamples[
      matchResult.item.template
    ][propName] ?? { matches_type: [], no_match: [] };
    summaryExamples[matchKey] = [
      ...summaryExamples[matchKey],
      ...matchResult.propertiesValidity.propertyTypeMatchExamples[propName][
        matchKey
      ],
    ];
    propSummary.typeMismatchExamples[matchResult.item.template][propName] =
      summaryExamples;
  }
  return propSummary;
}
