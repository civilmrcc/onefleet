import templates from '../templates';
import type { DbItem as DbItemV03 } from '../types/db-item';

export function convertBaseAttributes(inputItem: any): DbItemV03 {
  let outputItem: Partial<DbItemV03> = { ...inputItem };

  // rename any templates before guard statement:
  if (inputItem.template === 'vehicle') {
    outputItem.template = 'ship';
  }
  // guard statement
  const itemTemplate = templates.get(outputItem.template);
  if (!itemTemplate) {
    return outputItem as DbItemV03;
  }

  // define undefined attributes:
  outputItem = {
    ...outputItem,
    prefix_created: inputItem.prefix_created ?? 'v0_2_',
    prefix_converted: inputItem.prefix_converted ?? 'v0_3_',
    prefix_modified: inputItem.prefix_modified ?? 'v0_3_',
  };

  return outputItem as DbItemV03;
}
