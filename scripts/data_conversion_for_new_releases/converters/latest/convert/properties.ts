import templates from '../templates';
import type { DbItem as DbItemV03, DbItemProperty } from '../types/db-item';
import templateHelper from '../templateHelper';
import type { ItemTemplateField } from '../types/item-template-schema';

export function convertProperties(inputItem: any): DbItemV03 {
  const itemTemplate = templates.get(inputItem.template);
  // guard statement:
  if (!itemTemplate) {
    return inputItem;
  }

  const outputItem: Partial<DbItemV03> = inputItem; // just for typing

  outputItem.properties = Object.fromEntries(
    Object.entries(outputItem.properties).map(
      ([propName, propValue]: [string, DbItemProperty]) =>
        convertProperty(propName, propValue, outputItem)
    )
  );

  // delete specific unused properties:
  for (const propName of ['color']) {
    if (
      !Object.keys(
        templateHelper.templateFieldSpecifications[outputItem.template]
      ).includes(propName)
    ) {
      delete outputItem.properties[propName];
    }
  }

  return outputItem as DbItemV03;
}

function convertProperty(
  propName: string,
  propValue: DbItemProperty,
  outputItem: Partial<DbItemV03>
): [string, DbItemProperty] {
  // rename properties (i.e. field names)
  const propRenames = {
    people_drowned: 'people_dead',
    Outcome: 'outcome',
    ' boat_notes': 'boat_notes',
    Notes: 'notes',
    URL: 'url',
  };
  for (const [oldName, newName] of Object.entries(propRenames)) {
    if (propName === oldName) {
      propName = newName;
    }
  }

  // retrieve field information if it exists after renaming in the current template
  const field: ItemTemplateField | undefined =
    templateHelper.templateFieldSpecifications[outputItem.template][propName];

  // convert values per property type:
  if (field) {
    switch (field.type) {
      case 'number':
        if (propValue === '-1' || propValue == null) {
          propValue = -1;
        }
        break;

      case 'select':
        if (propValue === 'not_filled') {
          propValue = 'currently_unknown';
        }
        break;

      case 'datetimemulti':
        const dateString = String(propValue ?? '').trim();
        try {
          // try to parse as a single date:
          propValue = convertDate(dateString);
        } catch (e) {
          // try to parse as multiple dates:
          try {
            propValue = dateString
              .split(';')
              .map((singleDate) => convertDate(singleDate.trim()))
              .join(';');
          } catch (e) {
            try {
              propValue = dateString
                .split(',')
                .map((singleDate) => convertDate(singleDate.trim()))
                .join(';');
            } catch (e) {
              // just leave as-is we cannot even parse it as multiple dates
            }
          }
        }
        break;

      default:
      // no data conversion necessary for other fields
    }
  }

  return [propName, propValue];
}

function convertDate(dateString: string): string | null {
  if (dateString === '') {
    return null;
  }

  const regexReplacements = [
    {
      // matches "2022-03-05;23:55", "2022-03-05 ; 23:55", "2022-03-05 ;23:55", "2022-03-05; 23:55" and also "2022-03-05:23:55", "2022-03-05 : 23:55" and even "2022-03-0523:55" oh and "2022-09-02 @ 23:57z":
      regex: /^(\d{4})-(\d{2})-(\d{2})[ -]?[:;@]? ?(\d{2}:\d{2})(:\d{2})?(z)?$/,
      tries: {
        '$1-$2-$3T$4$5z': 'nearly ISO:    ',
        '$1-$2-$3T$4:00z': 'badSecondsISO: ',
        '$1-$3-$2T$4:00z': 'inverted nISO: ',
      },
    },
    {
      // matches "1016z, 12.09.2022", "1016, 12.09.2022", " 1942z, 10.10.2022", "22:38z 25.08.2022":
      regex: /^(\d{2}):?(\d{2})z,?  ?(\d{2}).(\d{2}).(\d{4})$/,
      tries: { '$5-$4-$3T$1:$2z': 'zulu & DE:     ' },
    },
    {
      // matches "1016z, 2022-12-05", "1016, 2022-12-05", " 1942z, 2022-12-05", "22:38z 2022-12-05":
      regex: /^(\d{2}):?(\d{2})z,?  ?(\d{4}).(\d{2}).(\d{2})$/,
      tries: { '$3-$4-$5T$1:$2z': 'zulu & ISO:    ' },
    },
    {
      // matches "0912z":
      regex: /^(\d{2}):?(\d{2})[zZ]$/,
      tries: { '1970-01-01T$1:$2z': 'zulu only:     ' },
    },
  ];

  for (const { regex, tries } of regexReplacements) {
    if (dateString.match(regex)) {
      for (const [replacement, logInfo] of Object.entries(tries)) {
        const repaired = dateString.replace(regex, replacement);
        if (Date.parse(repaired)) {
          return new Date(repaired).toISOString();
          // useful debug output while developing more regexes:
          // console.log(
          //   `${logInfo}${dateString}\t->${repaired}     \t->${propValue}`
          // );
          // const t_departure = output_item.properties.time_of_departure
          // const t_disembark = output_item.properties.time_of_disembarkation
          // if (t_departure) { console.log('  time_of_departure:', t_departure)}
          // if (t_disembark) { console.log('  time_of_disembarkation:', t_disembark)}
        }
      }
    }
  }
  throw new Error('The date string could not be parsed.');
}
