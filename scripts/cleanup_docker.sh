echo 'remove all images'
docker rmi $(docker images -a -q)  -f

echo 'stop all containers'
docker stop $(docker ps -a -q)

echo 'remove all containers'
docker rm $(docker ps -a -q)


read -p "do you also want to remove all volumes? " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
docker volume prune
fi


rm -rf ../services/database/docker_mount/

