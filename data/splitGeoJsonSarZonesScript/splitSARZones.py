import json
import os
import re

# Define the input and output paths
input_file = 'SARZones.geojson'
output_dir = 'split'

# Ensure the output directory exists
os.makedirs(output_dir, exist_ok=True)

# Function to split GeoJSON into its parts
def split_geojson(geojson):
    if geojson['type'] != 'FeatureCollection':
        raise ValueError('Invalid GeoJSON: Expected FeatureCollection')

    return [{'id': i, 'feature': feature} for i, feature in enumerate(geojson['features'])]

# Function to generate a safe filename by escaping non-alphanumeric characters and converting to lowercase
def sanitize_filename(name):
    return re.sub(r'[^a-z0-9_-]', '_', name.lower())

# Function to generate a unique filename for a country and name
def generate_filename(country, name, output_dir):
    base_name = f"{sanitize_filename(country)}_sar_{sanitize_filename(name)}"
    filename = f"{base_name}.geojson"
    counter = 1
    while os.path.exists(os.path.join(output_dir, filename)):
        filename = f"{base_name}_{counter}.geojson"
        counter += 1
    return filename

# Read the GeoJSON file
with open(input_file, 'r') as f:
    geojson = json.load(f)

# Split the GeoJSON into its parts
split_features = split_geojson(geojson)

# Save each feature to a separate file
for part in split_features:
    feature = part['feature']
    country = feature['properties'].get('country', 'unknown')
    name = feature['properties'].get('name', f'feature_{part["id"]}')
    filename = generate_filename(country, name, output_dir)
    output_file = os.path.join(output_dir, filename)

    with open(output_file, 'w') as f:
        json.dump(feature, f, indent=2)

print(f"Split {len(split_features)} features into the '{output_dir}' directory.")
