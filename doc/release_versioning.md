Changelog:
2022-01-18 First [draft ](https://gitlab.com/civilmrcc/onefleet/-/merge_requests/309/diffs?commit_id=73ca728240ac16acec6e1303920622698128d45d) and Discussions
2022-01-22 Discussions incorporated into [first commit of branch](https://gitlab.com/civilmrcc/onefleet/-/compare/develop...177-discuss-and-update-release_process_md)
2022-03-01 Correct details based on discussion processes
2022-09-01 Add infos about Gitlab Release labels and Template Versioning of Items

# Release Versioning

This page sets rules for updating the version number of releases. In addition, the usage of the database prefix is defined.

- [Release Versioning](#release-versioning)
  - [Semantic Versioning](#semantic-versioning)
  - [Public API](#public-api)
      - [Templates](#templates)
      - [Db Types](#db-types)
  - [Rules for updating versions](#rules-for-updating-versions)
    - [Onefleet Vue.js App](#onefleet-vuejs-app)
      - [Gitlab Release Labels](#gitlab-release-labels)
      - [Major Release](#major-release)
      - [Minor release](#minor-release)
      - [Patch release](#patch-release)
    - [Excel, defining the Database Schema](#excel-defining-the-database-schema)
  - [Database prefix](#database-prefix)
- [FAQ](#faq)

Onefleet is developed on the [development](https://gitlab.com/civilmrcc/onefleet/-/tree/develop) branch and each release merged to the [Main](https://gitlab.com/civilmrcc/onefleet/-/tree/main) branch.

## Semantic Versioning

Onefleet uses [Semantic Versioning](https://semver.org/) to release new versions. The versioning is defined as following:

The version number consists of: **Major.Minor.Patch**

**Patch**

> Patch version Z (x.y.Z | x > 0) MUST be incremented if only backwards compatible bug fixes are introduced. A bug fix is defined as an internal change that fixes incorrect behavior.

**Minor**

> Minor version Y (x.Y.z | x > 0) MUST be incremented if new, backwards compatible functionality is introduced to the public API.

**Major**

> Major version X (X.y.z | X > 0) MUST be incremented if any backwards incompatible changes are introduced to the public API.

Note: When the app has e.g. version number 2.1.1 and a major version update is made, the lower numbers are reset to 0 -> 3.0.0

- [ ] To be discussed: Include definition of [Serialization Versioning](https://gitlab.com/civilmrcc/onefleet/-/commit/73ca728240ac16acec6e1303920622698128d45d#note_816524948).

_[Back to top](#release-versioning)_

## Public API

The public API consists of:

#### Templates

The [templates](./template_system.md) systems defines the database schema of items. It is defined technically and organizationally for the stakeholders.

- Technical definition: Typescript files in `src/constants/templates/*` and in `src/types/item-template-schema.ts`
- Organizational definition: Excel file in `docs/template-fields/template-fields_<version>.xlsx`.

#### Db Types

Any file starting with `db-` in `src/types/`.

_[Back to top](#release-versioning)_

## Rules for updating versions

In addition to the definitions given above, different rules apply for

- The Onefleet Vue.js App
- The Excel, defining the template system

### Onefleet Vue.js App

#### Gitlab Release Labels

In case of pushing changes that require a version upgrade, the developer has add the respective `Release::` label to the MR, which are `Release::Major`, `Release::Minor` and `Release::Patch`.

Multiple MRs with a `Release::Patch`-label can be combined into one patch-release and multiple MRs with `Release::Minor`-label can be combined into one minor-release. But if one or more MRs have the minor or even major label, then the next release MUST receive a version bump instead of just being a patch release. This allows us to quickly check whether any such MRs have been merged since the previous release, and should help us decide whether an upcoming must bump up the minor or even major versions, or if it is ok to just make another path release without any breaking changes.

#### Major Release

The first stable release has the version 1.0.0:

> {Major} = 0 → Initial development when anything can change
> {Major} >= 1 → 1st public stable release

#### Minor release

A minor (or major) release must be issued when:

- The database schema is changed:
  - Either of the [templates](./template_system.md) is updated. Templates are part of the the [Public Api](#public-api). Exception: New allowed values can go in a patch release.
  - Either of the db types is updated. Db types are part of the the [Public Api](#public-api).
- The configuration files are changed.

#### Patch release

In a patch release the [public API](#public-api) is not allowed to change.

### Excel, defining the Database Schema

The Excel defining the database schema is part of the [Public API](#public-api). Each release of onefleet contains the current excel file in the repository in `docs/template-fields/template-fields_<version>.xlsx`.

The templates are change according to [the workflow to change templates.](../doc/template_system.md#workflow-to-change-template) The Version of the Excel updates with every Template Change in Onefleet as part of the Merge Request.

**The version of the Excel is updated as following:**

- Patch Version → New Allowed Values

- Minor Version → New Fields

- Major Version → Delete/Edit Fields.

_[Back to top](#release-versioning)_

## Database prefix

The database prefix contains the major and the minor version and excludes the patch version:

Example database prefix: `v1-3_` ({Major} = 1; {Minor} = 3; {Patch} = any)

With each minor and major release the database prefix must change. This creates a new database with the updated prefix. In a patch release the database schema is not allowed to change.

With the rules applied each database therefore has an unchanged database schema.

Every item has the two fields `prefix_created` and `prefix_modified`, which each respectively hold the version in which the item has been created or last been saved. They are set automatically to the value of `VUE_APP_DB_PREFIX` from the `.env` file.

_[Back to top](#release-versioning)_

# FAQ

Please go to the [FAQ Page](FAQ.md).

_[Back to top](#release-versioning)_
