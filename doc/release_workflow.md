# Release Workflow

## Release Planning

Minor and major releases should be planned using the [Milestone](https://gitlab.com/civilmrcc/onefleet/-/milestones) Feature of Gitlab (**TBD**).

A major or minor releases should be released every...**TBD**.

## Release Process

### Release Communication

Changes of releases are documented in the [Changelog](../CHANGELOG.md) and communicated to the users by...**TBD**..

### Release steps

**Create release**

1. Decide if the release is a major, minor or patch release. For major and minor releases, the [Excel, defining the database schema](./release_versioning.md#excel-defining-the-database-schema) needs to be updated.
2. Create a Tag in [GitLab](https://gitlab.com/civilmrcc/onefleet/-/tags) accordingly. The tag triggers a [pipeline](https://gitlab.com/civilmrcc/onefleet/-/pipelines) to be run.

**Update Changelog**

3. Update the [Changelog](../CHANGELOG.md) based on https://keepachangelog.com/en/1.0.0/

> **Guiding Principles**
>
> - Changelogs are for humans, not machines.
> - There should be an entry for every single version.
> - The same types of changes should be grouped.
> - Versions and sections should be linkable.
> - The latest version comes first.
> - The release date of each version is displayed.
> - Mention whether you follow Semantic Versioning.

> **Types of changes**
>
> - `Added` for new features.
> - `Changed` for changes in existing functionality.
> - `Deprecated` for soon-to-be removed features.
> - `Removed` for now removed features.
> - `Fixed` for any bug fixes.
> - `Security` in case of vulnerabilities.

**Deploy to staging server**

4. Each release contains a `release-artifact` which can be deployed to the servers. If a minor or major release is issued, the [database-prefix](./release_versioning.md#database-prefix) changes and a new database needs to be created.

**Test staging server**

5. Test the app on the staging server and make sure it works as expected. If the release is not stable, the issues need to be fixed first. Once the issues are fixed a new release can be created, starting from step 1.

**Merge to main**

6. If the release is stable, merge it to main. Main contains only stable releases.

**Migrate database**

7. **TBD.** For minor and major releases the data needs to be migrated to the new database.

**Communicate to users**

8. **TBD.** Communicate the release to the users.
