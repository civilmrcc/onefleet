# ADR-007: Mitt Event Emitter Library

## Context

[Vue3's Event API does not, among others, support `$on` instance method anymore](https://v3-migration.vuejs.org/breaking-changes/events-api.html), which break's the application's eventBus system after upgrading to Vue3.
The Migration Guide(https://v3-migration.vuejs.org/breaking-changes/events-api.html#event-bus) recommends [Mitt](https://github.com/developit/mitt).

**Pros**

- Mitt's API is based on the same ideas as Node's event emitter and hat the same names and usage.
- small package size
- no dependencies

## **Cons**

## Decision

Mitt will be temporarily used as event emitter until global state management with Pinia is implemented and therefore event bus is not needed anymore.

## Status

Removed.

## Consequences

Node's event emitterEventbus replaced with Mitt.

## Updates

With MR !499 usages of Mitt is replaced in favor of solutions making use of Global Store and other ways like accessing descendants by $refs.
