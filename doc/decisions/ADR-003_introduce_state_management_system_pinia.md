# ADR-003: Introduce State Management using Pinia

## Context

At the moment global states and data-exchange between documents is handled with events and props. This solution has been working well so far, however these custom made solutions might add complexity in the code. A standardized cross-component state management should make the code cleaner.

**Alternatives that were quickly looked at:**
Vuex was originally planned to be used. However, Pinia replaced Vuex as a the [new default](https://github.com/vuejs/rfcs/pull/271#issuecomment-1082732704).

**Pros**
Pinia is the new standard in Vue for state management and can help us to keep track of our items, positions and other data that is used by different components.

Another advantage of Pinia is the need to define how the data can be altered. This improves data quality.

**Cons**
It requires some setup work and it is a new concept to understand.

## Decision

We decide to use Pinia and slowly change existing to replace events and props where it makes sense. Before using it for items and positions, a careful planning should be made.

## Status

Implemented (MR !499)

## Consequences

Due to the current architecture, bigger changes are needed to make real use of Pinia. The introduction is plannend and executed in [Epic 61](https://gitlab.com/groups/civilmrcc/-/epics/61).
