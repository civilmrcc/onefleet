# ADR-006: Introducing CouchDB Filtered Replication

## Context

At the moment in take up to 30 minutes to start onefleet when started for the first time. This is due to the huge positions database which is loading all the automatically generated positions. However a user ideally wants to start using the application right away and it is assumed that they don't need to access all positions existing in the App right away.

Therefore it has been proposed to introduce [Filtered Replication](https://pouchdb.com/api.html#filtered-replication) to only load selected positions during initial App startup.

**Pros**

- Making use of a better option that is available in the CouchDB ecosystem
- Improved UX

**Cons**

None

## Decision

Implemented with CouchdDB's [selector syntax](http://docs.couchdb.org/en/stable/api/database/find.html#selector-syntax), because it didn't work with design documents.
For the first replication, only positions that were **created within Onefleet** (therefore excluding automatically generated positions) of **the last 30 days** are loaded (from remote to local).

The user can also manually refresh the application with all the positions loaded so far.
Once the first replication finishes, the second replication is triggered and synchronizes local and remote databases, thereby fetching all the remaining positions in the background.

## Status

Accepted and implemented.

## Consequences

The fields that shall be queried over need to be [indexed](https://pouchdb.com/guides/mango-queries.html#more-than-one-field).
