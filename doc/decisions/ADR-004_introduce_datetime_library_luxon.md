# ADR-004: Introducing Datetime library luxon.js for Time and Date Management

## Context

Currently we use native JS functions to handle time and date management, however they are insufficient and error-prone when it comes to managing [multiple timezones](https://gitlab.com/civilmrcc/onefleet/-/issues/155).

To make things easier and not to reinvent the wheel, we can use a datetime library like [Luxon](https://github.com/moment/luxon/blob/master/docs/why.md).

**Pros**

- clearer API
- integrated timezone features using immutable objects
- is endorsed by moment.js, a popular datetime library and is de-facto a successor
- [supports latest 2 versions of modern browsers](https://github.com/moment/luxon/blob/master/docs/matrix.md) like Chrome and Firefox
- wide community support

**Cons**

Both alternative candidates `day.js` and `date-fns` are currently more used and still receive more weekly downloads. Both are also smaller and size, especially day.js. However day.js has poor timezone features, which are not enough for the requirements of this project.

## Decision

Undecided.

## Status

It has been proposed and a first feature implemented, however it has not been fully agreed upon by everyone yet.

## Consequences

It is proposed to include luxon.js and abstract away calls to it to functions grouped in a utility file `datetimeUtils.ts`. That way, the library can be easily swapped out with a different library, in case requirements change.
