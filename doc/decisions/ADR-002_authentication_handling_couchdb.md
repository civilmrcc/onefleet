# ADR-002: Authentication Handling CouchDB

## Context

Until January 2022 authentication to couchDB was not necessary because the local develop environment and the current test server were still using PouchDB instead of CouchDB to store items and positions. PouchDB allowed to enter any credentials to authenticate. CouchDB is more restrictive.

Originally it was planned to use [PouchDB-Authentication](https://github.com/pouchdb-community/pouchdb-authentication) to solve the [authentication issue](https://gitlab.com/civilmrcc/onefleet/-/issues/250). However PouchDB-Authentication did not seem to be well maintained and broken according to posted issues on the repository. 

Therefore it was thought about writing authentication ourvelves. A better maintained module [React-CouchDB-Authentication](https://github.com/stanlemon/react-couchdb-authentication) existed for react which could help as an orientation.

CouchDB allows for [three different options](https://docs.couchdb.org/en/stable/api/server/authn.html?highlight=authentication) to authenticate. We chose cookie authentication because it omits storing and resending credentials.

**Pros**
Cookie authentication should be a rather safe option and is handled by CouchDB and the browser automatically. The session has to be started and then the browser uses the returned from the server for following requests. Also, the browser refreshes the cookie automatically.

**Cons**
Authentication handling is not handled by an external module which is reviewed by more people. This might be a safer option.

## Decision
It was decided to implement CouchDB Authentication on our own. At the same time refactor the PouchWrapper which handles DB Setup and Synchronization.

## Status
Accepted. Merge Request [307](https://gitlab.com/civilmrcc/onefleet/-/merge_requests/307) merges the refactored Database Handling and solved authentication to the develop branch.

## Consequences
- CouchDB can be used which makes it possible to move to the actual server setup.

