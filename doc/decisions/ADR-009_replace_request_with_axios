# ADR-009: Replace Request with Axios

## Context

During the switch of the testing library from `jest` to `vitest` the mocking of the `request` library breaks.
As request has been deprecated already since 2020, we have been in need of an alternative anyway.
Replacing request with axios at this point would have the advantage of not needing to sink more effort in fixing request mocking.

**Pros**

- Axios is the most commonly used library used for network requests
- Axios allows usage of relative urls unlike node-fetch
- Mandatory conversion of LocD from CommonJS to the more modern ESModule, also would establish consistency between frontend module and LocD

**Cons**

- Mandatory conversion from CommonJS to ESModule requires certain changes to be made.

## Decision
We move forward with replacing request with axios. We might want to introduce MockServiceWorker in the future to replace the current mocking strategy.

## Status
Accepted & Implemented

## Consequences

Conversion of LocD module from CommonJS to ESModule