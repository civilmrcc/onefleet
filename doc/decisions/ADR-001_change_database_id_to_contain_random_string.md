# ADR-001: Change Database ID to contain random String

## Context
We need a field for the users to fill in their case id, which differs between the organization. So, there is the need to enter multiple Case IDs for the user.
Currently, the Case ID is also used as the internal database _id. We should change the internal database _id to some (randomly generated) string that the users never see by using https://github.com/ulid/spec
Since the Case IDs are not unique (the number get's reset every year → AP012 can be a different case in 2020 than in 2021), it's also necessary to add a temporal reference. In principle, the year would be sufficient. However, we should use a reference, which is captured already. This could be the creation timestamp.



**Pros**
ULID has the advantage, that you still can sort the guides in a temporal manner.

**Cons**
No cons

## Decision

After a discussion between @simonsthings and @saschakiefersaar in this [issue](https://gitlab.com/civilmrcc/onefleet/-/issues/234) it was decided to create item IDs as `<template>_<ULID>` (upper case) instead of the previous `<template>_<identifier>`, which was not unique as identifiers reset counting again each year. Positions must internally reference their item by that item's_id field instead of the identifier field.

To be more precise, the previous way of creating position IDs in onefleet happened to also use an ad-hoc third part after the timestamp, which was a simple monotonously increasing counter that would be increamented if multiple positions with identical timestamps for a given item were created at once. So the previous format for position IDs was `<item-identifier>_<iso-timestamp>_<counter>` (e.g. SW3_2021-08-03T20:15:00.0000Z_0) and the new format is `<item-id>_<iso-timestamp>_<random-id>` (e.g. VEHICLE_SW3_2021-08-03T20:15:00.0000Z_ahfieoivkfv). ULID is now used to generate that last random id part, which also avoids duplicate key errors while creating multiple positions within the same minute. 

**Future changes**
- For positions middle iso-timestamp part is actually redundant and can be removed, but we need to change some other code for this to work first. See issue: #287
- TEMPLATE prefix in the items is currently still needed for compatibility with the backend location service. That can be removed and be addressed in a separate MR as it is a separate service. 

## Status
accepted

## Consequences
It is made sure, that no duplicate IDs exist and this increases data quality. 




