## Overview

This guide provides detailed instructions for configuring the import tool to parse your file according to the specified columns and formats of AP@0.1

### Initial Setup

#### Template Sheet

- **Sheet Name**: `Template`
- **Required String in A1 Cell**: `AP@0.1` (Activates the specific parsing logic.)

#### Data Sheet

- **Sheet Name**: `Data` (Contains the cases to be parsed.)

### Field Specifications

#### Name

- **NoC**: "AP NR"
- **CL**: "B"
- **OP**: "name"
- **Type**: Text
- **Mandatory**: Yes

#### Type of Boat

- **NoC**: "BOAT TYPE"
- **CL**: "E"
- **OP**: "boat_type"
- **Type**: Select
- **Options**: 
	- currently_unknown
	- wood
	- rubber
	- fiberglass
	- iron
	- sailing
	- fishing_vessel
	- other
	- unknown
- **Default**: "unknown"

#### Phone Number

- **NoC**: "THURAYA"
- **CL**: "C"
- **OP**: "phonenumber"
- **Type**: Text

#### Place of Departure

- **NoC**: "Place . DEP"
- **CL**: "I"
- **OP**: "place_of_departure"
- **Type**: Text
- **Fallback**:
    - **Fallback NoC**: "Country. DEP"
    - **Fallback CL**: "H"

#### SAR Region

- **NoC**: ""
- **CL**: "L"
- **OP**: "sar_region"
- **Type**: Select
- **Options**: 
	- currently_unknown
	- sar1
	- sar2
	- sar3
	- unknown
- **Default**: "unknown"

#### Occurred At

- **NoC**: "Date. DEP" and "time. DEP"
- **CL**: "J" and "K"
- **OP**: "occurred_at"
- **Type**: Datetime
- **Mandatory**: Yes
- **Fallback**:
    - **Fallback NoC**: "Date (rescued, arrived case closed)"
    - **Fallback CL**: "W"

#### Time of Departure

- **NoC**: "Date . DEP" and "time. DEP"
- **CL**: "J" and "K"
- **OP**: "time_of_departure"
- **Type**: Datetime

#### Total POB

- **NoC**: "POB Number"
- **CL**: "G"
- **OP**: "pob_total"
- **Type**: Number

#### People Dead

- **NoC**: "missing people" and "DEAD"
- **CL**: "O" and "P"
- **OP**: "people_dead"
- **Type**: Number

#### Source/URLs

- **NoC**: "Source"
- **CL**: "Z"
- **OP**: "url"
- **Type**: Inputtable
- **Special Character to Separate Multiple URLs**: "|||"

#### Outcome

- **NoC**: "outcome"
- **CL**: "R"
- **OP**: "outcome"
- **Type**: Select
- **Options**: 
	- currently_unknown
	- interception_libya
	- interception_tn
	- ngo_rescue
	- afm_rescue
	- italy_rescue
	- merv_interception
	- merv_rescue
	- returned
	- autonomous
	- unknown
	- empty_boat
	- shipwreck
	- unclear
- **Default Value**: "currently_unknown"

### Positions specifications

#### 1st Position

- **NoC**: "1st position"
- **CL**: "M"
- **Type**: Regex
- **Example**: "41.40338N, 2.17403E @ 2022-09-29T16:06:30Z"

#### Last Position

- **NoC**: "Last position"
- **CL**: "S"
- **Type**: Regex
- **Example**: "41.40338N, 2.17403E @ 2022-09-29T16:06:30Z"
