# Node

This page collects support for managing node.

## nvm

https://gitlab.com/civilmrcc/onefleet/-/issues/330

## Updating

### npm audit never settles

**Question**
The npm auditing never settles on the best combination of packages, but goes back and forth on changes. Also, it never removes all bugs, but I guess that would be wishful thinking? 😔

**Answer**
This is "normal" behavior, as npm fails to completely resolve the necessary major version updates. From experience the best way to keep deps updated (and thereby fix all fixable security issues) is:

1. Use `npm update` to update all packages according to the version constraint set in the `package.json`
2. Use [npm-check-updates](https://www.npmjs.com/package/npm-check-updates) to go through patch, minor and major updates separately:
3. `ncu -u --target minor` to upgrade all versions who have a newer patch or minor version
4. Check the remaining packages which have major upgrades available by running `ncu` and go through them one by one, checking the introduced breaking changes in the respective CHANGELOG and then upgrading it by running `npm install (--dev) PACKAGE_NAME@VERSION`

Anything which continues to pop up as an issue when running `npm audit` will only be fixed through updates by the package maintainers.

Based on [discussion](https://gitlab.com/civilmrcc/onefleet/-/issues/289#note_650339324)
